# État de la traduction (pfs-season-2-bestiary)

 * **aucune**: 229
 * **changé**: 22
 * **libre**: 3


Dernière mise à jour: 2024-06-13 06:52 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0ATZwmS2yxbheza4.htm](pfs-season-2-bestiary/0ATZwmS2yxbheza4.htm)|Scorched Earth Orc (Sharpshooter) (7-8)|
|[0OnpbW2SdOZXdRgw.htm](pfs-season-2-bestiary/0OnpbW2SdOZXdRgw.htm)|Glorzia|
|[0RPGHzGalRHqEEHx.htm](pfs-season-2-bestiary/0RPGHzGalRHqEEHx.htm)|Reinforced Animated Guardian|
|[16NjM1igmVpNcleL.htm](pfs-season-2-bestiary/16NjM1igmVpNcleL.htm)|Tewakam Nekotek (3-4)|
|[1Hr2rWZf52nWnJ4t.htm](pfs-season-2-bestiary/1Hr2rWZf52nWnJ4t.htm)|Tough Leaded Skeleton|
|[1Mn1KhwyL6gU4TRl.htm](pfs-season-2-bestiary/1Mn1KhwyL6gU4TRl.htm)|Hand Of Urxehl (7-8)|
|[1TYxfz5Q4iVQUOa5.htm](pfs-season-2-bestiary/1TYxfz5Q4iVQUOa5.htm)|Bloody Vortex (5-6)|
|[1xVm60zTlUsH8hnR.htm](pfs-season-2-bestiary/1xVm60zTlUsH8hnR.htm)|Barrow Quasit|
|[1ZIaQe0SNDmoVUoh.htm](pfs-season-2-bestiary/1ZIaQe0SNDmoVUoh.htm)|Brittle Ravener Husk|
|[2LcIG75RQoujanC9.htm](pfs-season-2-bestiary/2LcIG75RQoujanC9.htm)|Wishbound Belker (5-6)|
|[3KB7j8D0eMdn8Sol.htm](pfs-season-2-bestiary/3KB7j8D0eMdn8Sol.htm)|Empowered Blood (1-2)|
|[3oL6mD7SYNxbghTd.htm](pfs-season-2-bestiary/3oL6mD7SYNxbghTd.htm)|Kayajima Boar|
|[3ShJKXoXXENIToA8.htm](pfs-season-2-bestiary/3ShJKXoXXENIToA8.htm)|Frostbitten Trollhound|
|[3VXbICsrBiOGnxiz.htm](pfs-season-2-bestiary/3VXbICsrBiOGnxiz.htm)|Ogthup (7-8)|
|[4BaZvzpSYftJEkV8.htm](pfs-season-2-bestiary/4BaZvzpSYftJEkV8.htm)|Abyssal Fungi|
|[4DPuCgAisPyphiMN.htm](pfs-season-2-bestiary/4DPuCgAisPyphiMN.htm)|Teflar|
|[4ER4ulKtEiqgxWEg.htm](pfs-season-2-bestiary/4ER4ulKtEiqgxWEg.htm)|Fire-Eye Cyclops Zombie (3-4)|
|[4NVVOVTT1jIQfAjR.htm](pfs-season-2-bestiary/4NVVOVTT1jIQfAjR.htm)|Burned-Out Efreet|
|[51GRDiQOAA9CP3h3.htm](pfs-season-2-bestiary/51GRDiQOAA9CP3h3.htm)|Mephit Swarm (5-6)|
|[576v0bDKtGE5dwnQ.htm](pfs-season-2-bestiary/576v0bDKtGE5dwnQ.htm)|Clouded Quartz (5-6)|
|[5CosDEozyZDtngtD.htm](pfs-season-2-bestiary/5CosDEozyZDtngtD.htm)|Fire-Eye Cyclops Zombie (1-2)|
|[5NCKrDA869F82zPC.htm](pfs-season-2-bestiary/5NCKrDA869F82zPC.htm)|Leaded Skeleton|
|[5qyOQeszMplJXw8s.htm](pfs-season-2-bestiary/5qyOQeszMplJXw8s.htm)|Flooding Monolith (3-4)|
|[5Vye56emf5vTmHiQ.htm](pfs-season-2-bestiary/5Vye56emf5vTmHiQ.htm)|Collapsing Cabinet (3-4)|
|[61jBzgd1NYLw4NC0.htm](pfs-season-2-bestiary/61jBzgd1NYLw4NC0.htm)|Vermlek Centaur (3-4)|
|[64x0nZk3QlENAgUU.htm](pfs-season-2-bestiary/64x0nZk3QlENAgUU.htm)|Abyssal Firestorm Surge (7-8)|
|[6aIh6EXxkqkBciQA.htm](pfs-season-2-bestiary/6aIh6EXxkqkBciQA.htm)|Blighted Leaf Leshy|
|[6kRWSFIjPusy4d58.htm](pfs-season-2-bestiary/6kRWSFIjPusy4d58.htm)|The Scholar of Sorts (1-2)|
|[6PKCOGCNmsMQHc7A.htm](pfs-season-2-bestiary/6PKCOGCNmsMQHc7A.htm)|Flaming Eye (1-2)|
|[70vDhFdWWU6olNlb.htm](pfs-season-2-bestiary/70vDhFdWWU6olNlb.htm)|Archis Peers (1-2)|
|[7nqeVP1X5v3UMWxZ.htm](pfs-season-2-bestiary/7nqeVP1X5v3UMWxZ.htm)|Scorched Earth Orc (Scout) (5-6)|
|[7R69eW0f3BuHe7TS.htm](pfs-season-2-bestiary/7R69eW0f3BuHe7TS.htm)|Spark Troll (1-2)|
|[7rSEHlUPv6DLkAy1.htm](pfs-season-2-bestiary/7rSEHlUPv6DLkAy1.htm)|Wishbound Belker (7-8)|
|[7w0gBYxl6kS9WYgl.htm](pfs-season-2-bestiary/7w0gBYxl6kS9WYgl.htm)|Strak (5-6)|
|[83k9JooGu3wbCJKa.htm](pfs-season-2-bestiary/83k9JooGu3wbCJKa.htm)|Razortoothed Shark|
|[8Eywy3sqfECiULGl.htm](pfs-season-2-bestiary/8Eywy3sqfECiULGl.htm)|Kareida|
|[8FBm4IjFYlolzDXu.htm](pfs-season-2-bestiary/8FBm4IjFYlolzDXu.htm)|Lion (PFS 2-13)|
|[8OAeFPKcZGpFDTsv.htm](pfs-season-2-bestiary/8OAeFPKcZGpFDTsv.htm)|Mother Forsythe (5-6)|
|[8PH89qtfwyFvggMq.htm](pfs-season-2-bestiary/8PH89qtfwyFvggMq.htm)|Ember Fox (PFS 2-14)|
|[8WXX6nSrxpry1QYN.htm](pfs-season-2-bestiary/8WXX6nSrxpry1QYN.htm)|Chesjilawa Jadwiga Karina (7-8)|
|[8ZukoQpVoMcshX62.htm](pfs-season-2-bestiary/8ZukoQpVoMcshX62.htm)|Semyon (7-8)|
|[9a72g0wO4E9F46UX.htm](pfs-season-2-bestiary/9a72g0wO4E9F46UX.htm)|Pressurized Bottle (1-2)|
|[9mKcQFcY4Y1g5kJt.htm](pfs-season-2-bestiary/9mKcQFcY4Y1g5kJt.htm)|Ancient Ooze Pit|
|[9X3qkp1qsii5LFQP.htm](pfs-season-2-bestiary/9X3qkp1qsii5LFQP.htm)|Famished Mimic|
|[a4U7IlJCJSxVwl8Q.htm](pfs-season-2-bestiary/a4U7IlJCJSxVwl8Q.htm)|Mushroom Ring (1-2)|
|[A6zDTtZ6fLBLPmCP.htm](pfs-season-2-bestiary/A6zDTtZ6fLBLPmCP.htm)|Greeleep (3-4)|
|[aBxy7czPiVOSIlTl.htm](pfs-season-2-bestiary/aBxy7czPiVOSIlTl.htm)|Russian Soldier (5-6)|
|[ad58hkodIp5UjPa3.htm](pfs-season-2-bestiary/ad58hkodIp5UjPa3.htm)|Mutated Trollhound|
|[AkKxh2OXfWEb5OHt.htm](pfs-season-2-bestiary/AkKxh2OXfWEb5OHt.htm)|Urxehl's Firestorm (7-8)|
|[AMZBCYKz639sHMPZ.htm](pfs-season-2-bestiary/AMZBCYKz639sHMPZ.htm)|Firestorm Brimorak|
|[aqfexx59fbDiR7RH.htm](pfs-season-2-bestiary/aqfexx59fbDiR7RH.htm)|Valgomorus (3-4)|
|[aQqV0cBCyPuMoNAs.htm](pfs-season-2-bestiary/aQqV0cBCyPuMoNAs.htm)|Solvatar Caryg|
|[aQsmJxRJIM0nzw5e.htm](pfs-season-2-bestiary/aQsmJxRJIM0nzw5e.htm)|Corvius Vayn (5-6)|
|[B73Oo8pLdSGKqqOx.htm](pfs-season-2-bestiary/B73Oo8pLdSGKqqOx.htm)|Bloody Vortex (3-4)|
|[bARneaE3aBOFH1ty.htm](pfs-season-2-bestiary/bARneaE3aBOFH1ty.htm)|Explosive Monolith (5-6)|
|[BeenWzuutHRhsoBc.htm](pfs-season-2-bestiary/BeenWzuutHRhsoBc.htm)|Sliding Statue (1-2)|
|[BjJ5KAf1f3SeAesY.htm](pfs-season-2-bestiary/BjJ5KAf1f3SeAesY.htm)|Weakened Abyssal Fungi|
|[BPchAmEdLQtWhe9r.htm](pfs-season-2-bestiary/BPchAmEdLQtWhe9r.htm)|Animated Guardian Warrior|
|[BSbqNkNSs5IMu6RO.htm](pfs-season-2-bestiary/BSbqNkNSs5IMu6RO.htm)|Usij Acolyte|
|[BXnmDgVkhD97fI1o.htm](pfs-season-2-bestiary/BXnmDgVkhD97fI1o.htm)|Anatomical Model (1-2)|
|[bzV9QF6CfIGX8aNR.htm](pfs-season-2-bestiary/bzV9QF6CfIGX8aNR.htm)|Aspis Veteran Guard|
|[C0UQiPJLDm7vb0ZV.htm](pfs-season-2-bestiary/C0UQiPJLDm7vb0ZV.htm)|Master Sergeant Morgroar (5-6)|
|[c4KECFBOIXqAagwm.htm](pfs-season-2-bestiary/c4KECFBOIXqAagwm.htm)|Mishka The Bear (7-8)|
|[c4t6aDgo7pdMI7yN.htm](pfs-season-2-bestiary/c4t6aDgo7pdMI7yN.htm)|Mercenary Wizard (1-2)|
|[c7ppBFpsmLFP1YD5.htm](pfs-season-2-bestiary/c7ppBFpsmLFP1YD5.htm)|Corrupted Wildfire (5-6)|
|[c7uwPrgkjqSKRXQG.htm](pfs-season-2-bestiary/c7uwPrgkjqSKRXQG.htm)|Qxal, The Thorned Monarch|
|[ckvfamrYKeUYX7Cl.htm](pfs-season-2-bestiary/ckvfamrYKeUYX7Cl.htm)|Starved Sea Drake|
|[Cl2RNfFj3iuc1xp3.htm](pfs-season-2-bestiary/Cl2RNfFj3iuc1xp3.htm)|Chained Troll|
|[CmMK9sadwbxfBaYg.htm](pfs-season-2-bestiary/CmMK9sadwbxfBaYg.htm)|Yeth Warbeast|
|[cRrHxzUAZvbhgcDr.htm](pfs-season-2-bestiary/cRrHxzUAZvbhgcDr.htm)|Scorched Earth Orc (Scout) (7-8)|
|[cWenvBwMHSnN0sHV.htm](pfs-season-2-bestiary/cWenvBwMHSnN0sHV.htm)|Explosive Monolith (3-4)|
|[cx796RgvywxF8P1A.htm](pfs-season-2-bestiary/cx796RgvywxF8P1A.htm)|Flaming Moss Monster|
|[CXSThQwaRg1eEurb.htm](pfs-season-2-bestiary/CXSThQwaRg1eEurb.htm)|Master Sergeant Morgroar (3-4)|
|[D0eC5CwcBAsSoAJC.htm](pfs-season-2-bestiary/D0eC5CwcBAsSoAJC.htm)|Fence (PFS 2-00)|
|[D4B3dR8y5h9QIeqi.htm](pfs-season-2-bestiary/D4B3dR8y5h9QIeqi.htm)|Disciple of Urxehl (7-8)|
|[DlIJbeLe4iNKMOQd.htm](pfs-season-2-bestiary/DlIJbeLe4iNKMOQd.htm)|Run-Down Efreet|
|[dnClDBmInobiLLDY.htm](pfs-season-2-bestiary/dnClDBmInobiLLDY.htm)|Stranded Melody (7-8)|
|[dqPqUX69bPiVdMNG.htm](pfs-season-2-bestiary/dqPqUX69bPiVdMNG.htm)|Lesser Bruorsivi Mandragora|
|[DVqAA7JxUykHG2oT.htm](pfs-season-2-bestiary/DVqAA7JxUykHG2oT.htm)|Empowered Blood (3-4)|
|[dXvAp2TjUxpE16nS.htm](pfs-season-2-bestiary/dXvAp2TjUxpE16nS.htm)|Zombie Grindylow|
|[DyADskqXgDZlRWz3.htm](pfs-season-2-bestiary/DyADskqXgDZlRWz3.htm)|Urxehl's Firestorm (9-10)|
|[DYEJU8PPBg6yWGJe.htm](pfs-season-2-bestiary/DYEJU8PPBg6yWGJe.htm)|Mercenary Trapper (3-4)|
|[E0ShG9xKRN88vUdP.htm](pfs-season-2-bestiary/E0ShG9xKRN88vUdP.htm)|Wounded Dragon Turtle (5-6)|
|[E0V3aKLWuXyWrY8C.htm](pfs-season-2-bestiary/E0V3aKLWuXyWrY8C.htm)|Demoniac Spirit (7-8)|
|[E3KaNlIYwSD4fpfL.htm](pfs-season-2-bestiary/E3KaNlIYwSD4fpfL.htm)|Dragon's Death (1-2)|
|[E8mFpCpTrhiBfYcp.htm](pfs-season-2-bestiary/E8mFpCpTrhiBfYcp.htm)|Smoldering Moss Monster|
|[EK8DKarizcVrLG5E.htm](pfs-season-2-bestiary/EK8DKarizcVrLG5E.htm)|Demoniac Spirit (9-10)|
|[encctJrtSpWpAQaV.htm](pfs-season-2-bestiary/encctJrtSpWpAQaV.htm)|Abyssal Firestorm Surge (9-10)|
|[ErhCUif0jmnDKogH.htm](pfs-season-2-bestiary/ErhCUif0jmnDKogH.htm)|Collapsing Cabinet (1-2)|
|[EVWVIc2zNT18kZ2T.htm](pfs-season-2-bestiary/EVWVIc2zNT18kZ2T.htm)|Weak Lion|
|[F22a7CKj2leYfxyo.htm](pfs-season-2-bestiary/F22a7CKj2leYfxyo.htm)|Hobgoblin Skeleton|
|[f4tnLJMEjq9IUMNZ.htm](pfs-season-2-bestiary/f4tnLJMEjq9IUMNZ.htm)|Urkhas (3-4)|
|[FCs8aeSiwqnSyTMF.htm](pfs-season-2-bestiary/FCs8aeSiwqnSyTMF.htm)|Wind Malevolent (3-4)|
|[fF28drhtacyoHU96.htm](pfs-season-2-bestiary/fF28drhtacyoHU96.htm)|Skeletal Reveler|
|[fHDBl7jIleuzmOM2.htm](pfs-season-2-bestiary/fHDBl7jIleuzmOM2.htm)|Scorched Earth Orc (Sharpshooter) (5-6)|
|[fNX8O2z4d3peG5zJ.htm](pfs-season-2-bestiary/fNX8O2z4d3peG5zJ.htm)|Foretold Ruin (3-4)|
|[fpKKNDabU5XPov30.htm](pfs-season-2-bestiary/fpKKNDabU5XPov30.htm)|Ash Skeleton (1-2)|
|[fSeiGTAcD0mVUz5w.htm](pfs-season-2-bestiary/fSeiGTAcD0mVUz5w.htm)|Corrupted Wildfire (3-4)|
|[gAoKDxmIriTC7xVn.htm](pfs-season-2-bestiary/gAoKDxmIriTC7xVn.htm)|Churning Lava (1-2)|
|[ggYXvNjK12pVwJ0P.htm](pfs-season-2-bestiary/ggYXvNjK12pVwJ0P.htm)|Goblin Skeleton|
|[GIJOVHi9G7tcoMds.htm](pfs-season-2-bestiary/GIJOVHi9G7tcoMds.htm)|Lesser Skeletal Cyclops|
|[gKAUuRJoKEoYo3Ub.htm](pfs-season-2-bestiary/gKAUuRJoKEoYo3Ub.htm)|Flames of Verakivhan (5-6)|
|[GL9QRBCuNKjuvZSE.htm](pfs-season-2-bestiary/GL9QRBCuNKjuvZSE.htm)|Clouded Quartz (7-8)|
|[GopDDbkjVhXu4cwt.htm](pfs-season-2-bestiary/GopDDbkjVhXu4cwt.htm)|Plague Zombie (PFS 2-15)|
|[gOPvMgZIQXidd1n8.htm](pfs-season-2-bestiary/gOPvMgZIQXidd1n8.htm)|Demonplague Zombie|
|[GPEyzpKkfrZIIF9L.htm](pfs-season-2-bestiary/GPEyzpKkfrZIIF9L.htm)|Desiccated Giant Crawling Hand|
|[H8kJUgCxKicEMUFz.htm](pfs-season-2-bestiary/H8kJUgCxKicEMUFz.htm)|Mushroom Ring (5-6)|
|[hCZIkwJQQTMwdjfy.htm](pfs-season-2-bestiary/hCZIkwJQQTMwdjfy.htm)|Tough Small Opossum|
|[hSUo6Y5Cc26Eckd7.htm](pfs-season-2-bestiary/hSUo6Y5Cc26Eckd7.htm)|Onyx Alliance Agent (5-6)|
|[i3mgIwXZeJxwFWNI.htm](pfs-season-2-bestiary/i3mgIwXZeJxwFWNI.htm)|Planar Nixie|
|[ib1c9CIOEXxnEfQG.htm](pfs-season-2-bestiary/ib1c9CIOEXxnEfQG.htm)|Hand Of Urxehl (9-10)|
|[iEQyYzFBvgyD5yBy.htm](pfs-season-2-bestiary/iEQyYzFBvgyD5yBy.htm)|Wind Malevolent (5-6)|
|[IF1DWtelPtUF5aKh.htm](pfs-season-2-bestiary/IF1DWtelPtUF5aKh.htm)|Scorched Cavern Troll|
|[IHh8MUVqP315OCZ4.htm](pfs-season-2-bestiary/IHh8MUVqP315OCZ4.htm)|Blighted Fungus Leshy|
|[iqGqGi066OvLdgKb.htm](pfs-season-2-bestiary/iqGqGi066OvLdgKb.htm)|The Saboteur (3-4)|
|[iQprqoS2RXWPMAMQ.htm](pfs-season-2-bestiary/iQprqoS2RXWPMAMQ.htm)|Twigjack (PFS 2-08)|
|[iryp7aFx7CUaIksJ.htm](pfs-season-2-bestiary/iryp7aFx7CUaIksJ.htm)|Daughter of Cocytus Tough|
|[iuHyixlsXUA4KJ9Q.htm](pfs-season-2-bestiary/iuHyixlsXUA4KJ9Q.htm)|Tough Skeletal Reveler|
|[IxDPownROvUhSU21.htm](pfs-season-2-bestiary/IxDPownROvUhSU21.htm)|Ash Skeleton (3-4)|
|[j7K9SefeHJ16qHMl.htm](pfs-season-2-bestiary/j7K9SefeHJ16qHMl.htm)|Anatomical Model (3-4)|
|[jCyYRx0Kkaf0TNfF.htm](pfs-season-2-bestiary/jCyYRx0Kkaf0TNfF.htm)|Electric Lock Rune|
|[jFfxjbvNtfWyHHCK.htm](pfs-season-2-bestiary/jFfxjbvNtfWyHHCK.htm)|Bloodthirsty Razortoothed Shark|
|[Jie7zk3ct2AE92Om.htm](pfs-season-2-bestiary/Jie7zk3ct2AE92Om.htm)|Ssalarn|
|[jjx1CL2NpFG78tOZ.htm](pfs-season-2-bestiary/jjx1CL2NpFG78tOZ.htm)|Small Opossum|
|[JxJ3vbV7piLxZrJ9.htm](pfs-season-2-bestiary/JxJ3vbV7piLxZrJ9.htm)|The Scholar of Sorts (3-4)|
|[jZ5M9JxemkGVpfLy.htm](pfs-season-2-bestiary/jZ5M9JxemkGVpfLy.htm)|Awakened Giant Frilled Lizard|
|[KJAk6ALQk9Qkt53l.htm](pfs-season-2-bestiary/KJAk6ALQk9Qkt53l.htm)|Kobold Warrior (PFS 2-11)|
|[Kklhv0OWqkb6RbIF.htm](pfs-season-2-bestiary/Kklhv0OWqkb6RbIF.htm)|Cobbleswarm (B5)|
|[kRCNfSMqwHY9sYcg.htm](pfs-season-2-bestiary/kRCNfSMqwHY9sYcg.htm)|Semyon (5-6)|
|[KvFN6OFxOnTYjdJq.htm](pfs-season-2-bestiary/KvFN6OFxOnTYjdJq.htm)|Perizia (5-6)|
|[KVvRtfyqewytq40f.htm](pfs-season-2-bestiary/KVvRtfyqewytq40f.htm)|Hunting Spider (PFS 2-18)|
|[LdZgV7b5BhCF0Ye6.htm](pfs-season-2-bestiary/LdZgV7b5BhCF0Ye6.htm)|Collapsing Ramp (7-8)|
|[ljs6Ueki3nmInJ9v.htm](pfs-season-2-bestiary/ljs6Ueki3nmInJ9v.htm)|Archis Peers (3-4)|
|[LxPHOPYyrNhDNFod.htm](pfs-season-2-bestiary/LxPHOPYyrNhDNFod.htm)|Chained Drained Troll|
|[LZiiTvbxfgJ3QfIQ.htm](pfs-season-2-bestiary/LZiiTvbxfgJ3QfIQ.htm)|Ice Slick|
|[M1mbQnk40lu1lxdr.htm](pfs-season-2-bestiary/M1mbQnk40lu1lxdr.htm)|Young Ahuizotl|
|[M9AglEGo8jMH9CnU.htm](pfs-season-2-bestiary/M9AglEGo8jMH9CnU.htm)|Churning Lava (3-4)|
|[mFfhFprmaQr9rYC1.htm](pfs-season-2-bestiary/mFfhFprmaQr9rYC1.htm)|Drandle Dreng (7-8)|
|[mfvNh0UBzR1vXHRW.htm](pfs-season-2-bestiary/mfvNh0UBzR1vXHRW.htm)|Hands of Slow Death|
|[mot8pWHD6YEj08Js.htm](pfs-season-2-bestiary/mot8pWHD6YEj08Js.htm)|Sea Devil Impaler|
|[mREQ0kbYYSYsYb4e.htm](pfs-season-2-bestiary/mREQ0kbYYSYsYb4e.htm)|Weak Mist Stalker|
|[nA6U7TiEPyEbcio6.htm](pfs-season-2-bestiary/nA6U7TiEPyEbcio6.htm)|Mushroom Ring (3-4)|
|[NAUPuR7aWSQ6mBUs.htm](pfs-season-2-bestiary/NAUPuR7aWSQ6mBUs.htm)|Usij Cultist|
|[NAX7nXWzSkrjjsrq.htm](pfs-season-2-bestiary/NAX7nXWzSkrjjsrq.htm)|Awakened Megalania|
|[NFKQObG0b0lVsvgS.htm](pfs-season-2-bestiary/NFKQObG0b0lVsvgS.htm)|Unstable Ice|
|[NGNU5Rq90TmNhwhN.htm](pfs-season-2-bestiary/NGNU5Rq90TmNhwhN.htm)|Kayajima Guardian Dogu (3-4)|
|[NioUzYo4svyO981f.htm](pfs-season-2-bestiary/NioUzYo4svyO981f.htm)|Relive the Rending (1-2)|
|[NMD2wE4GLfoS41IB.htm](pfs-season-2-bestiary/NMD2wE4GLfoS41IB.htm)|Wishbound Mist Stalker (7-8)|
|[nSFA4Kei2Gyf7dc6.htm](pfs-season-2-bestiary/nSFA4Kei2Gyf7dc6.htm)|Murta Kronniksdottir (5-6)|
|[nu3o0lcRZ6Bk57pA.htm](pfs-season-2-bestiary/nu3o0lcRZ6Bk57pA.htm)|Iverri (5-6)|
|[nUweXdZmsMFLcEsM.htm](pfs-season-2-bestiary/nUweXdZmsMFLcEsM.htm)|Weak Sea Devil Scout|
|[o13KISidrNovd4jp.htm](pfs-season-2-bestiary/o13KISidrNovd4jp.htm)|Cyclops Bodak|
|[O78tQX7291q1CADm.htm](pfs-season-2-bestiary/O78tQX7291q1CADm.htm)|Bruorsivi Mandragora|
|[OMEVkAFReRtjj1zb.htm](pfs-season-2-bestiary/OMEVkAFReRtjj1zb.htm)|Sticky Web Lurker Noose|
|[OSNYHI3K4Ir8DVof.htm](pfs-season-2-bestiary/OSNYHI3K4Ir8DVof.htm)|Planar Cracks (7-8)|
|[P8fk8icXxlR6VVcm.htm](pfs-season-2-bestiary/P8fk8icXxlR6VVcm.htm)|Mercenary Wizard (3-4)|
|[p9guvHB0pj7Q4tOx.htm](pfs-season-2-bestiary/p9guvHB0pj7Q4tOx.htm)|Drandlesticks (7-8)|
|[P9xoZGN9pSqNPbRK.htm](pfs-season-2-bestiary/P9xoZGN9pSqNPbRK.htm)|Mechanical Jaws (1-2)|
|[pe7Pb0vsbah7L299.htm](pfs-season-2-bestiary/pe7Pb0vsbah7L299.htm)|Flames of Verakivhan (3-4)|
|[peEfisGfqzcCxaRf.htm](pfs-season-2-bestiary/peEfisGfqzcCxaRf.htm)|Urkhas (5-6)|
|[PFRXopyGSJmdYE6Y.htm](pfs-season-2-bestiary/PFRXopyGSJmdYE6Y.htm)|Kayajima Guardian Dogu (5-6)|
|[PH7M0jJ88cdGaNq9.htm](pfs-season-2-bestiary/PH7M0jJ88cdGaNq9.htm)|Dragon's Death (3-4)|
|[PIAR4xFEhkWlWg8D.htm](pfs-season-2-bestiary/PIAR4xFEhkWlWg8D.htm)|Hidden Bog|
|[pmuLQSwB0530m873.htm](pfs-season-2-bestiary/pmuLQSwB0530m873.htm)|Daughter of Cocytus Grunt|
|[pNPUo3lDaw7ZoDek.htm](pfs-season-2-bestiary/pNPUo3lDaw7ZoDek.htm)|Chaotic Door (3-4)|
|[psJpwY07YDdsQsgL.htm](pfs-season-2-bestiary/psJpwY07YDdsQsgL.htm)|Planar Cracks (5-6)|
|[PUb8Ipy6NKAMDFQw.htm](pfs-season-2-bestiary/PUb8Ipy6NKAMDFQw.htm)|Skeletal Cyclops|
|[pUcBzyZY5kY7ERNN.htm](pfs-season-2-bestiary/pUcBzyZY5kY7ERNN.htm)|Perizia (7-8)|
|[PZ0PjHKcmcekBTSp.htm](pfs-season-2-bestiary/PZ0PjHKcmcekBTSp.htm)|Ogthup (5-6)|
|[q5GtWMOvluZVt8Gx.htm](pfs-season-2-bestiary/q5GtWMOvluZVt8Gx.htm)|Mercenary Trapper (1-2)|
|[q5ObT0toaicC2OyP.htm](pfs-season-2-bestiary/q5ObT0toaicC2OyP.htm)|Nuglub (PFS 2-00)|
|[Qdx9UbT68Elqneti.htm](pfs-season-2-bestiary/Qdx9UbT68Elqneti.htm)|Debilitated Bulette|
|[QpNynyK54qEwmswI.htm](pfs-season-2-bestiary/QpNynyK54qEwmswI.htm)|Frost Troll Hunter|
|[QQ93gOZxsfTdTSn2.htm](pfs-season-2-bestiary/QQ93gOZxsfTdTSn2.htm)|Storm Bear (3-4)|
|[qZFgHBqfjIaBviTS.htm](pfs-season-2-bestiary/qZFgHBqfjIaBviTS.htm)|Aspis Guard|
|[R1QGjRlquOmfH2Zx.htm](pfs-season-2-bestiary/R1QGjRlquOmfH2Zx.htm)|Exploding Skeleton Guard|
|[R58hBfz4R5QQtFnD.htm](pfs-season-2-bestiary/R58hBfz4R5QQtFnD.htm)|Awakened Ball Python|
|[R7LYEfEApIhp6jdp.htm](pfs-season-2-bestiary/R7LYEfEApIhp6jdp.htm)|Joyful Planar Nixie|
|[rCErTabCxzPbR0ON.htm](pfs-season-2-bestiary/rCErTabCxzPbR0ON.htm)|Insidious Dragon Forest Rot|
|[rFh82u8Jb6ZaGkm3.htm](pfs-season-2-bestiary/rFh82u8Jb6ZaGkm3.htm)|Artennod Raike (5-6)|
|[rFRbYNAH3LXRXXrS.htm](pfs-season-2-bestiary/rFRbYNAH3LXRXXrS.htm)|Silaqui (3-4)|
|[RmcCPvmrAeSTx3BR.htm](pfs-season-2-bestiary/RmcCPvmrAeSTx3BR.htm)|Burning Sun Orc (7-8)|
|[RO4UAvgak0jAo2We.htm](pfs-season-2-bestiary/RO4UAvgak0jAo2We.htm)|Burning Sun Orc (5-6)|
|[SFBumlEX6jDIwAvL.htm](pfs-season-2-bestiary/SFBumlEX6jDIwAvL.htm)|Saddleback Bunyip|
|[SgRcBE1CNLlGXbwz.htm](pfs-season-2-bestiary/SgRcBE1CNLlGXbwz.htm)|Silaqui (5-6)|
|[SHmxvGZAPxkttd06.htm](pfs-season-2-bestiary/SHmxvGZAPxkttd06.htm)|Wounded Dragon Turtle (3-4)|
|[sm6SKZaGvDSAAwde.htm](pfs-season-2-bestiary/sm6SKZaGvDSAAwde.htm)|Sezruth (1-2)|
|[sOTeRfQ6NB3CtqBX.htm](pfs-season-2-bestiary/sOTeRfQ6NB3CtqBX.htm)|Quickling (PFS 2-00)|
|[sRXkrqt5mOXzceR5.htm](pfs-season-2-bestiary/sRXkrqt5mOXzceR5.htm)|Dragon Forest Rot|
|[StqA0Lpk26G4Ux1l.htm](pfs-season-2-bestiary/StqA0Lpk26G4Ux1l.htm)|The Saboteur (5-6)|
|[SX348ZSY0SyMfOXa.htm](pfs-season-2-bestiary/SX348ZSY0SyMfOXa.htm)|Flaming Eye (3-4)|
|[SXXftJEy3DL8XXfF.htm](pfs-season-2-bestiary/SXXftJEy3DL8XXfF.htm)|Scorched Earth Orc (Clipper) (5-6)|
|[Syfo21iDW5wnYkLS.htm](pfs-season-2-bestiary/Syfo21iDW5wnYkLS.htm)|Stranded Melody (5-6)|
|[T2IXTYeb4Ky6l1D8.htm](pfs-season-2-bestiary/T2IXTYeb4Ky6l1D8.htm)|Relive the Rending (3-4)|
|[tLCIfeC1WWuGB9zj.htm](pfs-season-2-bestiary/tLCIfeC1WWuGB9zj.htm)|Baron Utomo (3-4)|
|[TX7WPf5f7lq3AufA.htm](pfs-season-2-bestiary/TX7WPf5f7lq3AufA.htm)|Jinkin (PFS 2-00)|
|[TXHl1KvScSce9bQV.htm](pfs-season-2-bestiary/TXHl1KvScSce9bQV.htm)|Drandle Dreng (9-10)|
|[tY5snIGj4jBSJcay.htm](pfs-season-2-bestiary/tY5snIGj4jBSJcay.htm)|Ogre Spider (PFS 2-18)|
|[U3RoZnfVAAKbrULX.htm](pfs-season-2-bestiary/U3RoZnfVAAKbrULX.htm)|Disciple of Urxehl (9-10)|
|[U6gghw2Ke21cRzs0.htm](pfs-season-2-bestiary/U6gghw2Ke21cRzs0.htm)|Brittle Skeletal Horse|
|[UIAaR0AdABRgGGNc.htm](pfs-season-2-bestiary/UIAaR0AdABRgGGNc.htm)|Chesjilawa Jadwiga Karina (5-6)|
|[UkcHfRJUx1OHELZm.htm](pfs-season-2-bestiary/UkcHfRJUx1OHELZm.htm)|Duergar Raider|
|[UNdSfmCEICYna9ki.htm](pfs-season-2-bestiary/UNdSfmCEICYna9ki.htm)|Foretold Ruin (1-2)|
|[unfgNRDtbFivlOEJ.htm](pfs-season-2-bestiary/unfgNRDtbFivlOEJ.htm)|Weak Mudwretch|
|[UngnjxTXgiKzBTuB.htm](pfs-season-2-bestiary/UngnjxTXgiKzBTuB.htm)|Storasta's Last Stand (5-6)|
|[uROqM79s9ivzHzl3.htm](pfs-season-2-bestiary/uROqM79s9ivzHzl3.htm)|Ash Archer|
|[urrUBt8FFtuw3LNv.htm](pfs-season-2-bestiary/urrUBt8FFtuw3LNv.htm)|Drandlesticks (9-10)|
|[UtMuL2qXMHxt2ccB.htm](pfs-season-2-bestiary/UtMuL2qXMHxt2ccB.htm)|Mushroom Ring (7-8)|
|[V5O0ecsMk0QSTpqJ.htm](pfs-season-2-bestiary/V5O0ecsMk0QSTpqJ.htm)|Crumbling Ravener Husk|
|[VbRX9bK5kLIJWnXH.htm](pfs-season-2-bestiary/VbRX9bK5kLIJWnXH.htm)|Sezruth (3-4)|
|[VjVKGrVXJd7HRn5h.htm](pfs-season-2-bestiary/VjVKGrVXJd7HRn5h.htm)|Chaotic Door (1-2)|
|[vqgcpFLXabsGj7RQ.htm](pfs-season-2-bestiary/vqgcpFLXabsGj7RQ.htm)|Awakened Giant Chameleon|
|[VWjNkgTXFPqO0Qc5.htm](pfs-season-2-bestiary/VWjNkgTXFPqO0Qc5.htm)|Pressurized Bottle (3-4)|
|[W9XSFbMIXk1A7aT9.htm](pfs-season-2-bestiary/W9XSFbMIXk1A7aT9.htm)|Scorched Earth Mephit|
|[WEo5SYs2SPZkU6lu.htm](pfs-season-2-bestiary/WEo5SYs2SPZkU6lu.htm)|Animated Guardian|
|[WisQuxckwd6XU5MF.htm](pfs-season-2-bestiary/WisQuxckwd6XU5MF.htm)|Sliding Statue (3-4)|
|[wt2bOysQCOVDRGJu.htm](pfs-season-2-bestiary/wt2bOysQCOVDRGJu.htm)|Wishbound Mist Stalker (5-6)|
|[wTr7zLN8is9720ub.htm](pfs-season-2-bestiary/wTr7zLN8is9720ub.htm)|Mechanical Jaws (3-4)|
|[wYPS72mQp7VqullU.htm](pfs-season-2-bestiary/wYPS72mQp7VqullU.htm)|Artennod Raike (3-4)|
|[X0HPqlpqFHqmB9Ni.htm](pfs-season-2-bestiary/X0HPqlpqFHqmB9Ni.htm)|Valgomorus (1-2)|
|[x4m5ks6Rd8fYzXPm.htm](pfs-season-2-bestiary/x4m5ks6Rd8fYzXPm.htm)|Daughter Of Cocytus Enforcer|
|[X6ODple6SbEvbmmj.htm](pfs-season-2-bestiary/X6ODple6SbEvbmmj.htm)|Corvius Vayn (7-8)|
|[X7YcO5G0hpDP46EW.htm](pfs-season-2-bestiary/X7YcO5G0hpDP46EW.htm)|Greeleep (5-6)|
|[X951Ow3nHOvBFaFg.htm](pfs-season-2-bestiary/X951Ow3nHOvBFaFg.htm)|Mephit Swarm (7-8)|
|[Xgt8hV1WTSxQLv7y.htm](pfs-season-2-bestiary/Xgt8hV1WTSxQLv7y.htm)|Collapsing Ice|
|[XKrlU3hiLBTPg95p.htm](pfs-season-2-bestiary/XKrlU3hiLBTPg95p.htm)|Pixie (PFS 2-00)|
|[xmKHQAQ9z7XrPNp7.htm](pfs-season-2-bestiary/xmKHQAQ9z7XrPNp7.htm)|Russian Soldier (7-8)|
|[XoE0tsQ4twaoNtBu.htm](pfs-season-2-bestiary/XoE0tsQ4twaoNtBu.htm)|Collapsing Ramp (9-10)|
|[xRwAQ93HjGe59RvO.htm](pfs-season-2-bestiary/xRwAQ93HjGe59RvO.htm)|Ygracix|
|[XUyHffBEQ7K2bsiP.htm](pfs-season-2-bestiary/XUyHffBEQ7K2bsiP.htm)|Flooding Monolith (5-6)|
|[XVda3dqZWi21we2j.htm](pfs-season-2-bestiary/XVda3dqZWi21we2j.htm)|Ragewight (3-4)|
|[xW5IhEbAhQneniT2.htm](pfs-season-2-bestiary/xW5IhEbAhQneniT2.htm)|Storm Bear (1-2)|
|[XZOpmROxLS1bnApy.htm](pfs-season-2-bestiary/XZOpmROxLS1bnApy.htm)|Mother Forsythe (3-4)|
|[Y0qzWOi3ihBHwYWE.htm](pfs-season-2-bestiary/Y0qzWOi3ihBHwYWE.htm)|Ferocious Zombie Grindylow|
|[Y2tEkaZK5j2NySJu.htm](pfs-season-2-bestiary/Y2tEkaZK5j2NySJu.htm)|Sinkhole|
|[yCACZfj6PJJGCLcr.htm](pfs-season-2-bestiary/yCACZfj6PJJGCLcr.htm)|Murta Kronniksdottir (3-4)|
|[yEvWtmFxwVsNGjWh.htm](pfs-season-2-bestiary/yEvWtmFxwVsNGjWh.htm)|Unkillable Zombie Shambler|
|[YpACDyWCHXtaXppg.htm](pfs-season-2-bestiary/YpACDyWCHXtaXppg.htm)|Scorched Earth Orc (Clipper) (7-8)|
|[yQm21z0RyZgZWcpm.htm](pfs-season-2-bestiary/yQm21z0RyZgZWcpm.htm)|Baron Utomo (5-6)|
|[yqOuv6qNnJgCFPn1.htm](pfs-season-2-bestiary/yqOuv6qNnJgCFPn1.htm)|Spark Troll (3-4)|
|[z5YeacI4avkX3etA.htm](pfs-season-2-bestiary/z5YeacI4avkX3etA.htm)|Iverri (3-4)|
|[Z6qNXxgTgIz4rH2C.htm](pfs-season-2-bestiary/Z6qNXxgTgIz4rH2C.htm)|Mishka The Bear (5-6)|
|[zihTF7JohmZM8cdS.htm](pfs-season-2-bestiary/zihTF7JohmZM8cdS.htm)|Giant Worker Ant|
|[zLeePJplf25baqJa.htm](pfs-season-2-bestiary/zLeePJplf25baqJa.htm)|Charmed Crab|
|[zNN1Af6Xx5Tmglcu.htm](pfs-season-2-bestiary/zNN1Af6Xx5Tmglcu.htm)|Tewakam Nekotek (5-6)|
|[zpIVn1k5nsR0sMZ8.htm](pfs-season-2-bestiary/zpIVn1k5nsR0sMZ8.htm)|Ragewight (5-6)|
|[ZpSLUDoO0WmxwLCG.htm](pfs-season-2-bestiary/ZpSLUDoO0WmxwLCG.htm)|Onyx Alliance Agent (7-8)|
|[ZshsztmoWjDZEqGT.htm](pfs-season-2-bestiary/ZshsztmoWjDZEqGT.htm)|Elite Anatomical Model (3-4)|
|[zSuXInzH5E8rYNEl.htm](pfs-season-2-bestiary/zSuXInzH5E8rYNEl.htm)|Vermlek Centaur (5-6)|
|[ZzBtsGGjEvlqpiDq.htm](pfs-season-2-bestiary/ZzBtsGGjEvlqpiDq.htm)|Strak (7-8)|
|[ZzCskCNtjDetelAi.htm](pfs-season-2-bestiary/ZzCskCNtjDetelAi.htm)|Storasta's Last Stand (3-4)|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[Ek6C2abooQizETsX.htm](pfs-season-2-bestiary/Ek6C2abooQizETsX.htm)|Draugr Raider|Pilleur Draugr|libre|
|[qG5e77fMVkjBlK8B.htm](pfs-season-2-bestiary/qG5e77fMVkjBlK8B.htm)|Commoner (PFS 2-13)|Roturier ((PFS 2-13))|libre|
|[SqAWqJEi3IVB5TbO.htm](pfs-season-2-bestiary/SqAWqJEi3IVB5TbO.htm)|Kayajima Daeodon|Daeodon Kayajima|libre|
