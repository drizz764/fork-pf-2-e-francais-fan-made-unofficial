# État de la traduction (menace-under-otari-bestiary)

 * **officielle**: 23
 * **changé**: 46
 * **libre**: 1


Dernière mise à jour: 2024-06-13 06:52 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[7AzIsyvVOg19fSoa.htm](menace-under-otari-bestiary/7AzIsyvVOg19fSoa.htm)|Caligni Skulker (BB)|
|[7EuWv6tGtOASvzbG.htm](menace-under-otari-bestiary/7EuWv6tGtOASvzbG.htm)|Caligni Dancer (BB)|
|[CF82XJwObLx0TPnV.htm](menace-under-otari-bestiary/CF82XJwObLx0TPnV.htm)|Warg (BB)|
|[eq4tLYV3efCS2ouP.htm](menace-under-otari-bestiary/eq4tLYV3efCS2ouP.htm)|Reefclaw (BB)|
|[Rr1wwJ1jIIhRZbXh.htm](menace-under-otari-bestiary/Rr1wwJ1jIIhRZbXh.htm)|Caligni Hunter (BB)|
|[wCmlY4TixUlPm5Qx.htm](menace-under-otari-bestiary/wCmlY4TixUlPm5Qx.htm)|Minotaur Hunter (BB)|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0plBflWwrCWQO2RO.htm](menace-under-otari-bestiary/0plBflWwrCWQO2RO.htm)|Zombie Shambler (BB)|Titubeur Zombie (BI)|changé|
|[2vyM10zN0JYdzyxt.htm](menace-under-otari-bestiary/2vyM10zN0JYdzyxt.htm)|Giant Ant (BB)|Fourmi géante (BI)|changé|
|[4Axci50gPQArPg7d.htm](menace-under-otari-bestiary/4Axci50gPQArPg7d.htm)|Kobold Trapmaster (BB)|Kobold Maître-des-pièges (BI)|changé|
|[5MVBU86ZRw2ANMQn.htm](menace-under-otari-bestiary/5MVBU86ZRw2ANMQn.htm)|Skeletal Giant (BB)|Squelette Géant (BI)|changé|
|[6NdqvKIlxo4cGhf8.htm](menace-under-otari-bestiary/6NdqvKIlxo4cGhf8.htm)|Giant Spider (BB)|Araignée géante (BI)|changé|
|[93hZtLl9pRRfqI05.htm](menace-under-otari-bestiary/93hZtLl9pRRfqI05.htm)|Kobold Dragon Mage (Blue, BB)|Mage draconique bleu kobold (BI)|changé|
|[9sa2KE4Fbh3OPH7M.htm](menace-under-otari-bestiary/9sa2KE4Fbh3OPH7M.htm)|Brine Shark (BB)|Requin de saumure (BI)|changé|
|[aeCoh4u6c5kt1iCs.htm](menace-under-otari-bestiary/aeCoh4u6c5kt1iCs.htm)|Gargoyle (BB)|Gargouille (BI)|changé|
|[AuCC04X2AO8oFN75.htm](menace-under-otari-bestiary/AuCC04X2AO8oFN75.htm)|Harpy (BB)|Harpie (BI)|changé|
|[BKPRkJgq7ehsW7uX.htm](menace-under-otari-bestiary/BKPRkJgq7ehsW7uX.htm)|Giant Centipede (BB)|Mille-pattes géant (BI)|changé|
|[cBHpMcVaLRPZu9po.htm](menace-under-otari-bestiary/cBHpMcVaLRPZu9po.htm)|Zephyr Hawk (BB)|Faucon zéphyr (BI)|changé|
|[CJuHwIRCAgTB1SEl.htm](menace-under-otari-bestiary/CJuHwIRCAgTB1SEl.htm)|Kobold Dragon Mage (Red, BB)|Mage draconique rouge kobold (BI)|changé|
|[EtRqBsWh1Hv1toqh.htm](menace-under-otari-bestiary/EtRqBsWh1Hv1toqh.htm)|Orc Veteran (BB)|Orc soldat (BI)|changé|
|[gvCCATlH9mPGWbsp.htm](menace-under-otari-bestiary/gvCCATlH9mPGWbsp.htm)|Forest Troll (BB)|Troll (BI)|changé|
|[hiGwRWdxAsoCII4f.htm](menace-under-otari-bestiary/hiGwRWdxAsoCII4f.htm)|Cinder Rat (BB)|Rat des braises (BI)|changé|
|[jeAGl6OAVrrIPgu3.htm](menace-under-otari-bestiary/jeAGl6OAVrrIPgu3.htm)|Hell Hound (BB)|Molosse infernal (BI)|changé|
|[jGzVwekcRX5aQpbT.htm](menace-under-otari-bestiary/jGzVwekcRX5aQpbT.htm)|Goblin Commando (BB)|Gobelin Commando (BI)|changé|
|[jnmUcTs4hn1c5bz9.htm](menace-under-otari-bestiary/jnmUcTs4hn1c5bz9.htm)|Pugwampi (BB)|Pugwampi (BI)|changé|
|[jVZRROs0GzDjVrgi.htm](menace-under-otari-bestiary/jVZRROs0GzDjVrgi.htm)|Goblin Warrior (BB)|Gobelin Guerrier (BI)|changé|
|[kCRfBZqCugMQmdpd.htm](menace-under-otari-bestiary/kCRfBZqCugMQmdpd.htm)|Kobold Dragon Mage (White, BB)|Mage draconique blanc kobold (BI)|changé|
|[KsWAIXTTh3mfNWOY.htm](menace-under-otari-bestiary/KsWAIXTTh3mfNWOY.htm)|Giant Viper (BB)|Vipère géante (BI)|changé|
|[lFlXmieuHTBIonhj.htm](menace-under-otari-bestiary/lFlXmieuHTBIonhj.htm)|Viper (BB)|Vipère (BI)|changé|
|[LHHgGSs0ELCR4CYK.htm](menace-under-otari-bestiary/LHHgGSs0ELCR4CYK.htm)|Ghoul Stalker (BB)|Goule (BI)|changé|
|[M8oJOKJ4AgrLZcJQ.htm](menace-under-otari-bestiary/M8oJOKJ4AgrLZcJQ.htm)|Hobgoblin Soldier (BB)|Hobgobelin Guerrier (BI)|changé|
|[NVWaLagWOu5tCCZu.htm](menace-under-otari-bestiary/NVWaLagWOu5tCCZu.htm)|Sod Hound (BB)|Molosse de tourbe (BI)|changé|
|[Oilfs8Atv2LjAsUS.htm](menace-under-otari-bestiary/Oilfs8Atv2LjAsUS.htm)|Wolf (BB)|Loup (BI)|changé|
|[pw2NFqvkDm54lsbt.htm](menace-under-otari-bestiary/pw2NFqvkDm54lsbt.htm)|Envenomed Lock (BB)|Serrure empoisonnée (BI)|changé|
|[QaldZV2p9RpMXzzn.htm](menace-under-otari-bestiary/QaldZV2p9RpMXzzn.htm)|Kobold Dragon Mage (Green, BB)|Mage draconique vert kobold (BI)|changé|
|[R9eoGwQ2tudxUKxS.htm](menace-under-otari-bestiary/R9eoGwQ2tudxUKxS.htm)|Kobold Dragon Mage (Black, BB)|Mage draconique noir kobold (BI)|changé|
|[r9w1n85mp9Ip4QiS.htm](menace-under-otari-bestiary/r9w1n85mp9Ip4QiS.htm)|Kobold Warrior (BB)|Kobold Guerrier (BI)|changé|
|[rPaHIh0ICnTLnRO6.htm](menace-under-otari-bestiary/rPaHIh0ICnTLnRO6.htm)|Kobold Scout (BB)|Kobold Éclaireur (BI)|changé|
|[RTzFvmdSCf5yhguy.htm](menace-under-otari-bestiary/RTzFvmdSCf5yhguy.htm)|Xulgath Warrior (BB)|Xulgath Guerrier (BI)|changé|
|[shT19KaQjWRVrHLI.htm](menace-under-otari-bestiary/shT19KaQjWRVrHLI.htm)|Goblin Igniter (BB)|Gobelin Pyromane (BI)|changé|
|[UjREHs2JQoO85Glt.htm](menace-under-otari-bestiary/UjREHs2JQoO85Glt.htm)|Bugbear Prowler (BB)|Gobelours Maraudeur (BI)|changé|
|[WPsgrCUSFCqgDvJi.htm](menace-under-otari-bestiary/WPsgrCUSFCqgDvJi.htm)|Horned Dragon (Juvenile, BB)|Dragonnet vert (BI)|changé|
|[wqPYzMNgYvrO6oEP.htm](menace-under-otari-bestiary/wqPYzMNgYvrO6oEP.htm)|Leopard (BB)|Léopard (BI)|changé|
|[XrmHgbKgcHDi4OnK.htm](menace-under-otari-bestiary/XrmHgbKgcHDi4OnK.htm)|Shadow (BB)|Ombre (BI)|changé|
|[YdBCG0vzOA5BgoIi.htm](menace-under-otari-bestiary/YdBCG0vzOA5BgoIi.htm)|Xulgath Leader (BB)|Xulgath Chef (BI)|changé|
|[ZMr28tFTA5NUcBTi.htm](menace-under-otari-bestiary/ZMr28tFTA5NUcBTi.htm)|Web Lurker (BB)|Rôdeur des toiles (BI)|changé|
|[ZPjQkKVMi3xoPcU0.htm](menace-under-otari-bestiary/ZPjQkKVMi3xoPcU0.htm)|Wight (BB)|Nécrophage (BI)|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0oWKApY5FR8IO7GG.htm](menace-under-otari-bestiary/0oWKApY5FR8IO7GG.htm)|Orc Scrapper (BB)|Orc bagarreur (BI)|officielle|
|[2H2AEwQnfKJC0nrd.htm](menace-under-otari-bestiary/2H2AEwQnfKJC0nrd.htm)|Ghost Commoner (BB)|Fantôme roturier (BI)|officielle|
|[4MwjCsa5O9aAjxSm.htm](menace-under-otari-bestiary/4MwjCsa5O9aAjxSm.htm)|Boar (BB)|Sanglier (BI)|officielle|
|[4O7wKZdeAemTEbvG.htm](menace-under-otari-bestiary/4O7wKZdeAemTEbvG.htm)|Slamming Door (BB)|Claque-Porte (BI)|officielle|
|[5H8ZX7y5IkUBhvhF.htm](menace-under-otari-bestiary/5H8ZX7y5IkUBhvhF.htm)|Skeleton Guard (BB)|Squelette Garde (BI)|officielle|
|[5xjmJoJvBhASkEKS.htm](menace-under-otari-bestiary/5xjmJoJvBhASkEKS.htm)|Orc Commander (BB)|Orc commandant (BI)|officielle|
|[7VqibTAEXXX6PIhh.htm](menace-under-otari-bestiary/7VqibTAEXXX6PIhh.htm)|Scythe Blades (BB)|Lames de faux (BI)|officielle|
|[AdQVjlOWB6rmBRVp.htm](menace-under-otari-bestiary/AdQVjlOWB6rmBRVp.htm)|Doppelganger (BB)|Doppelganger (BI)|officielle|
|[AleeS0IRqT4tUphB.htm](menace-under-otari-bestiary/AleeS0IRqT4tUphB.htm)|Kobold Boss Zolgran (BB)|Zolgran, patronne des Kobolds (BI)|libre|
|[AYwdybUfm4meGUTJ.htm](menace-under-otari-bestiary/AYwdybUfm4meGUTJ.htm)|Giant Rat (BB)|Rat géant (BI)|officielle|
|[BHq5wpQU8hQEke8D.htm](menace-under-otari-bestiary/BHq5wpQU8hQEke8D.htm)|Hidden Pit (BB)|Fosse dissimulée (BI)|officielle|
|[Br1AtKUHe3nbzjnY.htm](menace-under-otari-bestiary/Br1AtKUHe3nbzjnY.htm)|Mimic (BB)|Mimique (BI)|officielle|
|[cZDiyluplFqRxmGy.htm](menace-under-otari-bestiary/cZDiyluplFqRxmGy.htm)|Animated Armor (BB)|Armure animée (BI)|officielle|
|[FaBHkmFGuEIqIYM1.htm](menace-under-otari-bestiary/FaBHkmFGuEIqIYM1.htm)|Drow Priestess (BB)|Drow Prêtresse (BI)|officielle|
|[gdXok08bITkhowDJ.htm](menace-under-otari-bestiary/gdXok08bITkhowDJ.htm)|Ogre Warrior (BB)|Ogre Guerrier (BI)|officielle|
|[Hkq9ZS2J2iKnT7vT.htm](menace-under-otari-bestiary/Hkq9ZS2J2iKnT7vT.htm)|Sewer Ooze (BB)|Vase des égouts (BI)|officielle|
|[j8qD2LVDSP2lhLUO.htm](menace-under-otari-bestiary/j8qD2LVDSP2lhLUO.htm)|Central Spears (BB)|Lances centrales (BI)|officielle|
|[rPHxXClTnoPYHYuZ.htm](menace-under-otari-bestiary/rPHxXClTnoPYHYuZ.htm)|Basilisk (BB)|Basilic (BI)|officielle|
|[sW8koPLrSgHalAnq.htm](menace-under-otari-bestiary/sW8koPLrSgHalAnq.htm)|Drow Warrior (BB)|Drow Guerrier (BI)|officielle|
|[v51J7K27abdDyLgJ.htm](menace-under-otari-bestiary/v51J7K27abdDyLgJ.htm)|Mermaid Fountain (BB)|Fontaine de la sirène (BI)|officielle|
|[vlMuFskctUvjJe8X.htm](menace-under-otari-bestiary/vlMuFskctUvjJe8X.htm)|Spear Launcher (BB)|Lance-épieu (BI)|officielle|
|[X03vq2RWi2jiA6Ri.htm](menace-under-otari-bestiary/X03vq2RWi2jiA6Ri.htm)|Owlbear (BB)|Hibours (BI)|officielle|
|[xKYIN88ULPFgSZmw.htm](menace-under-otari-bestiary/xKYIN88ULPFgSZmw.htm)|Drow Sneak (BB)|Drow Fureteur (BI)|officielle|
|[Z9ggO7spfHwr8up1.htm](menace-under-otari-bestiary/Z9ggO7spfHwr8up1.htm)|Falling Ceiling (BB)|Plafond Croulant (BI)|officielle|
