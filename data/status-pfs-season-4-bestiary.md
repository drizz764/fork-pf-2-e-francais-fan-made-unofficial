# État de la traduction (pfs-season-4-bestiary)

 * **libre**: 162
 * **changé**: 11
 * **aucune**: 47


Dernière mise à jour: 2024-06-13 06:52 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[kZr0bYzlCcMooMuf.htm](pfs-season-4-bestiary/kZr0bYzlCcMooMuf.htm)|Shaitan Tactics (5-6)|
|[lutwHdRXlCllpIsG.htm](pfs-season-4-bestiary/lutwHdRXlCllpIsG.htm)|Adagio|
|[mbTq6K9uYYIVbwDL.htm](pfs-season-4-bestiary/mbTq6K9uYYIVbwDL.htm)|Living Boulder (PFS 4-04)|
|[MHqOUkqUQ58Y2uFD.htm](pfs-season-4-bestiary/MHqOUkqUQ58Y2uFD.htm)|Abyssal Drool|
|[mjE5Fgn1nmJIPOP7.htm](pfs-season-4-bestiary/mjE5Fgn1nmJIPOP7.htm)|Damaged Automaton|
|[mS4CumAcPpkDdjrO.htm](pfs-season-4-bestiary/mS4CumAcPpkDdjrO.htm)|Greater Barghest (PFS 4-07)|
|[ntw6udZsknaAx6Ra.htm](pfs-season-4-bestiary/ntw6udZsknaAx6Ra.htm)|Bandit Skullcracker|
|[Nu2CYUqvUOsaeVih.htm](pfs-season-4-bestiary/Nu2CYUqvUOsaeVih.htm)|Death Drop (3-4)|
|[NvaXW0zwejtYSek4.htm](pfs-season-4-bestiary/NvaXW0zwejtYSek4.htm)|Elite Palace Skelm (PFS 4-11)|
|[NZpxlgV4KHdjigi4.htm](pfs-season-4-bestiary/NZpxlgV4KHdjigi4.htm)|Skulk Raider|
|[oEq4kZIpZ77jD5Pu.htm](pfs-season-4-bestiary/oEq4kZIpZ77jD5Pu.htm)|Seldrick Dralston (1-2)|
|[oH2KT9LhUs299AN9.htm](pfs-season-4-bestiary/oH2KT9LhUs299AN9.htm)|Dero Stalker (PFS 4-05)|
|[ojqnMjRsYCE8dTv8.htm](pfs-season-4-bestiary/ojqnMjRsYCE8dTv8.htm)|A Broken Promise (3-4)|
|[OQRaBwlV8ZgkZPrY.htm](pfs-season-4-bestiary/OQRaBwlV8ZgkZPrY.htm)|Festering Vermlek|
|[oTEsCGg0PxDNnj4p.htm](pfs-season-4-bestiary/oTEsCGg0PxDNnj4p.htm)|Lemming Rat|
|[p1ip40AhsO1fwy4M.htm](pfs-season-4-bestiary/p1ip40AhsO1fwy4M.htm)|Fasiel Ibn Sazadin (5-6)|
|[P9akw16RwTdkY6dI.htm](pfs-season-4-bestiary/P9akw16RwTdkY6dI.htm)|Averlace's Last Stand|
|[pZMUqoQrhQSyHMMc.htm](pfs-season-4-bestiary/pZMUqoQrhQSyHMMc.htm)|Death Drop (5-6)|
|[QbvkJokjXuCAaJcl.htm](pfs-season-4-bestiary/QbvkJokjXuCAaJcl.htm)|Enormous Rat|
|[QNgrBzqANdC88rhT.htm](pfs-season-4-bestiary/QNgrBzqANdC88rhT.htm)|Unbridled Necromantic Energy (3-4)|
|[R6aEJWeDH1DFfP0z.htm](pfs-season-4-bestiary/R6aEJWeDH1DFfP0z.htm)|Fetid Familiar|
|[R8ibeKqtuMFKoGcM.htm](pfs-season-4-bestiary/R8ibeKqtuMFKoGcM.htm)|Unbridled Necromantic Energy (1-2)|
|[Rq4b1pFU36QDkW7c.htm](pfs-season-4-bestiary/Rq4b1pFU36QDkW7c.htm)|Fireball Rune|
|[sMgRhArIc1GsFifq.htm](pfs-season-4-bestiary/sMgRhArIc1GsFifq.htm)|Seldrick Dralston (3-4)|
|[sN1Mm3M5fnb0SjYq.htm](pfs-season-4-bestiary/sN1Mm3M5fnb0SjYq.htm)|Deep Quicksand|
|[spj8i9bMQDZhj8C9.htm](pfs-season-4-bestiary/spj8i9bMQDZhj8C9.htm)|Farah Al-Saleel (3-4)|
|[swGHHImKBCUTwpyc.htm](pfs-season-4-bestiary/swGHHImKBCUTwpyc.htm)|Denufair|
|[TJ6ybz5Y7ZwSy6FH.htm](pfs-season-4-bestiary/TJ6ybz5Y7ZwSy6FH.htm)|Butterfly's Breath Trap (3-4)|
|[Tmev5DrM9pgZE00u.htm](pfs-season-4-bestiary/Tmev5DrM9pgZE00u.htm)|In The Witch's Clutches|
|[tP99gLygKXOZdbvr.htm](pfs-season-4-bestiary/tP99gLygKXOZdbvr.htm)|Cunning Vampire Spawn|
|[TubB70Gyx3RV7inZ.htm](pfs-season-4-bestiary/TubB70Gyx3RV7inZ.htm)|Butterfly's Breath Trap (1-2)|
|[TYB4yp93pCDfRevV.htm](pfs-season-4-bestiary/TYB4yp93pCDfRevV.htm)|In The Witch's Grasp|
|[u6hNwxqw69JIeBEx.htm](pfs-season-4-bestiary/u6hNwxqw69JIeBEx.htm)|Accursed Succubus|
|[UCLaNgaKG3vXzXiB.htm](pfs-season-4-bestiary/UCLaNgaKG3vXzXiB.htm)|Aslynn's Eye|
|[udCpriOCsMBEuhr8.htm](pfs-season-4-bestiary/udCpriOCsMBEuhr8.htm)|Brinna|
|[uJ9U93JZlW4T0Hnn.htm](pfs-season-4-bestiary/uJ9U93JZlW4T0Hnn.htm)|Mutated Scorpion|
|[vHLV1hPjfAwMSUEI.htm](pfs-season-4-bestiary/vHLV1hPjfAwMSUEI.htm)|Farah Al-Saleel (5-6)|
|[Vy5P6hdCiVLeRWTa.htm](pfs-season-4-bestiary/Vy5P6hdCiVLeRWTa.htm)|A Broken Promise (1-2)|
|[w54JWkw6lcLXL0Za.htm](pfs-season-4-bestiary/w54JWkw6lcLXL0Za.htm)|Elite Cave Bear (PFS 4-07)|
|[wmdK9SL7u1g4a0wY.htm](pfs-season-4-bestiary/wmdK9SL7u1g4a0wY.htm)|Major Fireball Rune|
|[xKCWrQM7tgIz5vTx.htm](pfs-season-4-bestiary/xKCWrQM7tgIz5vTx.htm)|Giant Fetid Familiar|
|[XSOh2n1QPwfSdejr.htm](pfs-season-4-bestiary/XSOh2n1QPwfSdejr.htm)|Aslynn's Sharper Eye|
|[y1Pe1I9h9gKzWC8k.htm](pfs-season-4-bestiary/y1Pe1I9h9gKzWC8k.htm)|Patched Up Automaton|
|[Y2mjAL3OGIuof4mz.htm](pfs-season-4-bestiary/Y2mjAL3OGIuof4mz.htm)|Dig-Widget (PFS 4-07)|
|[YBKHuyR2fNxtkYRu.htm](pfs-season-4-bestiary/YBKHuyR2fNxtkYRu.htm)|Weak Cunning Vampire Spawn|
|[YjTKd7VZwCFaTWPB.htm](pfs-season-4-bestiary/YjTKd7VZwCFaTWPB.htm)|Elite Smokesight Stalker Wight|
|[yKEJfIO67TdgW69B.htm](pfs-season-4-bestiary/yKEJfIO67TdgW69B.htm)|Greater Fireball Rune|
|[yTBGycsIv6hxeyXv.htm](pfs-season-4-bestiary/yTBGycsIv6hxeyXv.htm)|Larcius|
|[ytltNW8kF7VssFNJ.htm](pfs-season-4-bestiary/ytltNW8kF7VssFNJ.htm)|Shaitan Tactics (3-4)|
|[zbohP3l19fvkAypi.htm](pfs-season-4-bestiary/zbohP3l19fvkAypi.htm)|Allegro|
|[zj9L3bvmhtnOilmf.htm](pfs-season-4-bestiary/zj9L3bvmhtnOilmf.htm)|Depleted Roiling Incant|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[7Bon2mCIl8psUpUR.htm](pfs-season-4-bestiary/7Bon2mCIl8psUpUR.htm)|Serraleth|Serraleth|changé|
|[7W9dzIWzgEOB2UTK.htm](pfs-season-4-bestiary/7W9dzIWzgEOB2UTK.htm)|Suhko (3-4)|Suhko (3-4)|changé|
|[AkbAW7XeZQ67qW1R.htm](pfs-season-4-bestiary/AkbAW7XeZQ67qW1R.htm)|Faded Animate Dream|Rêve animé estompé|changé|
|[eWJwFmEasIOSGQgu.htm](pfs-season-4-bestiary/eWJwFmEasIOSGQgu.htm)|Elite Animate Dream (PFS 4-11)|Rêve animé élite (PFS 4-11)|changé|
|[GiSG0NC5MMpekWAu.htm](pfs-season-4-bestiary/GiSG0NC5MMpekWAu.htm)|Transformed Bremix (9-10)|Brémix transformé (9-10)|changé|
|[PIxgIqgDs3he4VV6.htm](pfs-season-4-bestiary/PIxgIqgDs3he4VV6.htm)|Serraleth (7-8)|Serraleth (7-8)|changé|
|[VQ1TNqlUvdlKkuN6.htm](pfs-season-4-bestiary/VQ1TNqlUvdlKkuN6.htm)|Suhko (1-2)|Suhko (1-2)|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0x30gt2GzALItGAh.htm](pfs-season-4-bestiary/0x30gt2GzALItGAh.htm)|Recon Patroller|Parouilleur de reconnaissance|libre|
|[16Ye7Wd2pXiUiGJo.htm](pfs-season-4-bestiary/16Ye7Wd2pXiUiGJo.htm)|Animated Dagger (3-4)|Dague animée (3-4)|libre|
|[1G589gnuWGIOdrou.htm](pfs-season-4-bestiary/1G589gnuWGIOdrou.htm)|Pappy Draighean|Pappy Draigehan|libre|
|[1iEGNvvHIG8EdOf1.htm](pfs-season-4-bestiary/1iEGNvvHIG8EdOf1.htm)|Harpy Forager|Harpie glaneuse|libre|
|[1iYvAte2y5fTN9WE.htm](pfs-season-4-bestiary/1iYvAte2y5fTN9WE.htm)|Dorenea|Dorenéa|libre|
|[1ZUAVNlJb4MWtSCw.htm](pfs-season-4-bestiary/1ZUAVNlJb4MWtSCw.htm)|Witch's Gaze|Regard du sorcier|libre|
|[2iba0fJU9Arc0XYN.htm](pfs-season-4-bestiary/2iba0fJU9Arc0XYN.htm)|Sounrel (5-6)|Sounrel (5-6)|libre|
|[2KVO6l8BKRO3pxE8.htm](pfs-season-4-bestiary/2KVO6l8BKRO3pxE8.htm)|Witch's Talon|Serre du socier|libre|
|[2N9DhnUedzp3FHhM.htm](pfs-season-4-bestiary/2N9DhnUedzp3FHhM.htm)|Elite Combustive Zombie|Zombie de combustion élite|libre|
|[2OBumnPBmrTgZRou.htm](pfs-season-4-bestiary/2OBumnPBmrTgZRou.htm)|Liaskra (1-2)|Liaskra (1-2)|libre|
|[2rEw46Ye8jWPkXys.htm](pfs-season-4-bestiary/2rEw46Ye8jWPkXys.htm)|Ambush Assassin|Assassin d'embuscade|libre|
|[3TEapF0mBKvW3Rqf.htm](pfs-season-4-bestiary/3TEapF0mBKvW3Rqf.htm)|Determined Thisnabel (3-4)|Thisnabel déterminé (3-4)|libre|
|[40Ow3r8prGGGjXgB.htm](pfs-season-4-bestiary/40Ow3r8prGGGjXgB.htm)|Nathatel (3-4)|Nathael (3-4)|libre|
|[473NoC8CXToJcWzk.htm](pfs-season-4-bestiary/473NoC8CXToJcWzk.htm)|Burrowing Viper Rat|Vipère-rat fouisseuse|libre|
|[4bGcT950NIM36Pmn.htm](pfs-season-4-bestiary/4bGcT950NIM36Pmn.htm)|Elven Grave Robber|Pilleur de tombes elfe|libre|
|[4G6VvfIsceXfEZBB.htm](pfs-season-4-bestiary/4G6VvfIsceXfEZBB.htm)|Elite Smokesight Deadeye Wight|Nécrophage chasseur précis Visefumée élite|libre|
|[4Kll3POFvVeSNVkH.htm](pfs-season-4-bestiary/4Kll3POFvVeSNVkH.htm)|Meddlesome Scrit|Scrit indiscret|libre|
|[4NyzLWjptheUAbgW.htm](pfs-season-4-bestiary/4NyzLWjptheUAbgW.htm)|Elven Hunter|Chasseur elfe|libre|
|[4r7haYeUSxjW3wtR.htm](pfs-season-4-bestiary/4r7haYeUSxjW3wtR.htm)|Smokesight Deadeye Wight|Nécrophage chasseur précis Visefumée|libre|
|[4RC6Uw5awahsRh2K.htm](pfs-season-4-bestiary/4RC6Uw5awahsRh2K.htm)|Skeletal Pathfinder Veteran|Éclaireur vétéran squelette|libre|
|[548JZ6YeXsfYxVe5.htm](pfs-season-4-bestiary/548JZ6YeXsfYxVe5.htm)|Animated Dagger|Dague animée|libre|
|[55qjNw6rOCdJYC52.htm](pfs-season-4-bestiary/55qjNw6rOCdJYC52.htm)|Mutant Shadow Drake|Drake d'ombre mutant|libre|
|[55XJegJx3b5MoB2s.htm](pfs-season-4-bestiary/55XJegJx3b5MoB2s.htm)|Elven Tracker|Pisteur elfe|libre|
|[56GciWoZDuiqjINk.htm](pfs-season-4-bestiary/56GciWoZDuiqjINk.htm)|Weak Clacking Skull Swarm|Nuée cliquetante de crânes affaibli|libre|
|[576LENfQB9u4V92Y.htm](pfs-season-4-bestiary/576LENfQB9u4V92Y.htm)|Dream of Doom|Rêve de ruine|libre|
|[5R9kkxzfr4KJAeA1.htm](pfs-season-4-bestiary/5R9kkxzfr4KJAeA1.htm)|Hidden Forest Bog (5-6)|Tourbière forestière cachée (5-6)|libre|
|[5t2uXTLYX03YUmUk.htm](pfs-season-4-bestiary/5t2uXTLYX03YUmUk.htm)|Witch's Eye|Oeil du sorcier|libre|
|[6gkKpEHa1YjnBu69.htm](pfs-season-4-bestiary/6gkKpEHa1YjnBu69.htm)|Coven Winter Hag|Guenaude d'hiver de cercle|libre|
|[6pg2w3qRLK4h6alz.htm](pfs-season-4-bestiary/6pg2w3qRLK4h6alz.htm)|Tempest-Forged Chains|Chaînes forgées par la tempête|libre|
|[7LUMcdFywdt10u9p.htm](pfs-season-4-bestiary/7LUMcdFywdt10u9p.htm)|Hallways Full of Mostly Illusory Opossums|Corridor rempli d'une majorité d'oppossums illusoires|libre|
|[7Njvm0rs09uwz7wv.htm](pfs-season-4-bestiary/7Njvm0rs09uwz7wv.htm)|Harpy Wailer|Harpie pleureuse|libre|
|[7NNuVN0DwPvmZLN9.htm](pfs-season-4-bestiary/7NNuVN0DwPvmZLN9.htm)|Recon Scout|Éclaireur de reconnaissance|libre|
|[7yap6E0QQkY0N9HO.htm](pfs-season-4-bestiary/7yap6E0QQkY0N9HO.htm)|Bloody Barber Ruffian|Voyou des Barbiers sanglants|libre|
|[82otwWwog77fPBJ8.htm](pfs-season-4-bestiary/82otwWwog77fPBJ8.htm)|Witch's Wing|Aile du sorcier|libre|
|[8DKm9pmBJdgsA4MQ.htm](pfs-season-4-bestiary/8DKm9pmBJdgsA4MQ.htm)|Transformed Narcela (7-8)|Narcela transformée (7-8)|libre|
|[8h0PJxZu0WVcNm6H.htm](pfs-season-4-bestiary/8h0PJxZu0WVcNm6H.htm)|Stabilized Black Pudding|Pudding noir stabilisé|libre|
|[8i1a132Z2xJtqyQI.htm](pfs-season-4-bestiary/8i1a132Z2xJtqyQI.htm)|Witch of the Owl|Sorcier du Hibou|libre|
|[8mtq3RF6L0EL1kSW.htm](pfs-season-4-bestiary/8mtq3RF6L0EL1kSW.htm)|It Rains|Il pleut|libre|
|[8OzYnoDXrQcSL0GD.htm](pfs-season-4-bestiary/8OzYnoDXrQcSL0GD.htm)|Ambush Spy|Espion d'embuscade|libre|
|[8WT81nQ029L5MN34.htm](pfs-season-4-bestiary/8WT81nQ029L5MN34.htm)|Veteran Vermlek|Vermlek vétéran|libre|
|[8xeZ6N46DLqnOw32.htm](pfs-season-4-bestiary/8xeZ6N46DLqnOw32.htm)|Charlatan (PFS 4-05)|Charlatan (PFS 4-05)|libre|
|[9fBLqeIyvH232sFp.htm](pfs-season-4-bestiary/9fBLqeIyvH232sFp.htm)|Elite Smokesight Prowler Wight|Nécrophage rôdeur Visefumée élite|libre|
|[9TWGnI8mAUwt4jqE.htm](pfs-season-4-bestiary/9TWGnI8mAUwt4jqE.htm)|Evasive Vampire Count|Comte vampire évasif|libre|
|[9zNW4VwBHjhMnEqe.htm](pfs-season-4-bestiary/9zNW4VwBHjhMnEqe.htm)|Shadow Cannon|Canon d'ombres|libre|
|[ACDuWRp4JELgqmo8.htm](pfs-season-4-bestiary/ACDuWRp4JELgqmo8.htm)|Pappy Draighean (5-6)|Pappy Draghean (5-6)|libre|
|[Akq5V2jzJYUZXT8d.htm](pfs-season-4-bestiary/Akq5V2jzJYUZXT8d.htm)|Potted Violet Etheroot|Racinéther violet en pot|libre|
|[aOniinvqGZ6EUbyq.htm](pfs-season-4-bestiary/aOniinvqGZ6EUbyq.htm)|Growling Shadow Drake|Drake d'ombre grognant|libre|
|[ay3mb6lMokkgCxQL.htm](pfs-season-4-bestiary/ay3mb6lMokkgCxQL.htm)|Fey-Touched Great Wolf|Grand loup béni des fey|libre|
|[BFTZmBmib06cgtXD.htm](pfs-season-4-bestiary/BFTZmBmib06cgtXD.htm)|Wicked Reflection|Réflection dangereuse|libre|
|[bP2uuVEI01zNZOlD.htm](pfs-season-4-bestiary/bP2uuVEI01zNZOlD.htm)|Sounrel (3-4)|Sounrel (3-4)|libre|
|[BRo5HCigb4YTjJys.htm](pfs-season-4-bestiary/BRo5HCigb4YTjJys.htm)|Elite Smokesight Wight Captain|Capitaine nécrophage Visefumée élite|libre|
|[BsuqNSusIHt19ijd.htm](pfs-season-4-bestiary/BsuqNSusIHt19ijd.htm)|Grace of the Owl|La grâce du hibou|libre|
|[c3YWo3zyPsybTYhC.htm](pfs-season-4-bestiary/c3YWo3zyPsybTYhC.htm)|Possessed Clockwork Heap|Amas de mécanismes possédé|libre|
|[C9gWLpxJr4QKxP64.htm](pfs-season-4-bestiary/C9gWLpxJr4QKxP64.htm)|Undead Nathatel|Nathatel mort-vivant|libre|
|[CgoJslSLVrUc86W5.htm](pfs-season-4-bestiary/CgoJslSLVrUc86W5.htm)|Revna the Brave (5-6)|Revna le Vaillant (5-6)|libre|
|[cIa7Zdq9cUziEL3U.htm](pfs-season-4-bestiary/cIa7Zdq9cUziEL3U.htm)|Pups|Pups|libre|
|[cINSKPBcnWgzeTFx.htm](pfs-season-4-bestiary/cINSKPBcnWgzeTFx.htm)|Overtaxed Vrisk|Vrisk surchargé|libre|
|[d50bL4eXBTXkcE8l.htm](pfs-season-4-bestiary/d50bL4eXBTXkcE8l.htm)|Fleshwarp Apprentice|Apprenti distordu|libre|
|[DMbCbc6tGOjJfdIo.htm](pfs-season-4-bestiary/DMbCbc6tGOjJfdIo.htm)|Abyssal Warhound Pack Leader|Molosse chthonien chef de meute|libre|
|[DSJtkGzddv1y6as5.htm](pfs-season-4-bestiary/DSJtkGzddv1y6as5.htm)|Vrisk|Vrisk|libre|
|[DwxyObby3PbYNyrf.htm](pfs-season-4-bestiary/DwxyObby3PbYNyrf.htm)|Elite Sulfur Zombie|Zombie de soufre élite|libre|
|[E0KmdZTSCae2rVJi.htm](pfs-season-4-bestiary/E0KmdZTSCae2rVJi.htm)|Travel Worn Lurker In Light|Rôdeur de lumière affaibli par le voyage|libre|
|[eC1Y25pgLNSpAoki.htm](pfs-season-4-bestiary/eC1Y25pgLNSpAoki.htm)|Smokesight Stalker Wight|Nécrophage harceleur Visefumée|libre|
|[eEwgeT3w7CBJLpbr.htm](pfs-season-4-bestiary/eEwgeT3w7CBJLpbr.htm)|Abyssal Warhound General|Molosse chthonien Général|libre|
|[eon011qZn3I3wYxK.htm](pfs-season-4-bestiary/eon011qZn3I3wYxK.htm)|Hidden Forest Bog|Tourbière forestière cachée|libre|
|[ETL4tgrSRxPS5ooV.htm](pfs-season-4-bestiary/ETL4tgrSRxPS5ooV.htm)|House Spirits With an Absurd Number of Regenerating Pies|Esprit de maison avec un nombre absurde de tartes régénérées|libre|
|[FHOgTGMrt9342ZbI.htm](pfs-season-4-bestiary/FHOgTGMrt9342ZbI.htm)|Transformed Narcela (9-10)|Narcela transformée (9-10)|libre|
|[fn5Jex9qIMxB86xH.htm](pfs-season-4-bestiary/fn5Jex9qIMxB86xH.htm)|Bugbear Night Terror|Gobelours terrorisant de la nuit|libre|
|[Fu4ChrUyc18E0V0v.htm](pfs-season-4-bestiary/Fu4ChrUyc18E0V0v.htm)|Bugbear Raider|Pilleur gobelours|libre|
|[G0ur3TDztA2Vj8vv.htm](pfs-season-4-bestiary/G0ur3TDztA2Vj8vv.htm)|Malevolent Reflections|Réflections malveillantes|libre|
|[gApYWrEZpYI0fOnk.htm](pfs-season-4-bestiary/gApYWrEZpYI0fOnk.htm)|Thisnabel (1-2)|Thisnabel (1-2)|libre|
|[Gf9RNXtFbMPA8yqa.htm](pfs-season-4-bestiary/Gf9RNXtFbMPA8yqa.htm)|Fascinating Lecture (3-4)|Conférence fascinante (3-4)|libre|
|[GGypi6t8oJBqWdWS.htm](pfs-season-4-bestiary/GGypi6t8oJBqWdWS.htm)|Suncatcher Blooms (3-4)|Fleur Attrape-soleil (3-4)|libre|
|[GJivJZpKr26648NR.htm](pfs-season-4-bestiary/GJivJZpKr26648NR.htm)|Hidden Forest Bog (7-8)|Tourbière forestière cachée (7-8)|libre|
|[gOmGPZ3Ul1bFtQZO.htm](pfs-season-4-bestiary/gOmGPZ3Ul1bFtQZO.htm)|Revna the Brave (7-8)|Revna le Vaillant (7-8)|libre|
|[gqQoahBEk37LqeTL.htm](pfs-season-4-bestiary/gqQoahBEk37LqeTL.htm)|Rhenei (3-4)|Rhenei (3-4)|libre|
|[H9z43UB0KXksVTZ6.htm](pfs-season-4-bestiary/H9z43UB0KXksVTZ6.htm)|Elite Clacking Skull Swarm|Nuée de crânes cliquetant d'élite|libre|
|[hbnmM6YafwUqH50s.htm](pfs-season-4-bestiary/hbnmM6YafwUqH50s.htm)|Bolger Trusk (7-8)|Bolger Trusk (7-8)|libre|
|[hKyWYatvNfvM82QG.htm](pfs-season-4-bestiary/hKyWYatvNfvM82QG.htm)|Menacing Shadow Drake|Drake d'ombre menaçant|libre|
|[HLxLEIVC9hN1Ze9c.htm](pfs-season-4-bestiary/HLxLEIVC9hN1Ze9c.htm)|Darkened Shadow Bombard|Bombarde d'ombre obscurcie|libre|
|[hm3ZkeNOSM8b5KLo.htm](pfs-season-4-bestiary/hm3ZkeNOSM8b5KLo.htm)|Halran (3-4)|Halran (3-4)|libre|
|[HXq9Vqshk7DzFQ0C.htm](pfs-season-4-bestiary/HXq9Vqshk7DzFQ0C.htm)|Storm-Wrought Chains|Chaînes forgées par l'orage|libre|
|[iacML9PbFyo49TMe.htm](pfs-season-4-bestiary/iacML9PbFyo49TMe.htm)|Determined Dura (3-4)|Dura déterminée (3-4)|libre|
|[IbC54Tp90MApKMXn.htm](pfs-season-4-bestiary/IbC54Tp90MApKMXn.htm)|Nathatel (7-8)|Nathatel (7-8)|libre|
|[IIlSpHMHQ8ZqftvG.htm](pfs-season-4-bestiary/IIlSpHMHQ8ZqftvG.htm)|Alarmingly Efficient Unseen Arson Assistant|Assistant pyromane terriblement efficace|libre|
|[IkS9AxSHCAPTYSoB.htm](pfs-season-4-bestiary/IkS9AxSHCAPTYSoB.htm)|Tsimaan|Tsimaan|libre|
|[IMeWlbCAMJRKc5Ao.htm](pfs-season-4-bestiary/IMeWlbCAMJRKc5Ao.htm)|Aslynn|Aslynn|libre|
|[IpoCKKY4bUW4Jox9.htm](pfs-season-4-bestiary/IpoCKKY4bUW4Jox9.htm)|Deimostride (9-10)|Deimostride (9-10)|libre|
|[ixCuB33ckYp0BLnh.htm](pfs-season-4-bestiary/ixCuB33ckYp0BLnh.htm)|Snakebush (1-2)|Buisson-serpent|libre|
|[J7dnmfDzygsR1mLq.htm](pfs-season-4-bestiary/J7dnmfDzygsR1mLq.htm)|Possessed Clockwork Vessel|Récipient mécanique possédé|libre|
|[JGbgYTBRSSf94v0U.htm](pfs-season-4-bestiary/JGbgYTBRSSf94v0U.htm)|Shard of Aslynn (3-4)|Fragment d'Aslynn (3-4)|libre|
|[jJwEeklWJu49aXi7.htm](pfs-season-4-bestiary/jJwEeklWJu49aXi7.htm)|Grim Rictus Bridge Guard|Garde-pont du Rictus sombre|libre|
|[Jk2lUr8825TMWZ8R.htm](pfs-season-4-bestiary/Jk2lUr8825TMWZ8R.htm)|Bugbear Trickster|Gobelours arnaqueur|libre|
|[JTrKsjnCLJy7W1gj.htm](pfs-season-4-bestiary/JTrKsjnCLJy7W1gj.htm)|Urdefhan Blood Mage (PFS 4-11)|Mage de sang urdefhan (PFS 4-11)|libre|
|[jV3wANLbZbv6S5Mv.htm](pfs-season-4-bestiary/jV3wANLbZbv6S5Mv.htm)|Animated Leather Armor|Armure en cuir animée|libre|
|[K9ClMcqPZ4BdZ7iU.htm](pfs-season-4-bestiary/K9ClMcqPZ4BdZ7iU.htm)|Rhenei (1-2)|Rhenei (1-2)|libre|
|[KV2LmRMc9ho9mBPy.htm](pfs-season-4-bestiary/KV2LmRMc9ho9mBPy.htm)|Elite Smokesight Wight Commander|Commandant nécrophage Visefumée élite|libre|
|[Ky52WrkDauoAFbG6.htm](pfs-season-4-bestiary/Ky52WrkDauoAFbG6.htm)|Abyssal Bunny Swarm|Nuées de lapins chthoniens|libre|
|[kzI1W2HlTbA1wRIV.htm](pfs-season-4-bestiary/kzI1W2HlTbA1wRIV.htm)|Darkened Shadow Cannon (4-17)|Canon des ombres obscurci (4-17)|libre|
|[lC3AaEl9XFaewjx0.htm](pfs-season-4-bestiary/lC3AaEl9XFaewjx0.htm)|Snakebush (3-4)|Buisson-serpent (3-4)|libre|
|[LGgd2TQ90JG2CZKE.htm](pfs-season-4-bestiary/LGgd2TQ90JG2CZKE.htm)|Nathatel (5-6)|Nathatel (5-6)|libre|
|[lGz4MraAcv1NwyDS.htm](pfs-season-4-bestiary/lGz4MraAcv1NwyDS.htm)|Bugbear Terror|Gobelours terrorisant|libre|
|[LIU62taP4KjBpqEk.htm](pfs-season-4-bestiary/LIU62taP4KjBpqEk.htm)|Bolger Trusk (5-6)|Bolger Trusk (5-6)|libre|
|[m68Zvfw17TYfoacL.htm](pfs-season-4-bestiary/m68Zvfw17TYfoacL.htm)|Halran (1-2)|Halran (1-2)|libre|
|[MGcRvpxJjShaEIoD.htm](pfs-season-4-bestiary/MGcRvpxJjShaEIoD.htm)|Determined Rhenei (3-4)|Rhenei déterminé (3-4)|libre|
|[MiVmx6v1nMTX09Aa.htm](pfs-season-4-bestiary/MiVmx6v1nMTX09Aa.htm)|Vicious Abyssal Bunny Swarm|Nuée de lapins chthoniens vicieux|libre|
|[MJd091LyUYQR2cK0.htm](pfs-season-4-bestiary/MJd091LyUYQR2cK0.htm)|Ambush Scout|Éclaireur d'embuscade|libre|
|[mQcGeS71GA7QnPlY.htm](pfs-season-4-bestiary/mQcGeS71GA7QnPlY.htm)|Unseen Arson Assistant|Assistant invisible pyromane|libre|
|[mSPLEMPyiA0NkKN0.htm](pfs-season-4-bestiary/mSPLEMPyiA0NkKN0.htm)|Bugbear Bully|Gobelours brutal|libre|
|[NCTFP1qF2F7I5QT8.htm](pfs-season-4-bestiary/NCTFP1qF2F7I5QT8.htm)|Ambush Runner|Coureur d'embuscade|libre|
|[neAEB3E1yECRRmEv.htm](pfs-season-4-bestiary/neAEB3E1yECRRmEv.htm)|Stabilized Ochre Jelly|Gelée ocre stabilisée|libre|
|[nr6qCJkCLNAZh5ED.htm](pfs-season-4-bestiary/nr6qCJkCLNAZh5ED.htm)|Undead Nathatel (7-8)|Nathatel mort-vivant (7-8)|libre|
|[OOwfdaGQYDknAH4e.htm](pfs-season-4-bestiary/OOwfdaGQYDknAH4e.htm)|Shard of Aslynn (1-2)|Fragment d'Aslynn (1-2)|libre|
|[oPA6PVdRSC43FDDB.htm](pfs-season-4-bestiary/oPA6PVdRSC43FDDB.htm)|Bugbear Slasher|Assaillant gobelours|libre|
|[oqHSst4QRsoFZMXk.htm](pfs-season-4-bestiary/oqHSst4QRsoFZMXk.htm)|Nathatel|Nathatel|libre|
|[OXhYQYaXtFRHj0EG.htm](pfs-season-4-bestiary/OXhYQYaXtFRHj0EG.htm)|Ambush Observer|Observateur d'embuscade|libre|
|[p35bUOIZohXTurQM.htm](pfs-season-4-bestiary/p35bUOIZohXTurQM.htm)|Determined Rhenei (1-2)|Rhenei déterminé (1-2)|libre|
|[P6zS0Cdwu6Gkxv26.htm](pfs-season-4-bestiary/P6zS0Cdwu6Gkxv26.htm)|Veteran Ulfen Soldier|Soldat ulfe vétéran|libre|
|[PKLIiMW0UMWYT6os.htm](pfs-season-4-bestiary/PKLIiMW0UMWYT6os.htm)|Ulfen Soldier|Soldat ulfe|libre|
|[pKw8DutJNom5QxBZ.htm](pfs-season-4-bestiary/pKw8DutJNom5QxBZ.htm)|Aslynn the Dream Taker|Aslynn voleuse de rêves|libre|
|[pomHkkLGJxTvpAyA.htm](pfs-season-4-bestiary/pomHkkLGJxTvpAyA.htm)|Smokesight Hunter Wight|Nécrophage chasseur Visefumée|libre|
|[Q0N3WeP9tulx1Zx6.htm](pfs-season-4-bestiary/Q0N3WeP9tulx1Zx6.htm)|Weak Evasive Vampire Count|Comte vampire évasif affaibli|libre|
|[Q6BTF30qgInYi2BA.htm](pfs-season-4-bestiary/Q6BTF30qgInYi2BA.htm)|Smokesight Wight Commander|Commandant nécrophage Visefumée|libre|
|[qdUFJOVrItZjjl6j.htm](pfs-season-4-bestiary/qdUFJOVrItZjjl6j.htm)|Animated Leather Armor (4-99)|Armure de cuir animée (4-99)|libre|
|[qkqL6iX7WRmOfNk7.htm](pfs-season-4-bestiary/qkqL6iX7WRmOfNk7.htm)|Runt (3-4)|Runt (3-4)|libre|
|[qqNfvVoquLzXsg9j.htm](pfs-season-4-bestiary/qqNfvVoquLzXsg9j.htm)|Smokesight Wight Captain|Capitaine nécrophage Visefumée|libre|
|[r77a464nSqsntQnv.htm](pfs-season-4-bestiary/r77a464nSqsntQnv.htm)|Grim Rictus Robber|Voleur du Rictus sombre|libre|
|[RG0E58wEmyclbeJA.htm](pfs-season-4-bestiary/RG0E58wEmyclbeJA.htm)|Determined Dura (1-2)|Dura déterminée (1-2)|libre|
|[RpDO2ClbBiAUfQCL.htm](pfs-season-4-bestiary/RpDO2ClbBiAUfQCL.htm)|Ambush Raider|Écumeur d'embuscade|libre|
|[sCLcQpldPYWnxPSL.htm](pfs-season-4-bestiary/sCLcQpldPYWnxPSL.htm)|Liaskra (3-4)|Liaskra (3-4)|libre|
|[SO8aRCEXdCRJ8TIG.htm](pfs-season-4-bestiary/SO8aRCEXdCRJ8TIG.htm)|Smokesight Prowler Wight|Nécrophage rôdeur Visefumée|libre|
|[sQeRXRsly4npOU4r.htm](pfs-season-4-bestiary/sQeRXRsly4npOU4r.htm)|Infested Clockwork Vessel|Récipient mécanique infesté|libre|
|[sV98dOFzkqnhzCwr.htm](pfs-season-4-bestiary/sV98dOFzkqnhzCwr.htm)|Skeletal Pathfinder|Éclaireur squelette|libre|
|[sxN5QwOxfFx0160i.htm](pfs-season-4-bestiary/sxN5QwOxfFx0160i.htm)|Hidden Forest Bog (3-4)|Tourbière forestière cachée (3-4)|libre|
|[t4trkvNuN5cQudRR.htm](pfs-season-4-bestiary/t4trkvNuN5cQudRR.htm)|Determined Halran (1-2)|Halran déterminé (1-2)|libre|
|[tihsW2usCLrMwa2Y.htm](pfs-season-4-bestiary/tihsW2usCLrMwa2Y.htm)|Troublesome Scrit|Scrit fauteur de trouble|libre|
|[TMiMJVvmOhZVQkVW.htm](pfs-season-4-bestiary/TMiMJVvmOhZVQkVW.htm)|Thisnabel (3-4)|Thisnabel (3-4)|libre|
|[TzfxNwZgSLCaIRcL.htm](pfs-season-4-bestiary/TzfxNwZgSLCaIRcL.htm)|Hallways Full of Even More Mostly Illusory Opossums|Corridor rempli d'encore plus d'une majorité d'oppossums illusoires|libre|
|[U5pmX9DX9Xk7zrJj.htm](pfs-season-4-bestiary/U5pmX9DX9Xk7zrJj.htm)|Elven Torchbearer|Porteur de flambeau elfe|libre|
|[UDodAGDnGkGwYsMi.htm](pfs-season-4-bestiary/UDodAGDnGkGwYsMi.htm)|Elite Smokesight Hunter Wight|Nécrophage chasseur Visefumée élite|libre|
|[UT4hacQMILQiIqSn.htm](pfs-season-4-bestiary/UT4hacQMILQiIqSn.htm)|Darkened Shadow Cannon|Canon d'ombre obscurci|libre|
|[vA5VwmGb7xZ0WQdM.htm](pfs-season-4-bestiary/vA5VwmGb7xZ0WQdM.htm)|Combustive Zombie|Zombie de combustion|libre|
|[VhmCiYhSVBhxqdEb.htm](pfs-season-4-bestiary/VhmCiYhSVBhxqdEb.htm)|Deimostride (7-8)|Deimostride (7-8)|libre|
|[vTndTdmCdHiNxJ6x.htm](pfs-season-4-bestiary/vTndTdmCdHiNxJ6x.htm)|Abyssal Warhound|Molosse chthonien|libre|
|[VXAsqEVmE2Gi8ZHk.htm](pfs-season-4-bestiary/VXAsqEVmE2Gi8ZHk.htm)|Bugbear Creeper|Gobelours insidieux|libre|
|[w0empC43Ni3K4IQN.htm](pfs-season-4-bestiary/w0empC43Ni3K4IQN.htm)|House Spirits With a Large Number of Regenerating Pies|Esprit de maison avec un grand nombre de tartes régénérées|libre|
|[weilkjJuw0L1iFdZ.htm](pfs-season-4-bestiary/weilkjJuw0L1iFdZ.htm)|Determined Thisnabel (1-2)|Thisnabel déterminé (1-2)|libre|
|[WET7yEJxJmqQUMmn.htm](pfs-season-4-bestiary/WET7yEJxJmqQUMmn.htm)|Bloody Barber Thief|Cambrioleur des Barbiers sanglants|libre|
|[woCCr4BBVOjc3x92.htm](pfs-season-4-bestiary/woCCr4BBVOjc3x92.htm)|Coven Grave Hag|Guenaude des tombes de cercle|libre|
|[wvH5n8xxN8rYeNeC.htm](pfs-season-4-bestiary/wvH5n8xxN8rYeNeC.htm)|Determined Halran (3-4)|Halran déterminé (3-4)|libre|
|[xiowQvReC58NPbD5.htm](pfs-season-4-bestiary/xiowQvReC58NPbD5.htm)|Dura (1-2)|Dura (1-2)|libre|
|[XkoZF15KKDsz2m2x.htm](pfs-season-4-bestiary/XkoZF15KKDsz2m2x.htm)|Runt (1-2)|Runt (1-2)|libre|
|[xY95sdnK1MMIyeee.htm](pfs-season-4-bestiary/xY95sdnK1MMIyeee.htm)|Well-Fed Potted Violet Etheroot|Racinéther violet bien nourri en pot|libre|
|[xYzi5lnTSwcsKi8S.htm](pfs-season-4-bestiary/xYzi5lnTSwcsKi8S.htm)|It Pours|Il pleut à verse|libre|
|[YBrbpODnj7nYtI7W.htm](pfs-season-4-bestiary/YBrbpODnj7nYtI7W.htm)|Transformed Bremix (7-8)|Brémix transformé (7-8)|libre|
|[YcSoXBR5lacVKYzX.htm](pfs-season-4-bestiary/YcSoXBR5lacVKYzX.htm)|Bloody Barber Gang Leader|Chef de gang des Barbiers sanglant|libre|
|[yMIbNWGAZTLYUFVI.htm](pfs-season-4-bestiary/yMIbNWGAZTLYUFVI.htm)|Dura (3-4)|Dura (3-4)|libre|
|[ysyBat8g5V1KxAYN.htm](pfs-season-4-bestiary/ysyBat8g5V1KxAYN.htm)|Suncatcher Blooms (1-2)|Fleur Attrape-soleil (1-2)|libre|
|[z8or2fyRrPjNJL10.htm](pfs-season-4-bestiary/z8or2fyRrPjNJL10.htm)|Urdefhan High Tormentor (PFS 4-11)|Grand bourreau Urdefhan (PFS 4-11)|libre|
|[Zc2J10Bbc5mmkZd7.htm](pfs-season-4-bestiary/Zc2J10Bbc5mmkZd7.htm)|Fascinating Lecture (1-2)|Conférence fascinante (1-2)|libre|
|[zmOYvFVhxA6zDEBg.htm](pfs-season-4-bestiary/zmOYvFVhxA6zDEBg.htm)|Infested Clockwork Heap|Amas de mécanismes infesté|libre|
|[ZWfXDI2Xy4HJroL8.htm](pfs-season-4-bestiary/ZWfXDI2Xy4HJroL8.htm)|Elven Fence|Receleur elfe|libre|
