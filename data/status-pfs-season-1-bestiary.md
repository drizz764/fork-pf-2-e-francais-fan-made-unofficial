# État de la traduction (pfs-season-1-bestiary)

 * **aucune**: 312
 * **libre**: 4
 * **changé**: 15


Dernière mise à jour: 2024-06-13 06:52 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[02MIBcyPwCEmh8lA.htm](pfs-season-1-bestiary/02MIBcyPwCEmh8lA.htm)|Wounded Orc Brute|
|[06T3st7Hyx2O4E3v.htm](pfs-season-1-bestiary/06T3st7Hyx2O4E3v.htm)|Lesser Guardian Statue|
|[0DudqNVGfXC39umi.htm](pfs-season-1-bestiary/0DudqNVGfXC39umi.htm)|Reefclaw Spawn|
|[0oZJXOPw0OfkaMMU.htm](pfs-season-1-bestiary/0oZJXOPw0OfkaMMU.htm)|Trapmaster Tok's Surprise (3-4)|
|[0TEnslG0jaLrbf58.htm](pfs-season-1-bestiary/0TEnslG0jaLrbf58.htm)|Tough Giant Shrew|
|[1eeMzFoSPuQAAsiY.htm](pfs-season-1-bestiary/1eeMzFoSPuQAAsiY.htm)|Paravaax (5-6)|
|[1JVuJXe27PLyUVwf.htm](pfs-season-1-bestiary/1JVuJXe27PLyUVwf.htm)|Farmer Drystan's Scarecrow (1-2)|
|[1lfSUWK706fFrvhT.htm](pfs-season-1-bestiary/1lfSUWK706fFrvhT.htm)|Bandit Lieutenant|
|[1LM0AUYkZcsbtETs.htm](pfs-season-1-bestiary/1LM0AUYkZcsbtETs.htm)|Elite Boggard Swampseer (PFS 1-24, Staff)|
|[1MXO3xJS50lENkHL.htm](pfs-season-1-bestiary/1MXO3xJS50lENkHL.htm)|Meleeka Sanvara (1-2)|
|[1Yb78i4YajKrsens.htm](pfs-season-1-bestiary/1Yb78i4YajKrsens.htm)|Nevashi (7-8)|
|[20DJMDdNk0zLzaZZ.htm](pfs-season-1-bestiary/20DJMDdNk0zLzaZZ.htm)|Immature Giant Gecko|
|[24UI02EJVRSO6KZx.htm](pfs-season-1-bestiary/24UI02EJVRSO6KZx.htm)|House Cat|
|[26SFLzXVXqX7aEUD.htm](pfs-season-1-bestiary/26SFLzXVXqX7aEUD.htm)|Annis Hag (1-06)|
|[2cIhbpiQcXGnWdCP.htm](pfs-season-1-bestiary/2cIhbpiQcXGnWdCP.htm)|Lightning Platform (5-6)|
|[2fUyhm29ugKzkIdS.htm](pfs-season-1-bestiary/2fUyhm29ugKzkIdS.htm)|Kobold Dragon Spellweaver|
|[2HEhlngODb1KfSwE.htm](pfs-season-1-bestiary/2HEhlngODb1KfSwE.htm)|Cairn Wight (PFS 1-20)|
|[32ifzPZVkaLfVkDa.htm](pfs-season-1-bestiary/32ifzPZVkaLfVkDa.htm)|Collapsing Ceiling (1-2)|
|[3fctnHmcLQM3KrZj.htm](pfs-season-1-bestiary/3fctnHmcLQM3KrZj.htm)|Greater Shadow Wisp|
|[3MSKMI7ehLTC1bqw.htm](pfs-season-1-bestiary/3MSKMI7ehLTC1bqw.htm)|Webhekiz (5-6)|
|[3wNJeQbBO1FDg5I9.htm](pfs-season-1-bestiary/3wNJeQbBO1FDg5I9.htm)|Creeping Slime|
|[4MXoQZUrmGeuySrp.htm](pfs-season-1-bestiary/4MXoQZUrmGeuySrp.htm)|Meleeka Sanvara (3-4)|
|[4QEU9t2vYef8QfZQ.htm](pfs-season-1-bestiary/4QEU9t2vYef8QfZQ.htm)|Loyal Giant Fox|
|[54m8L1izUMDi02O9.htm](pfs-season-1-bestiary/54m8L1izUMDi02O9.htm)|Harsus (3-4)|
|[5hR3mQw2sEy3Fy8g.htm](pfs-season-1-bestiary/5hR3mQw2sEy3Fy8g.htm)|Ghost Of Diggen Thrune (3-4)|
|[5JcVwrLq7moBA3WE.htm](pfs-season-1-bestiary/5JcVwrLq7moBA3WE.htm)|Bijan And Zaynap Ohrlavi|
|[5ss5AIhNdmoreHcd.htm](pfs-season-1-bestiary/5ss5AIhNdmoreHcd.htm)|Kobold Scout (PFS 1-11)|
|[5tDBVocD4ScJHKQz.htm](pfs-season-1-bestiary/5tDBVocD4ScJHKQz.htm)|Nevashi (5-6)|
|[6dL5bhGVdRyID20x.htm](pfs-season-1-bestiary/6dL5bhGVdRyID20x.htm)|Mr. Chitters (7-8)|
|[6rjkzfJcwGUVq8Uo.htm](pfs-season-1-bestiary/6rjkzfJcwGUVq8Uo.htm)|Storm Warrior-Priest|
|[6T2Ui881qJflski6.htm](pfs-season-1-bestiary/6T2Ui881qJflski6.htm)|Krooth Hatchlings|
|[73j0kmPBzSCIOtq1.htm](pfs-season-1-bestiary/73j0kmPBzSCIOtq1.htm)|Deeply Flawed Ritual|
|[7467YYEHyLkJzXvU.htm](pfs-season-1-bestiary/7467YYEHyLkJzXvU.htm)|Technic Zombie (3-4)|
|[7aBQjC0nswiFuRAX.htm](pfs-season-1-bestiary/7aBQjC0nswiFuRAX.htm)|Spiky Pit (3-4)|
|[7PXPcv8x3Wnc730i.htm](pfs-season-1-bestiary/7PXPcv8x3Wnc730i.htm)|Prefect (Fist) (3-4)|
|[7VCfX1QpRb2T7324.htm](pfs-season-1-bestiary/7VCfX1QpRb2T7324.htm)|Muraxi (5-6)|
|[81H7cwCK8TBi6dnv.htm](pfs-season-1-bestiary/81H7cwCK8TBi6dnv.htm)|Weak Zephyr Hawk (PFS 1-24)|
|[86JAsGyUAfSviibC.htm](pfs-season-1-bestiary/86JAsGyUAfSviibC.htm)|Ahksiva (1-2)|
|[8axdoIyHw2WK9anh.htm](pfs-season-1-bestiary/8axdoIyHw2WK9anh.htm)|Ptiro Valner (3-4)|
|[8hdL1UOQZ1WPb6v7.htm](pfs-season-1-bestiary/8hdL1UOQZ1WPb6v7.htm)|Groetus's Chosen (3-4)|
|[8ndVAfhYn9kROK3R.htm](pfs-season-1-bestiary/8ndVAfhYn9kROK3R.htm)|Witchwyrd (PFS 1-21)|
|[8q3pSr4OLkcIDMIH.htm](pfs-season-1-bestiary/8q3pSr4OLkcIDMIH.htm)|Barralbus (3-4)|
|[8y0d3FLVkqbObMLM.htm](pfs-season-1-bestiary/8y0d3FLVkqbObMLM.htm)|Veteran Guard Captain|
|[91rxlFyg6uGC16RJ.htm](pfs-season-1-bestiary/91rxlFyg6uGC16RJ.htm)|Ankle Trap (1-2)|
|[9dnyyDG4MCkL3zkn.htm](pfs-season-1-bestiary/9dnyyDG4MCkL3zkn.htm)|Zombie Riding Horse|
|[9f5t3nOOqhNykqhj.htm](pfs-season-1-bestiary/9f5t3nOOqhNykqhj.htm)|Angry Fox|
|[9KauvSo8zxsaBWRc.htm](pfs-season-1-bestiary/9KauvSo8zxsaBWRc.htm)|Animal Companion (Wolf) (3-4)|
|[9ZqTtwmViROBjYZf.htm](pfs-season-1-bestiary/9ZqTtwmViROBjYZf.htm)|Goblin Skeleton|
|[a15tV7snudRPQXHO.htm](pfs-season-1-bestiary/a15tV7snudRPQXHO.htm)|Toxic Snapjaw (5-6)|
|[AaiDy0SMHX6beLgm.htm](pfs-season-1-bestiary/AaiDy0SMHX6beLgm.htm)|Season's Toll (5-6)|
|[AbFKkDIHUwx2EnR5.htm](pfs-season-1-bestiary/AbFKkDIHUwx2EnR5.htm)|Dorobu (3-4)|
|[AFQU2tJl09ykBkwY.htm](pfs-season-1-bestiary/AFQU2tJl09ykBkwY.htm)|Thorned Cocoon (3-4)|
|[aGLtnb99MyPf3gp4.htm](pfs-season-1-bestiary/aGLtnb99MyPf3gp4.htm)|Murderous Bathhouse (5-6)|
|[agRs8xhRc6QTi9lC.htm](pfs-season-1-bestiary/agRs8xhRc6QTi9lC.htm)|Port Peril Bar Fight (Rivanti's Bar) (1-2)|
|[Aidv4CvhXsSXKtON.htm](pfs-season-1-bestiary/Aidv4CvhXsSXKtON.htm)|Automatic Fire Suppression (5-6)|
|[AKNY8155nYQ1WsmG.htm](pfs-season-1-bestiary/AKNY8155nYQ1WsmG.htm)|Skeletal Mount|
|[anjyGCh5n1fj2kn3.htm](pfs-season-1-bestiary/anjyGCh5n1fj2kn3.htm)|Ruffian|
|[aWiBgV5B9sIO7UtN.htm](pfs-season-1-bestiary/aWiBgV5B9sIO7UtN.htm)|Human Bandit (3-4)|
|[awImEAjEPe1LOY8m.htm](pfs-season-1-bestiary/awImEAjEPe1LOY8m.htm)|Ralthiss (1-2)|
|[B437LbKlbmgXBeVV.htm](pfs-season-1-bestiary/B437LbKlbmgXBeVV.htm)|Port Peril Bar Fight (The Watchtower) (1-2)|
|[b7VoFnPimmOwx8nX.htm](pfs-season-1-bestiary/b7VoFnPimmOwx8nX.htm)|Lelzeshin (Tier 3-4)|
|[BcaaEonlKq3K7jxV.htm](pfs-season-1-bestiary/BcaaEonlKq3K7jxV.htm)|Steaming Fields (5-6)|
|[bDKozdvVEElVQywQ.htm](pfs-season-1-bestiary/bDKozdvVEElVQywQ.htm)|Fleshforge Dreg (Acid Spit)|
|[bfTmy4404MFMykxH.htm](pfs-season-1-bestiary/bfTmy4404MFMykxH.htm)|Dryad (1-16)|
|[BjkteD4admd5peO7.htm](pfs-season-1-bestiary/BjkteD4admd5peO7.htm)|Lightning Platform (7-8)|
|[bjugOjcHcBHgWTeZ.htm](pfs-season-1-bestiary/bjugOjcHcBHgWTeZ.htm)|Unkillable Zombie Brute (PFS 1-01)|
|[bM27sjs9yA3rX4DA.htm](pfs-season-1-bestiary/bM27sjs9yA3rX4DA.htm)|The Ascendant (5-6)|
|[bpCyglPCTMC8EL5w.htm](pfs-season-1-bestiary/bpCyglPCTMC8EL5w.htm)|Minor Summoning Rune|
|[BVgoWV3U3kHHAMg9.htm](pfs-season-1-bestiary/BVgoWV3U3kHHAMg9.htm)|Neidre Fliavazzana (3-4)|
|[BY9A7Isy3YWqUyyg.htm](pfs-season-1-bestiary/BY9A7Isy3YWqUyyg.htm)|Zombie Charger|
|[c6Nj3CaiHSXpfehS.htm](pfs-season-1-bestiary/c6Nj3CaiHSXpfehS.htm)|Crumbling Floor|
|[cG7wNXEp67uWgwfB.htm](pfs-season-1-bestiary/cG7wNXEp67uWgwfB.htm)|Injured Sewer Ooze|
|[cnmrSCuK8FuiBA7J.htm](pfs-season-1-bestiary/cnmrSCuK8FuiBA7J.htm)|Ogre Glutton (PFS 1-05)|
|[CPDWLOHl0ODbMplt.htm](pfs-season-1-bestiary/CPDWLOHl0ODbMplt.htm)|Dorobu (1-2)|
|[Cq7YWQF3qqkMF4oN.htm](pfs-season-1-bestiary/Cq7YWQF3qqkMF4oN.htm)|Port Peril Bar Fight (The Watchtower) (3-4)|
|[Cr0jrWe49odtef2P.htm](pfs-season-1-bestiary/Cr0jrWe49odtef2P.htm)|Raven Swarm (1-05)|
|[CsGxHwDh1y8Ti4mX.htm](pfs-season-1-bestiary/CsGxHwDh1y8Ti4mX.htm)|Boggard Pit (5-6)|
|[cuTgbvAxDEkT4XhZ.htm](pfs-season-1-bestiary/cuTgbvAxDEkT4XhZ.htm)|Fleshforge Dreg (Shield)|
|[czYt4Yp6ZeGnzyV6.htm](pfs-season-1-bestiary/czYt4Yp6ZeGnzyV6.htm)|Animal Companion (Wolf) (1-2)|
|[D2Bc4GbmiuW29310.htm](pfs-season-1-bestiary/D2Bc4GbmiuW29310.htm)|Elite Boggard Swampseer (PFS 1-24, Animal Staff)|
|[D8CPosS1fj2ZRBv0.htm](pfs-season-1-bestiary/D8CPosS1fj2ZRBv0.htm)|Elite Skeletal Giant (PFS 1-18)|
|[D9Ogfbn0ooPAqa9X.htm](pfs-season-1-bestiary/D9Ogfbn0ooPAqa9X.htm)|Elderly Giant Tarantula|
|[da3XtaZGVIYkfpKE.htm](pfs-season-1-bestiary/da3XtaZGVIYkfpKE.htm)|Weak Spider Swarm|
|[dagMCzX17LqZfuqW.htm](pfs-season-1-bestiary/dagMCzX17LqZfuqW.htm)|Collapsing Floor|
|[dbEMEKNFWOIQw4BQ.htm](pfs-season-1-bestiary/dbEMEKNFWOIQw4BQ.htm)|Mercenary|
|[DFu5ktENReo9lpbe.htm](pfs-season-1-bestiary/DFu5ktENReo9lpbe.htm)|Treacherous Quagmire|
|[Dhs9OY0qqZQAkbAp.htm](pfs-season-1-bestiary/Dhs9OY0qqZQAkbAp.htm)|Phoenix Rune (3-4)|
|[dOSeTKHADNhJ1bhz.htm](pfs-season-1-bestiary/dOSeTKHADNhJ1bhz.htm)|Kobold Dragon Mage (1-14)|
|[dSA9TMhvF4yw20c7.htm](pfs-season-1-bestiary/dSA9TMhvF4yw20c7.htm)|Kobold Warrior (PFS 1-11)|
|[DsEg9sT7jCcsuEDI.htm](pfs-season-1-bestiary/DsEg9sT7jCcsuEDI.htm)|Ogre|
|[dtXiSNcy7bM4UITo.htm](pfs-season-1-bestiary/dtXiSNcy7bM4UITo.htm)|Paravaax (3-4)|
|[E17afWSsCBGM8Gnq.htm](pfs-season-1-bestiary/E17afWSsCBGM8Gnq.htm)|Nalla, Rebel Leader (3-4)|
|[E2ExOJ9KFfGpw2H3.htm](pfs-season-1-bestiary/E2ExOJ9KFfGpw2H3.htm)|Chops (3-4)|
|[eBOY53WFYYksLmiH.htm](pfs-season-1-bestiary/eBOY53WFYYksLmiH.htm)|Dockhand Cultist|
|[Eccdm7gQHoppXFU5.htm](pfs-season-1-bestiary/Eccdm7gQHoppXFU5.htm)|Experienced Mercenary|
|[EGwZ6MSs8AVHf3q6.htm](pfs-season-1-bestiary/EGwZ6MSs8AVHf3q6.htm)|Rebel|
|[EKwisoYBBn9femPg.htm](pfs-season-1-bestiary/EKwisoYBBn9femPg.htm)|Osprey|
|[eR68t20AU5lPKALJ.htm](pfs-season-1-bestiary/eR68t20AU5lPKALJ.htm)|Voidworm Ouroboros|
|[Et2jn8eZ1nQStapE.htm](pfs-season-1-bestiary/Et2jn8eZ1nQStapE.htm)|Goblin Warrior (PFS 1-01)|
|[eWlUE6xrNLGMcfMX.htm](pfs-season-1-bestiary/eWlUE6xrNLGMcfMX.htm)|Halfling Druid (1-2)|
|[F4LvpkwzeT7spRX9.htm](pfs-season-1-bestiary/F4LvpkwzeT7spRX9.htm)|Harsus (1-2)|
|[F7gjEHGO2c956FEy.htm](pfs-season-1-bestiary/F7gjEHGO2c956FEy.htm)|Doctor Velshun (5-6)|
|[f9jyVOOKEvRkvIY4.htm](pfs-season-1-bestiary/f9jyVOOKEvRkvIY4.htm)|'Zombie' Flesh Golem (3-4)|
|[f9NDRa2grZrqnTIs.htm](pfs-season-1-bestiary/f9NDRa2grZrqnTIs.htm)|Webhekiz (3-4)|
|[FcqRRAWeUzV89Z2s.htm](pfs-season-1-bestiary/FcqRRAWeUzV89Z2s.htm)|Mitflit (PFS 1-02)|
|[ff0j2iOsGGONwOVw.htm](pfs-season-1-bestiary/ff0j2iOsGGONwOVw.htm)|Seaborn Captain|
|[FG0Cc949yZ8opW8I.htm](pfs-season-1-bestiary/FG0Cc949yZ8opW8I.htm)|Hogweed Leshy (1-2)|
|[fG2rxzrSiMS01Xqe.htm](pfs-season-1-bestiary/fG2rxzrSiMS01Xqe.htm)|Unkillable Zombie Brute (PFS 1-13)|
|[fK69SijSGN3bOgSE.htm](pfs-season-1-bestiary/fK69SijSGN3bOgSE.htm)|Plum Leshy (3-4)|
|[FN8SN6gIqQa9d8Ex.htm](pfs-season-1-bestiary/FN8SN6gIqQa9d8Ex.htm)|Enraged Raven Swarm|
|[FWNxPgsg0cfvrnY0.htm](pfs-season-1-bestiary/FWNxPgsg0cfvrnY0.htm)|Crowd with Trash|
|[fXIJWv2uooumqZo8.htm](pfs-season-1-bestiary/fXIJWv2uooumqZo8.htm)|'Zombie' Flesh Golem (5-6)|
|[FXwYc62zORdcfnYb.htm](pfs-season-1-bestiary/FXwYc62zORdcfnYb.htm)|Vengeant Thorn (3-4)|
|[gB7km6bV9dCXUQjZ.htm](pfs-season-1-bestiary/gB7km6bV9dCXUQjZ.htm)|Synthetic Khismar (7-8)|
|[GdYMRHVctjyvOD7D.htm](pfs-season-1-bestiary/GdYMRHVctjyvOD7D.htm)|Prefect (Fist) (5-6)|
|[glxrhALybtp9WYh9.htm](pfs-season-1-bestiary/glxrhALybtp9WYh9.htm)|Rabid Squirrel Swarm|
|[gq4YudXiHmpaTbbQ.htm](pfs-season-1-bestiary/gq4YudXiHmpaTbbQ.htm)|Malicious Spirits|
|[Gs2zYE86QGrDzCrv.htm](pfs-season-1-bestiary/Gs2zYE86QGrDzCrv.htm)|Human Bandit (PFS 1-06)|
|[Gsqdd6TysPcSd64h.htm](pfs-season-1-bestiary/Gsqdd6TysPcSd64h.htm)|Weak Arboreal Warden|
|[Gxy0zuO7aqC9NBv0.htm](pfs-season-1-bestiary/Gxy0zuO7aqC9NBv0.htm)|Little Zura (3-4)|
|[gZXQGOZO5XbWr9yD.htm](pfs-season-1-bestiary/gZXQGOZO5XbWr9yD.htm)|Elite Rebel|
|[H3Iz5JJCXBLxR499.htm](pfs-season-1-bestiary/H3Iz5JJCXBLxR499.htm)|Ankle Trap (3-4)|
|[HbMQ2PXNYZH69HDP.htm](pfs-season-1-bestiary/HbMQ2PXNYZH69HDP.htm)|Hungry Blade Recruit|
|[hevuaQkMCF9o5KZ2.htm](pfs-season-1-bestiary/hevuaQkMCF9o5KZ2.htm)|Zatqualmish (3-4)|
|[HffGyY6B9Kz20jOY.htm](pfs-season-1-bestiary/HffGyY6B9Kz20jOY.htm)|Lesser Giant Short-faced Bear|
|[HFzLYAANff4fQSJZ.htm](pfs-season-1-bestiary/HFzLYAANff4fQSJZ.htm)|Season's Toll (3-4)|
|[Hjl0cYnMyrcsbKlY.htm](pfs-season-1-bestiary/Hjl0cYnMyrcsbKlY.htm)|Cinder Wolf (3-4)|
|[HmyF3aYbHxm8RDWf.htm](pfs-season-1-bestiary/HmyF3aYbHxm8RDWf.htm)|Quagmire|
|[hNAN6bqWxFKjxR4Q.htm](pfs-season-1-bestiary/hNAN6bqWxFKjxR4Q.htm)|Reast Mycer (3-4)|
|[hpz50kmKMKVQWxW9.htm](pfs-season-1-bestiary/hpz50kmKMKVQWxW9.htm)|Scrabbling Hands|
|[HYUQOdH0WB6ckC5e.htm](pfs-season-1-bestiary/HYUQOdH0WB6ckC5e.htm)|Moldy Foodstuffs (3-4)|
|[I3jGuCG3qL5ua7yE.htm](pfs-season-1-bestiary/I3jGuCG3qL5ua7yE.htm)|Glass River Midge|
|[I3RciQPRS1LWb5Sv.htm](pfs-season-1-bestiary/I3RciQPRS1LWb5Sv.htm)|Spitting Acid 'Bushes' (7-8)|
|[i4PIffSaQO2ry5nb.htm](pfs-season-1-bestiary/i4PIffSaQO2ry5nb.htm)|Kobold Warrior (1-14)|
|[I4Vcd1ce8ltK8OqJ.htm](pfs-season-1-bestiary/I4Vcd1ce8ltK8OqJ.htm)|Ruffian (Q11)|
|[IcbDCGnefdowfQE0.htm](pfs-season-1-bestiary/IcbDCGnefdowfQE0.htm)|Skeleton Captain|
|[ihm2nXJtzcgFUJRN.htm](pfs-season-1-bestiary/ihm2nXJtzcgFUJRN.htm)|Statue Of Set|
|[iozA5orydjkBmbHx.htm](pfs-season-1-bestiary/iozA5orydjkBmbHx.htm)|Farmer Drystan's Scarecrow (3-4)|
|[iPInBeC8feDsmP2e.htm](pfs-season-1-bestiary/iPInBeC8feDsmP2e.htm)|Moldy Deck|
|[IsNHPVT7c6K42rIR.htm](pfs-season-1-bestiary/IsNHPVT7c6K42rIR.htm)|Malfunctioning Annihilator|
|[JDjwID3HZJqytPFP.htm](pfs-season-1-bestiary/JDjwID3HZJqytPFP.htm)|Pine Brute|
|[jEjUHCcGcGinFqOU.htm](pfs-season-1-bestiary/jEjUHCcGcGinFqOU.htm)|Chops (1-2)|
|[JEwMnYFfyxr5i6Dy.htm](pfs-season-1-bestiary/JEwMnYFfyxr5i6Dy.htm)|Fleshforge Dreg (Roots)|
|[JF257wpwvWWjCsN7.htm](pfs-season-1-bestiary/JF257wpwvWWjCsN7.htm)|Reefclaw (PFS 1-12)|
|[Jfm63bPimFxf3u1T.htm](pfs-season-1-bestiary/Jfm63bPimFxf3u1T.htm)|Halfling Druid (3-4)|
|[jJd7Sa8DB7uBOn93.htm](pfs-season-1-bestiary/jJd7Sa8DB7uBOn93.htm)|Bandit Disciple (3-4)|
|[jkGd8nN4hnNLGS2D.htm](pfs-season-1-bestiary/jkGd8nN4hnNLGS2D.htm)|Mummy Shambler|
|[jMjK6j16K1NC2BdC.htm](pfs-season-1-bestiary/jMjK6j16K1NC2BdC.htm)|Weak Soulbound Homunculus|
|[js2sA6P939oTuLuI.htm](pfs-season-1-bestiary/js2sA6P939oTuLuI.htm)|Ahrkinos (5-6)|
|[JSZGXyG3cItFmG9N.htm](pfs-season-1-bestiary/JSZGXyG3cItFmG9N.htm)|Elite Waterworks Rebel|
|[jXEmUa47j5o3lQIE.htm](pfs-season-1-bestiary/jXEmUa47j5o3lQIE.htm)|Barbed Bloodseeker|
|[K6JYKFI9HLFWf12d.htm](pfs-season-1-bestiary/K6JYKFI9HLFWf12d.htm)|Guard Captain|
|[k76O7wKWMJrycd9i.htm](pfs-season-1-bestiary/k76O7wKWMJrycd9i.htm)|Collapsing Barricade (1-2)|
|[KcLEkekZZw9uI88I.htm](pfs-season-1-bestiary/KcLEkekZZw9uI88I.htm)|Toxic Snapjaw (3-4)|
|[kDEiL11fbXWHQrdH.htm](pfs-season-1-bestiary/kDEiL11fbXWHQrdH.htm)|The Ascendant (3-4)|
|[KFAjzSMh6HAyJMhd.htm](pfs-season-1-bestiary/KFAjzSMh6HAyJMhd.htm)|Port Peril Bar Fight (Last Catch) (1-2)|
|[kfoGOOgTkRjIzD4U.htm](pfs-season-1-bestiary/kfoGOOgTkRjIzD4U.htm)|Kanker|
|[KL2L7m5WlL21WcGN.htm](pfs-season-1-bestiary/KL2L7m5WlL21WcGN.htm)|Port Peril Bar Fight (Rivanti's Bar) (3-4)|
|[KorFY6YT3UBrzXOL.htm](pfs-season-1-bestiary/KorFY6YT3UBrzXOL.htm)|Spitting Acid 'Bushes' (5-6)|
|[KoYqhZoY3RSBEo5Y.htm](pfs-season-1-bestiary/KoYqhZoY3RSBEo5Y.htm)|Submerged Shard|
|[kpYq0piOlafgYSdo.htm](pfs-season-1-bestiary/kpYq0piOlafgYSdo.htm)|Ghost Of Diggen Thrune (1-2)|
|[KR3t3QnVESdhkYr2.htm](pfs-season-1-bestiary/KR3t3QnVESdhkYr2.htm)|Barralbus (1-2)|
|[ksClnBrkXAhoq4Di.htm](pfs-season-1-bestiary/ksClnBrkXAhoq4Di.htm)|Groetus's Chosen (1-2)|
|[KsTZYCptXLSGgrcW.htm](pfs-season-1-bestiary/KsTZYCptXLSGgrcW.htm)|Whispering Spirits|
|[Kv7ciLOLN8vDgIGy.htm](pfs-season-1-bestiary/Kv7ciLOLN8vDgIGy.htm)|Kobold Dragon Mage (PFS 1-11)|
|[Kz4YDRpaUaL3BQd7.htm](pfs-season-1-bestiary/Kz4YDRpaUaL3BQd7.htm)|Spear Launcher (Q9:1-2)|
|[l8PzmglYbEtM5Xy6.htm](pfs-season-1-bestiary/l8PzmglYbEtM5Xy6.htm)|Squirrel Swarm (1-04)|
|[LAo7jI8NvMfuDbek.htm](pfs-season-1-bestiary/LAo7jI8NvMfuDbek.htm)|Great White Shark (PFS 1-12)|
|[LAZDcdX1ISXJCqPd.htm](pfs-season-1-bestiary/LAZDcdX1ISXJCqPd.htm)|Steel Mephit|
|[lhSKgDzZm8FZknvG.htm](pfs-season-1-bestiary/lhSKgDzZm8FZknvG.htm)|Kobold Scout (1-14)|
|[LKdTBOV9LUiFrKOU.htm](pfs-season-1-bestiary/LKdTBOV9LUiFrKOU.htm)|Sea Devil Invader|
|[LMyYMgdcJqQPgGiY.htm](pfs-season-1-bestiary/LMyYMgdcJqQPgGiY.htm)|Iloise (7-8)|
|[loJBcC34yhpVwfio.htm](pfs-season-1-bestiary/loJBcC34yhpVwfio.htm)|Rending Hands|
|[LOTsEiTCiONfJl3s.htm](pfs-season-1-bestiary/LOTsEiTCiONfJl3s.htm)|Lion|
|[lvWJic1WYdyJATa0.htm](pfs-season-1-bestiary/lvWJic1WYdyJATa0.htm)|Manifestation of Qxal (3-4)|
|[LXXodovLOERAGqWj.htm](pfs-season-1-bestiary/LXXodovLOERAGqWj.htm)|Sodden Floor|
|[lzDOAywWh0mP4UeH.htm](pfs-season-1-bestiary/lzDOAywWh0mP4UeH.htm)|Fleshforge Prototype (Acid Spit)|
|[m3YBXS3ool7PmRfX.htm](pfs-season-1-bestiary/m3YBXS3ool7PmRfX.htm)|Fleshforge Prototype (Shield)|
|[mhKK0mhhI9V3OiUQ.htm](pfs-season-1-bestiary/mhKK0mhhI9V3OiUQ.htm)|Acid Spray Fountain|
|[mIz8CjpB8uxf6M9f.htm](pfs-season-1-bestiary/mIz8CjpB8uxf6M9f.htm)|Cinder Wolf (1-2)|
|[mjD8kNTb8IC5FBNA.htm](pfs-season-1-bestiary/mjD8kNTb8IC5FBNA.htm)|Ravenous Giant Centipede|
|[MJtndSx73zEx8702.htm](pfs-season-1-bestiary/MJtndSx73zEx8702.htm)|Guard|
|[mjW1q98gMnEmZJKt.htm](pfs-season-1-bestiary/mjW1q98gMnEmZJKt.htm)|Shadow Wisp|
|[MOPGJAQUS4Aop7Mm.htm](pfs-season-1-bestiary/MOPGJAQUS4Aop7Mm.htm)|Doctor Velshun (7-8)|
|[MOX8VzhEBVMkOpzr.htm](pfs-season-1-bestiary/MOX8VzhEBVMkOpzr.htm)|Crowd Agitator|
|[mTXgvQ7zhA8VNVeG.htm](pfs-season-1-bestiary/mTXgvQ7zhA8VNVeG.htm)|Orc Alchemist|
|[mUWCqhhcmEHsSjKo.htm](pfs-season-1-bestiary/mUWCqhhcmEHsSjKo.htm)|Blue Streak Mage (5-6)|
|[mWzScgUiWAGEb0RE.htm](pfs-season-1-bestiary/mWzScgUiWAGEb0RE.htm)|Human Bandit (5-6)|
|[N6zflwcAIP2wewRI.htm](pfs-season-1-bestiary/N6zflwcAIP2wewRI.htm)|Skeleton Soldier|
|[Nh3mKGv0uy1tovpL.htm](pfs-season-1-bestiary/Nh3mKGv0uy1tovpL.htm)|Keff The Lion (3-4)|
|[NhMS6b3W6gXefijq.htm](pfs-season-1-bestiary/NhMS6b3W6gXefijq.htm)|Offended High Roller|
|[NmBBGhDNNEEfxCun.htm](pfs-season-1-bestiary/NmBBGhDNNEEfxCun.htm)|Skeletal Gladiator|
|[NmkeNdV7SWYG7bYe.htm](pfs-season-1-bestiary/NmkeNdV7SWYG7bYe.htm)|Vengeant Thorn (1-2)|
|[nmNVx7cAqeurlbeu.htm](pfs-season-1-bestiary/nmNVx7cAqeurlbeu.htm)|Hungry Blade Apprentice|
|[NQgsuMnXXABeebuS.htm](pfs-season-1-bestiary/NQgsuMnXXABeebuS.htm)|Prefect (Electrowhip) (3-4)|
|[Nr8LlIa0kRW71fJs.htm](pfs-season-1-bestiary/Nr8LlIa0kRW71fJs.htm)|Phoenix Rune (1-2)|
|[NTPKBjuubDmuSJTN.htm](pfs-season-1-bestiary/NTPKBjuubDmuSJTN.htm)|Vicious Vulpine|
|[NUFsxlmHqk4LeYlq.htm](pfs-season-1-bestiary/NUFsxlmHqk4LeYlq.htm)|Greater Metallic Sod Hound|
|[NZvXkQHfXM2XS50I.htm](pfs-season-1-bestiary/NZvXkQHfXM2XS50I.htm)|Weak Green Hag|
|[O6YxMaRh6tRGnnbl.htm](pfs-season-1-bestiary/O6YxMaRh6tRGnnbl.htm)|Biloko Veteran (1-16)|
|[ocdy8RFkw10tCUuY.htm](pfs-season-1-bestiary/ocdy8RFkw10tCUuY.htm)|Biloko Warrior (1-16)|
|[oEHGUzWB0IH26h7y.htm](pfs-season-1-bestiary/oEHGUzWB0IH26h7y.htm)|Mummy Brute|
|[oGYYCFUoopC8MSYS.htm](pfs-season-1-bestiary/oGYYCFUoopC8MSYS.htm)|Deadfall Trap (1-2)|
|[oHbuqmMgS8PutzMZ.htm](pfs-season-1-bestiary/oHbuqmMgS8PutzMZ.htm)|Blue Streak Mage (7-8)|
|[oKACWOSiBpxgMqa4.htm](pfs-season-1-bestiary/oKACWOSiBpxgMqa4.htm)|Prefect (Electrowhip) (5-6)|
|[OOG9wbIY1mC6ehzS.htm](pfs-season-1-bestiary/OOG9wbIY1mC6ehzS.htm)|Dead Organ (5-6)|
|[ORzg1jprbdWl64gA.htm](pfs-season-1-bestiary/ORzg1jprbdWl64gA.htm)|Murderous Bathhouse (3-4)|
|[OTTvIkBvJ2ABYwEd.htm](pfs-season-1-bestiary/OTTvIkBvJ2ABYwEd.htm)|Whispering Souls|
|[OtYNhDlyXrVWxeiS.htm](pfs-season-1-bestiary/OtYNhDlyXrVWxeiS.htm)|Damaged Alchemical Golem|
|[oVg5QxC9vf0QQQeb.htm](pfs-season-1-bestiary/oVg5QxC9vf0QQQeb.htm)|Port Peril Bar Fight (Last Catch) (3-4)|
|[peNTVZwcm8Tg7s2e.htm](pfs-season-1-bestiary/peNTVZwcm8Tg7s2e.htm)|Blue Streak Sentry|
|[PFSkqGyg7eGaES1k.htm](pfs-season-1-bestiary/PFSkqGyg7eGaES1k.htm)|Technic Zombie (5-6)|
|[pG7P9AxqzcsCjikh.htm](pfs-season-1-bestiary/pG7P9AxqzcsCjikh.htm)|Zombie Shambler (PFS 1-03)|
|[pJck5AXRMWcAequ5.htm](pfs-season-1-bestiary/pJck5AXRMWcAequ5.htm)|Boggard Warrior (PFS 1-09)|
|[pPpc1IevAeLYHZsh.htm](pfs-season-1-bestiary/pPpc1IevAeLYHZsh.htm)|Ahksiva (3-4)|
|[Pq4fqQkoghoXPBoi.htm](pfs-season-1-bestiary/Pq4fqQkoghoXPBoi.htm)|Young Bloodseeker Swarm|
|[PQaQSSjfkoV9mZzi.htm](pfs-season-1-bestiary/PQaQSSjfkoV9mZzi.htm)|Giant Short-faced Bear|
|[pSQYsY76zjDMfjv2.htm](pfs-season-1-bestiary/pSQYsY76zjDMfjv2.htm)|Blue Streak Burglar|
|[ptDF1ghCxNHGhH0h.htm](pfs-season-1-bestiary/ptDF1ghCxNHGhH0h.htm)|Greater Steel Mephit|
|[PtU1netfc7U8MaR4.htm](pfs-season-1-bestiary/PtU1netfc7U8MaR4.htm)|Kip The Druid (1-2)|
|[pWJUeb0LfCXFSYpE.htm](pfs-season-1-bestiary/pWJUeb0LfCXFSYpE.htm)|Dilapidated Alchemical Golem|
|[PX7QH8D43Bax2LC4.htm](pfs-season-1-bestiary/PX7QH8D43Bax2LC4.htm)|Moldy Foodstuffs (1-2)|
|[pxDyzHzkEQTz3R1K.htm](pfs-season-1-bestiary/pxDyzHzkEQTz3R1K.htm)|Leshy Crafter (1-2)|
|[PXXHRzLRLB4bJ7M5.htm](pfs-season-1-bestiary/PXXHRzLRLB4bJ7M5.htm)|Fleshforge Dreg (Reach)|
|[Qcl0FPQS2Ovr179p.htm](pfs-season-1-bestiary/Qcl0FPQS2Ovr179p.htm)|Collapsing Ceiling (3-4)|
|[Qeu2rsI1J5nohgm7.htm](pfs-season-1-bestiary/Qeu2rsI1J5nohgm7.htm)|Giant Centipede (PFS 1-03)|
|[qgWB2lP5EyaHsvKH.htm](pfs-season-1-bestiary/qgWB2lP5EyaHsvKH.htm)|Summoning Rune (PFS 1-11)|
|[QMT1Yfa2IrNrdZGP.htm](pfs-season-1-bestiary/QMT1Yfa2IrNrdZGP.htm)|Dire Cinder Wolf|
|[QtDH32oEVWpFmlCi.htm](pfs-season-1-bestiary/QtDH32oEVWpFmlCi.htm)|Lelzeshin (Tier 5-6)|
|[QUimZFYGY12QjoC4.htm](pfs-season-1-bestiary/QUimZFYGY12QjoC4.htm)|Kip The Druid (3-4)|
|[qumj2VUZ6aTnjjIU.htm](pfs-season-1-bestiary/qumj2VUZ6aTnjjIU.htm)|Plum Leshy (1-2)|
|[qXd4fcaY4cWKvj3G.htm](pfs-season-1-bestiary/qXd4fcaY4cWKvj3G.htm)|Spear Launcher (Q9:3-4)|
|[QxlguPRUm3rDPhjF.htm](pfs-season-1-bestiary/QxlguPRUm3rDPhjF.htm)|Waterworks Rebel|
|[QYTRwhdkSWZKRtlj.htm](pfs-season-1-bestiary/QYTRwhdkSWZKRtlj.htm)|Synthetic Khismar (5-6)|
|[QZVLjVPLl0PNo2K3.htm](pfs-season-1-bestiary/QZVLjVPLl0PNo2K3.htm)|Fleshforge Prototype (Leaping Charge)|
|[R8D2GrilngQIb1eI.htm](pfs-season-1-bestiary/R8D2GrilngQIb1eI.htm)|Fleshforge Prototype (Reach)|
|[rEPnlpuRVCpnQNYP.htm](pfs-season-1-bestiary/rEPnlpuRVCpnQNYP.htm)|Zombie Warhorse|
|[rJ9IdtFmjnqtN02o.htm](pfs-season-1-bestiary/rJ9IdtFmjnqtN02o.htm)|Palace Guard (PFS 1-17)|
|[RLT5yfpN3FIgs3b6.htm](pfs-season-1-bestiary/RLT5yfpN3FIgs3b6.htm)|Spiky Pit (1-2)|
|[rOYazaPBX73nLCJE.htm](pfs-season-1-bestiary/rOYazaPBX73nLCJE.htm)|Manifestation of Qxal (5-6)|
|[rrlWzq8qKpOgaq7P.htm](pfs-season-1-bestiary/rrlWzq8qKpOgaq7P.htm)|Mountain Goat|
|[RS04fRQEWzk2aZOx.htm](pfs-season-1-bestiary/RS04fRQEWzk2aZOx.htm)|Desert Wind|
|[RUXPx9VPJCmuD1V8.htm](pfs-season-1-bestiary/RUXPx9VPJCmuD1V8.htm)|Mr. Chitters (5-6)|
|[RYhsJ69TGrcvtfaM.htm](pfs-season-1-bestiary/RYhsJ69TGrcvtfaM.htm)|Giant Osprey|
|[RzLRzsHVjaPBKCkh.htm](pfs-season-1-bestiary/RzLRzsHVjaPBKCkh.htm)|Hogweed Leshy (3-4)|
|[S50loNsuTigldGll.htm](pfs-season-1-bestiary/S50loNsuTigldGll.htm)|Bandit Disciple (1-2)|
|[Si5l2FvDCCmG6DRa.htm](pfs-season-1-bestiary/Si5l2FvDCCmG6DRa.htm)|Skeleton Corporal|
|[sLt5RvozduZ2soA5.htm](pfs-season-1-bestiary/sLt5RvozduZ2soA5.htm)|Little Zura (1-2)|
|[sN5jueRXgYI9DDGH.htm](pfs-season-1-bestiary/sN5jueRXgYI9DDGH.htm)|Fleshforge Prototype (Breath Weapon)|
|[SprnhrgMbZxb59fA.htm](pfs-season-1-bestiary/SprnhrgMbZxb59fA.htm)|Nalla, Rebel Leader (1-2)|
|[stqY18rymB2XUSHq.htm](pfs-season-1-bestiary/stqY18rymB2XUSHq.htm)|Dead Organ (7-8)|
|[t1QyIgwLORMhHNL4.htm](pfs-season-1-bestiary/t1QyIgwLORMhHNL4.htm)|Fleshforge Dreg (Breath Weapon)|
|[t238i4Zw2JsDvUop.htm](pfs-season-1-bestiary/t238i4Zw2JsDvUop.htm)|Metallic Sod Hound|
|[t93p04HpLq4O2BjR.htm](pfs-season-1-bestiary/t93p04HpLq4O2BjR.htm)|Boggard Pit (3-4)|
|[tBY1ayiQyI5tyME6.htm](pfs-season-1-bestiary/tBY1ayiQyI5tyME6.htm)|Ptiro Valner (1-2)|
|[tDKhy3OvsOX8Hzik.htm](pfs-season-1-bestiary/tDKhy3OvsOX8Hzik.htm)|Nashaxian The Bored|
|[TmDKaw1WenWKvNBU.htm](pfs-season-1-bestiary/TmDKaw1WenWKvNBU.htm)|Tough Mountain Goat|
|[Tnq5pjDMMSmPkBtB.htm](pfs-season-1-bestiary/Tnq5pjDMMSmPkBtB.htm)|Ralthiss (3-4)|
|[toAsLLjdRwu1pT0b.htm](pfs-season-1-bestiary/toAsLLjdRwu1pT0b.htm)|Fleshforge Prototype (Roots)|
|[tShwzNRi8eEqznAi.htm](pfs-season-1-bestiary/tShwzNRi8eEqznAi.htm)|Collapsing Barricade (3-4)|
|[tt4tZR8udjJ1zNyp.htm](pfs-season-1-bestiary/tt4tZR8udjJ1zNyp.htm)|Seaborn Fisher|
|[ttGtrGAOC2QRBWjn.htm](pfs-season-1-bestiary/ttGtrGAOC2QRBWjn.htm)|Trapmaster Tok's Surprise (1-2)|
|[tuuajdgozH7Tueiv.htm](pfs-season-1-bestiary/tuuajdgozH7Tueiv.htm)|Elite Sewer Ooze (Q9)|
|[U2igHGiYNNclSdsm.htm](pfs-season-1-bestiary/U2igHGiYNNclSdsm.htm)|Spirit Guardian Statue|
|[u2TGKzPfGniZLL14.htm](pfs-season-1-bestiary/u2TGKzPfGniZLL14.htm)|Flesh Golem (PFS 1-25)|
|[U6mEWRAurIbSgtnF.htm](pfs-season-1-bestiary/U6mEWRAurIbSgtnF.htm)|Giant Shrew|
|[uedXIip0UPHJ4Egt.htm](pfs-season-1-bestiary/uedXIip0UPHJ4Egt.htm)|Flaming Skull Skeleton Guard|
|[UGB9w4JLH8GFhew7.htm](pfs-season-1-bestiary/UGB9w4JLH8GFhew7.htm)|Jury-Rigged Annihilator|
|[Uhb6f4ettpS7K2Jl.htm](pfs-season-1-bestiary/Uhb6f4ettpS7K2Jl.htm)|Blue Streak Ambushers (7-8)|
|[UjVrEU4MTBH2eZOq.htm](pfs-season-1-bestiary/UjVrEU4MTBH2eZOq.htm)|Weak Ceustodaemon (PFS 1-01)|
|[UKGbCdZlsWmjZiFN.htm](pfs-season-1-bestiary/UKGbCdZlsWmjZiFN.htm)|Automatic Fire Suppression (3-4)|
|[ul23A9ZfCMCUwADK.htm](pfs-season-1-bestiary/ul23A9ZfCMCUwADK.htm)|Jinkin (PFS 1-17)|
|[UlBksr4oy0gT3b3g.htm](pfs-season-1-bestiary/UlBksr4oy0gT3b3g.htm)|Guard (PFS 1-17)|
|[uQdILi1siFfXewFf.htm](pfs-season-1-bestiary/uQdILi1siFfXewFf.htm)|Zombie Minihulk|
|[UQyvGqfFcYva3fzc.htm](pfs-season-1-bestiary/UQyvGqfFcYva3fzc.htm)|Zatqualmish (5-6)|
|[uRUiLihSFDU78IVP.htm](pfs-season-1-bestiary/uRUiLihSFDU78IVP.htm)|Aeon Nexus (5-6)|
|[ut2cLLH0tvJV2ewg.htm](pfs-season-1-bestiary/ut2cLLH0tvJV2ewg.htm)|Elite Ghast (PFS 1-07)|
|[uwAyhK6sEoOyoR96.htm](pfs-season-1-bestiary/uwAyhK6sEoOyoR96.htm)|Vigilant Guard|
|[UyJUEp0asfGDBvYa.htm](pfs-season-1-bestiary/UyJUEp0asfGDBvYa.htm)|Elite Prefect (Fist) (3-4)|
|[uZWktkWAmzadBTh1.htm](pfs-season-1-bestiary/uZWktkWAmzadBTh1.htm)|Thorned Cocoon (5-6)|
|[V0Lsna2i106JZYHY.htm](pfs-season-1-bestiary/V0Lsna2i106JZYHY.htm)|Flawed Ritual|
|[v4XGvSCIj9dVS5Z9.htm](pfs-season-1-bestiary/v4XGvSCIj9dVS5Z9.htm)|Steaming Fields (7-8)|
|[v8Spc0ALJdX8WUi4.htm](pfs-season-1-bestiary/v8Spc0ALJdX8WUi4.htm)|Eerie Skeletal Giant|
|[vgn39Yta3lgeRP1s.htm](pfs-season-1-bestiary/vgn39Yta3lgeRP1s.htm)|Muraxi (3-4)|
|[vLOwfWlS2XQQYhD2.htm](pfs-season-1-bestiary/vLOwfWlS2XQQYhD2.htm)|Raging Forest Fire|
|[vtSVLCmJvef8980U.htm](pfs-season-1-bestiary/vtSVLCmJvef8980U.htm)|Leopard (1-14)|
|[vV6a488IlsKsHpL8.htm](pfs-season-1-bestiary/vV6a488IlsKsHpL8.htm)|Ahrkinos (3-4)|
|[vve98ekdPx8nJbF1.htm](pfs-season-1-bestiary/vve98ekdPx8nJbF1.htm)|Injured Weak Otyugh|
|[w8pkSmcvpZfp7Hsw.htm](pfs-season-1-bestiary/w8pkSmcvpZfp7Hsw.htm)|Larraz Virtanne|
|[wctTtNkLjyMeCCi2.htm](pfs-season-1-bestiary/wctTtNkLjyMeCCi2.htm)|Skeletal Mage|
|[wCZcZnEHskvccWJE.htm](pfs-season-1-bestiary/wCZcZnEHskvccWJE.htm)|Young Sea Serpent|
|[WdmbEmvLzPciM7au.htm](pfs-season-1-bestiary/WdmbEmvLzPciM7au.htm)|Adolescent Sea Serpent|
|[wGjNDVC0mDsk7rrH.htm](pfs-season-1-bestiary/wGjNDVC0mDsk7rrH.htm)|Pile of Fireworks (3-4)|
|[whbIBJJxlv5oVm6c.htm](pfs-season-1-bestiary/whbIBJJxlv5oVm6c.htm)|Leshy Crafter (3-4)|
|[WlJ8fVMV7gXNQ9eR.htm](pfs-season-1-bestiary/WlJ8fVMV7gXNQ9eR.htm)|Fleshforge Dreg (Leaping Charge)|
|[WM8ycweNEOEFE08F.htm](pfs-season-1-bestiary/WM8ycweNEOEFE08F.htm)|Aeon Nexus (3-4)|
|[wPe1IWcxMinHkuJE.htm](pfs-season-1-bestiary/wPe1IWcxMinHkuJE.htm)|Vengeful Spirits|
|[WT62cRWceB4NlSE8.htm](pfs-season-1-bestiary/WT62cRWceB4NlSE8.htm)|Smoldering Forest Fire|
|[wv8PrDy91ae3njBv.htm](pfs-season-1-bestiary/wv8PrDy91ae3njBv.htm)|Rebel Brute|
|[X6xVydw9XznvvFC2.htm](pfs-season-1-bestiary/X6xVydw9XznvvFC2.htm)|Mirrored Lelzeshin (Tier 3-4)|
|[xbpF7O68n876hD3B.htm](pfs-season-1-bestiary/xbpF7O68n876hD3B.htm)|Storm Acolyte|
|[xFeeszDfh4RISbib.htm](pfs-season-1-bestiary/xFeeszDfh4RISbib.htm)|Gwibble (1-2)|
|[xHbU1j5KpscKk6q8.htm](pfs-season-1-bestiary/xHbU1j5KpscKk6q8.htm)|Marcon Tinol (3-4)|
|[XiGTud90i1WkJYz2.htm](pfs-season-1-bestiary/XiGTud90i1WkJYz2.htm)|Offended Gambler|
|[xIWdGhSA6Z7CQKBY.htm](pfs-season-1-bestiary/xIWdGhSA6Z7CQKBY.htm)|Marcon Tinol (1-2)|
|[xKfj6ctW8fKEfrxp.htm](pfs-season-1-bestiary/xKfj6ctW8fKEfrxp.htm)|Deadfall Trap (3-4)|
|[xu1dm0KnEHx5vySR.htm](pfs-season-1-bestiary/xu1dm0KnEHx5vySR.htm)|Elite Glass River Midge|
|[XVoGQC0ret7XiGZD.htm](pfs-season-1-bestiary/XVoGQC0ret7XiGZD.htm)|Dehydrated Krooth|
|[xwLdaMLETLv6XSlK.htm](pfs-season-1-bestiary/xwLdaMLETLv6XSlK.htm)|Grasping Limbs (3-4)|
|[y14EZYMrfejrAuPI.htm](pfs-season-1-bestiary/y14EZYMrfejrAuPI.htm)|Frog Swarm|
|[yAZBjwCVDGbdT4Sb.htm](pfs-season-1-bestiary/yAZBjwCVDGbdT4Sb.htm)|Mirrored Lelzeshin (Tier 5-6)|
|[yn7x4Me1YrPuXFZf.htm](pfs-season-1-bestiary/yn7x4Me1YrPuXFZf.htm)|Keff The Lion (1-2)|
|[ySXw00WW2LJoDhZ6.htm](pfs-season-1-bestiary/ySXw00WW2LJoDhZ6.htm)|Crowd Leader|
|[YtkTx8318EKs43yn.htm](pfs-season-1-bestiary/YtkTx8318EKs43yn.htm)|Grasping Limbs (5-6)|
|[YtR25uDQSkKQMoaW.htm](pfs-season-1-bestiary/YtR25uDQSkKQMoaW.htm)|Reast Mycer (1-2)|
|[z8Bp3EtiJhA3nNrY.htm](pfs-season-1-bestiary/z8Bp3EtiJhA3nNrY.htm)|Flaming Skull Skeletal Champion|
|[zAOgSgAqMLCrIvTl.htm](pfs-season-1-bestiary/zAOgSgAqMLCrIvTl.htm)|Blue Streak Ambushers (5-6)|
|[zE9WuJXq3I4uZNNX.htm](pfs-season-1-bestiary/zE9WuJXq3I4uZNNX.htm)|Neidre Fliavazzana (5-6)|
|[zgvD5MpjPBJXIian.htm](pfs-season-1-bestiary/zgvD5MpjPBJXIian.htm)|Elite Duergar Taskmaster|
|[zJhgmaI99nePKSaY.htm](pfs-season-1-bestiary/zJhgmaI99nePKSaY.htm)|Boggard Swampseer (PFS 1-24)|
|[zM01pTDcCS3F6z2V.htm](pfs-season-1-bestiary/zM01pTDcCS3F6z2V.htm)|Iloise (5-6)|
|[ZMGXiRuVZ8r7hQVe.htm](pfs-season-1-bestiary/ZMGXiRuVZ8r7hQVe.htm)|Bandit Scout|
|[ZpowNcW774gt8XjS.htm](pfs-season-1-bestiary/ZpowNcW774gt8XjS.htm)|Nashaxian The Angered|
|[zqZu0W4L5HCUaKw7.htm](pfs-season-1-bestiary/zqZu0W4L5HCUaKw7.htm)|Crowd with Chamberpots|
|[ZUwhZ8D3TpH2ctzo.htm](pfs-season-1-bestiary/ZUwhZ8D3TpH2ctzo.htm)|Pile of Fireworks (5-6)|
|[zveTUg1NtYmTwlFn.htm](pfs-season-1-bestiary/zveTUg1NtYmTwlFn.htm)|Dire Warg|
|[ZX2dP4rP01qaCJh4.htm](pfs-season-1-bestiary/ZX2dP4rP01qaCJh4.htm)|Protoplasmic Extruder|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[6RnFVhsi5XImLuuF.htm](pfs-season-1-bestiary/6RnFVhsi5XImLuuF.htm)|Charlatan|Charlatan|libre|
|[8G0V3AjaLUkTbq0H.htm](pfs-season-1-bestiary/8G0V3AjaLUkTbq0H.htm)|Mitflit Sneak|Mitflit furtif|libre|
|[oQmjMw2MTQDSGRs7.htm](pfs-season-1-bestiary/oQmjMw2MTQDSGRs7.htm)|Gwibble (3-4)|Gwibble (3-4)|libre|
|[xKanh4M6QEF39bvd.htm](pfs-season-1-bestiary/xKanh4M6QEF39bvd.htm)|Zombie Brute (PFS 1-18)|Brute zombie (PFS 1-18)|libre|
