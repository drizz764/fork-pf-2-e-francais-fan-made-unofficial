# État de la traduction (pfs-season-3-bestiary)

 * **aucune**: 253
 * **libre**: 9
 * **changé**: 16


Dernière mise à jour: 2024-06-13 06:52 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[06XCiuS7ZDEM5Xpf.htm](pfs-season-3-bestiary/06XCiuS7ZDEM5Xpf.htm)|Filth Fire (PFS 3-01)|
|[0T4gSWRTGUN0aFzT.htm](pfs-season-3-bestiary/0T4gSWRTGUN0aFzT.htm)|Natron Explosion|
|[1NWTUcGckuJ2qC5I.htm](pfs-season-3-bestiary/1NWTUcGckuJ2qC5I.htm)|Overloading Arch (5-6)|
|[1OHuJmY24AnfJHpz.htm](pfs-season-3-bestiary/1OHuJmY24AnfJHpz.htm)|Bergworm|
|[1PE6CVTY6BBH9k7t.htm](pfs-season-3-bestiary/1PE6CVTY6BBH9k7t.htm)|Elite Hadrosaurid (PFS 3-99)|
|[2jiOLCpHekITyQTQ.htm](pfs-season-3-bestiary/2jiOLCpHekITyQTQ.htm)|Temporal Echoes (5-6)|
|[2N2Nj4W9kWn2fU6P.htm](pfs-season-3-bestiary/2N2Nj4W9kWn2fU6P.htm)|Animated Knickknack (1-2)|
|[39ShWwOPdJq2X5o2.htm](pfs-season-3-bestiary/39ShWwOPdJq2X5o2.htm)|Aydrian Thrune (3-4)|
|[3bijukcKD8MfoHA9.htm](pfs-season-3-bestiary/3bijukcKD8MfoHA9.htm)|Forged Bones|
|[3eowuKJaQ99K8Ofp.htm](pfs-season-3-bestiary/3eowuKJaQ99K8Ofp.htm)|Ghast (Heretical)|
|[3r3etd3bUOn58TH3.htm](pfs-season-3-bestiary/3r3etd3bUOn58TH3.htm)|Advanced Eltha Embercall (9-10)/Eltha Embercall (11-12)|
|[3TvWKI01CMwxex51.htm](pfs-season-3-bestiary/3TvWKI01CMwxex51.htm)|Cloud of Reverie|
|[43L2RtbBprItSjPY.htm](pfs-season-3-bestiary/43L2RtbBprItSjPY.htm)|Pontrius Tilasti (3-4)|
|[4f96tNhBeuyTR5jO.htm](pfs-season-3-bestiary/4f96tNhBeuyTR5jO.htm)|Cleansing Torrent|
|[4fhQXAfVGQ0aX8LA.htm](pfs-season-3-bestiary/4fhQXAfVGQ0aX8LA.htm)|Manipulator's Metronome (1-2)|
|[4paPYhdN4HHT3Orz.htm](pfs-season-3-bestiary/4paPYhdN4HHT3Orz.htm)|Flaming Animated Armor (Holy Hand) (1-2)|
|[4S77ftUx1zp7JscD.htm](pfs-season-3-bestiary/4S77ftUx1zp7JscD.htm)|Bloody Skeletal Champion|
|[4XlfENtBfF9GB2fy.htm](pfs-season-3-bestiary/4XlfENtBfF9GB2fy.htm)|Ninth Army Burglar|
|[538nxlPKC93GXzZh.htm](pfs-season-3-bestiary/538nxlPKC93GXzZh.htm)|Mud Pot|
|[55uIx8vScaf8JeTY.htm](pfs-season-3-bestiary/55uIx8vScaf8JeTY.htm)|Junior (3-4)|
|[5vtvRmXRvQAvG39l.htm](pfs-season-3-bestiary/5vtvRmXRvQAvG39l.htm)|Weeping Carved Mourner|
|[5YBQ8KPyEaKnR5eX.htm](pfs-season-3-bestiary/5YBQ8KPyEaKnR5eX.htm)|Bloodletting Scythe Blade|
|[83lyaQloNlS16sJu.htm](pfs-season-3-bestiary/83lyaQloNlS16sJu.htm)|Mature Arabuk|
|[8CEKF7wq9qZzmveH.htm](pfs-season-3-bestiary/8CEKF7wq9qZzmveH.htm)|Sparkles (9-10)|
|[8p1RAJIqtx9JhHqO.htm](pfs-season-3-bestiary/8p1RAJIqtx9JhHqO.htm)|Grieving Carved Mourner|
|[8sCLuGyghlIvMoBo.htm](pfs-season-3-bestiary/8sCLuGyghlIvMoBo.htm)|Languid Purple Worm|
|[8Sea3unOcDH6dWJI.htm](pfs-season-3-bestiary/8Sea3unOcDH6dWJI.htm)|Bergworm Tyrant|
|[8tDDIvcinSurN8BF.htm](pfs-season-3-bestiary/8tDDIvcinSurN8BF.htm)|Marcien Blakros (9-10)|
|[92qhrLmutMVqwQlj.htm](pfs-season-3-bestiary/92qhrLmutMVqwQlj.htm)|Trip Wire|
|[93b4CDDzR9xmHss4.htm](pfs-season-3-bestiary/93b4CDDzR9xmHss4.htm)|Muesello's Summoning Rune|
|[986YKe07u2538RcF.htm](pfs-season-3-bestiary/986YKe07u2538RcF.htm)|Fasiel Ibn Sazadin (3-4)|
|[98iFvMVq4MvrwvK3.htm](pfs-season-3-bestiary/98iFvMVq4MvrwvK3.htm)|Eternal Hall (1-2)|
|[9auIrnhXBCHnO5Ww.htm](pfs-season-3-bestiary/9auIrnhXBCHnO5Ww.htm)|Shrouded Bloodseeker|
|[9FB0dIfsZSk6bdKt.htm](pfs-season-3-bestiary/9FB0dIfsZSk6bdKt.htm)|Animated Cookware|
|[9FgyBdbHxwMYdGAt.htm](pfs-season-3-bestiary/9FgyBdbHxwMYdGAt.htm)|Pup of Tindalos|
|[9RI0biNpRaZBU1de.htm](pfs-season-3-bestiary/9RI0biNpRaZBU1de.htm)|Flaming Animated Armor (Ashgrip) (1-2)|
|[AaCmgrEQ4sPakXca.htm](pfs-season-3-bestiary/AaCmgrEQ4sPakXca.htm)|Caliclotherax (11-12)|
|[AffE7EuKLFijoJzG.htm](pfs-season-3-bestiary/AffE7EuKLFijoJzG.htm)|Ninth Army Operative|
|[aICR8DJ80nD1rOM0.htm](pfs-season-3-bestiary/aICR8DJ80nD1rOM0.htm)|Living Landslide (PFS 3-11)|
|[AIO9Hug8KBesUfxI.htm](pfs-season-3-bestiary/AIO9Hug8KBesUfxI.htm)|Bloody Beetle Exoskeleton|
|[ALH2skuaXvEy6NHW.htm](pfs-season-3-bestiary/ALH2skuaXvEy6NHW.htm)|Revinus's Guards (3-4)|
|[ATBFj4El0uLXDrjb.htm](pfs-season-3-bestiary/ATBFj4El0uLXDrjb.htm)|Cinder Rat (Leadroar)|
|[AuU3BGEz05Q180yI.htm](pfs-season-3-bestiary/AuU3BGEz05Q180yI.htm)|Tascio Raetullus (3-4)|
|[ayfX6eOrfR8695ev.htm](pfs-season-3-bestiary/ayfX6eOrfR8695ev.htm)|Cooking Catastrophe|
|[aZl2962bnq1j6Hpy.htm](pfs-season-3-bestiary/aZl2962bnq1j6Hpy.htm)|Vengeful Ember Fox|
|[B57HMAtx8pj3kt8u.htm](pfs-season-3-bestiary/B57HMAtx8pj3kt8u.htm)|Fipp the Prophet|
|[b5CAFC9BgfOCm7NM.htm](pfs-season-3-bestiary/b5CAFC9BgfOCm7NM.htm)|Muesello's Best Summoning Rune|
|[BcSac9voqpoDjMTy.htm](pfs-season-3-bestiary/BcSac9voqpoDjMTy.htm)|Thundering Wihsaak (Heretical)|
|[bgwjxU9MmKJtcxx5.htm](pfs-season-3-bestiary/bgwjxU9MmKJtcxx5.htm)|Old Drakauthix (PFS 3-15)|
|[bn4ghzDcq3XncBwS.htm](pfs-season-3-bestiary/bn4ghzDcq3XncBwS.htm)|Cleansing Cascade|
|[Bpprw07yfbxqLdBp.htm](pfs-season-3-bestiary/Bpprw07yfbxqLdBp.htm)|Poltergeist (Holy Hand)|
|[BQOgvcGbGxFSQQ0T.htm](pfs-season-3-bestiary/BQOgvcGbGxFSQQ0T.htm)|Leopard (PFS 3-13)|
|[BuLgk7s7lPfn1HUS.htm](pfs-season-3-bestiary/BuLgk7s7lPfn1HUS.htm)|Weakened Cobbleswarm|
|[bW7iJLqP4AOvkMeu.htm](pfs-season-3-bestiary/bW7iJLqP4AOvkMeu.htm)|Veteran Onyx Alliance Enforcer|
|[by3YpQPT2mWc0kFE.htm](pfs-season-3-bestiary/by3YpQPT2mWc0kFE.htm)|Tadrun (3-4)|
|[byhh6wysf9MdM7LM.htm](pfs-season-3-bestiary/byhh6wysf9MdM7LM.htm)|Ninth Army Guard|
|[Ce5z95Z1twvMCsw9.htm](pfs-season-3-bestiary/Ce5z95Z1twvMCsw9.htm)|Arabuk Yearling|
|[ChuQ39tv0plbsfVJ.htm](pfs-season-3-bestiary/ChuQ39tv0plbsfVJ.htm)|Smilglen Daet|
|[CIjBUkWnth0CE30k.htm](pfs-season-3-bestiary/CIjBUkWnth0CE30k.htm)|Cog the Ruffian|
|[cm09iVg6JGpCu3Rh.htm](pfs-season-3-bestiary/cm09iVg6JGpCu3Rh.htm)|Fire Sentry|
|[CmHz3bQDj6FxEecW.htm](pfs-season-3-bestiary/CmHz3bQDj6FxEecW.htm)|Deadly Fetchling Sneak|
|[cYovLvzfbqkTF9uF.htm](pfs-season-3-bestiary/cYovLvzfbqkTF9uF.htm)|Cobbled Bruiser|
|[d0kPtsi2DH3V9WJu.htm](pfs-season-3-bestiary/d0kPtsi2DH3V9WJu.htm)|Shadow (PFS 3-17)|
|[dCljcVK6RAc0Y8Z7.htm](pfs-season-3-bestiary/dCljcVK6RAc0Y8Z7.htm)|Wasp Swarm (PFS 3-13)|
|[DEfeDmBh7lR7c9W8.htm](pfs-season-3-bestiary/DEfeDmBh7lR7c9W8.htm)|Animated Full Plate (PFS 3-99)|
|[df7a1D5Zbv1yZIYl.htm](pfs-season-3-bestiary/df7a1D5Zbv1yZIYl.htm)|Spinning Juice Fountain|
|[dgwmJ8A8sROXAGgf.htm](pfs-season-3-bestiary/dgwmJ8A8sROXAGgf.htm)|Hellbound Jailer (5-6)|
|[dj61OwkpKzv9ctsN.htm](pfs-season-3-bestiary/dj61OwkpKzv9ctsN.htm)|Nuglub (PFS 3-18)|
|[DOG6jdFk7JmBDEBg.htm](pfs-season-3-bestiary/DOG6jdFk7JmBDEBg.htm)|Fast Shambler Troop|
|[DTBa5iNcHztFu7oo.htm](pfs-season-3-bestiary/DTBa5iNcHztFu7oo.htm)|Deep Shadow Guardian|
|[dYcDARPuNmLlGNuG.htm](pfs-season-3-bestiary/dYcDARPuNmLlGNuG.htm)|Flaming Animated Armor (Firecutter) (1-2)|
|[DZwKXXrlzmk5Hqyx.htm](pfs-season-3-bestiary/DZwKXXrlzmk5Hqyx.htm)|Cairn Wight (PFS 3-09)|
|[e9kWyI8plxVuMOe3.htm](pfs-season-3-bestiary/e9kWyI8plxVuMOe3.htm)|Deep Mud|
|[EB00f6ADElWInuix.htm](pfs-season-3-bestiary/EB00f6ADElWInuix.htm)|Shobhad Hunter (7-8)|
|[EiCWpQOwOlGnvWB8.htm](pfs-season-3-bestiary/EiCWpQOwOlGnvWB8.htm)|Explosive Rat|
|[El0QINB4nVKeiI5A.htm](pfs-season-3-bestiary/El0QINB4nVKeiI5A.htm)|Giant Amoeba (PFS 3-98)|
|[eVVAitAkOGCPzhXJ.htm](pfs-season-3-bestiary/eVVAitAkOGCPzhXJ.htm)|Aydrian Thrune (5-6)|
|[ExDYM8AqQVVDKEKM.htm](pfs-season-3-bestiary/ExDYM8AqQVVDKEKM.htm)|Anguished Tombstone Door|
|[Eyf5gaIlI4X2HCos.htm](pfs-season-3-bestiary/Eyf5gaIlI4X2HCos.htm)|Crushing Spirits (5-6)|
|[FfhpuXIxjhWGVnv2.htm](pfs-season-3-bestiary/FfhpuXIxjhWGVnv2.htm)|Ninth Army Soldier|
|[ffzfTw9KfaJopkCx.htm](pfs-season-3-bestiary/ffzfTw9KfaJopkCx.htm)|Peryton (Heretical)|
|[FHsq4Yw458ogI2RQ.htm](pfs-season-3-bestiary/FHsq4Yw458ogI2RQ.htm)|Angry Void Boils|
|[fiArvThMW2QanZGk.htm](pfs-season-3-bestiary/fiArvThMW2QanZGk.htm)|Raseri Kanton Skeleton|
|[fiwIeiExpZ1CQKAj.htm](pfs-season-3-bestiary/fiwIeiExpZ1CQKAj.htm)|Glurorchaes (7-8)|
|[FjRztl9Aro21lXj6.htm](pfs-season-3-bestiary/FjRztl9Aro21lXj6.htm)|Lacerating Trip Wire|
|[fRIyhOmNon1GtQeT.htm](pfs-season-3-bestiary/fRIyhOmNon1GtQeT.htm)|Mutated Brown Mold|
|[FyQji78S25WiZMct.htm](pfs-season-3-bestiary/FyQji78S25WiZMct.htm)|Khisisi (7-8)|
|[g3UYRJQCwnjdvCm9.htm](pfs-season-3-bestiary/g3UYRJQCwnjdvCm9.htm)|Tyrannical Interrogator|
|[g4hfpuGdtTDq7KZw.htm](pfs-season-3-bestiary/g4hfpuGdtTDq7KZw.htm)|Tascio Raetullus (5-6)|
|[gehsgt6uN6sUsmDW.htm](pfs-season-3-bestiary/gehsgt6uN6sUsmDW.htm)|Revinus (3-4)|
|[GKF2zLJ0VLy1IBML.htm](pfs-season-3-bestiary/GKF2zLJ0VLy1IBML.htm)|Aspis Oppressor|
|[gMWqywO74q6evs8U.htm](pfs-season-3-bestiary/gMWqywO74q6evs8U.htm)|Tilting Floor|
|[GODf6SocqJJOktWY.htm](pfs-season-3-bestiary/GODf6SocqJJOktWY.htm)|Unstable Column (1-2)|
|[Gt2NqeZldkUf4vHI.htm](pfs-season-3-bestiary/Gt2NqeZldkUf4vHI.htm)|Aspis Enforcer|
|[gtcISL1zMhQ0mv5g.htm](pfs-season-3-bestiary/gtcISL1zMhQ0mv5g.htm)|Arisept (7-8)|
|[gTJsV3w2gknQJXiH.htm](pfs-season-3-bestiary/gTJsV3w2gknQJXiH.htm)|Smiglen Daet|
|[gwy6v9P8E8hIlP19.htm](pfs-season-3-bestiary/gwy6v9P8E8hIlP19.htm)|Zebub (PFS)|
|[gzMCaAr2Bd2gbhrF.htm](pfs-season-3-bestiary/gzMCaAr2Bd2gbhrF.htm)|Thundering Wihsaak (Arctic)|
|[h2vnwOGmGFcKVGYq.htm](pfs-season-3-bestiary/h2vnwOGmGFcKVGYq.htm)|Iron Ring Pirate|
|[HB1bcZ5gU5tjGoIF.htm](pfs-season-3-bestiary/HB1bcZ5gU5tjGoIF.htm)|Sliding Floor|
|[hEh3j8Fz8lbjuiRF.htm](pfs-season-3-bestiary/hEh3j8Fz8lbjuiRF.htm)|Aspis Recruit|
|[Hez7S4Mhvi816fHi.htm](pfs-season-3-bestiary/Hez7S4Mhvi816fHi.htm)|Unstable Skeletal Champion|
|[hGu2JDWOAHHMLYeH.htm](pfs-season-3-bestiary/hGu2JDWOAHHMLYeH.htm)|Thundering Wihsaak (Rifle Mutation)|
|[hINxfZgkvp21ePQ2.htm](pfs-season-3-bestiary/hINxfZgkvp21ePQ2.htm)|Eternal Hall (3-4)|
|[HIPqiHMAfN2mmAlg.htm](pfs-season-3-bestiary/HIPqiHMAfN2mmAlg.htm)|Cobbled Brutalizer|
|[hjdO3H4Kh3iZbRAD.htm](pfs-season-3-bestiary/hjdO3H4Kh3iZbRAD.htm)|Firespark Trap (3-4)|
|[HOQG0wl3ZtqS2j4v.htm](pfs-season-3-bestiary/HOQG0wl3ZtqS2j4v.htm)|Weak Hell Hound (PFS 3-07)|
|[hqFuN3UINx7ChbqS.htm](pfs-season-3-bestiary/hqFuN3UINx7ChbqS.htm)|Aspis Striker|
|[hRVg3dgSjOrLxgLs.htm](pfs-season-3-bestiary/hRVg3dgSjOrLxgLs.htm)|Severing Trip Wire|
|[HtrdSWlJzDf7dMOO.htm](pfs-season-3-bestiary/HtrdSWlJzDf7dMOO.htm)|Hellbound Robber|
|[HvrXHDTyttxDYvpL.htm](pfs-season-3-bestiary/HvrXHDTyttxDYvpL.htm)|Thomil Bolyrius (5-6)|
|[hYqXZ2glAj0mVfCx.htm](pfs-season-3-bestiary/hYqXZ2glAj0mVfCx.htm)|Temporal Echoes (1-2)|
|[i9cCVnI5OlMe8d2u.htm](pfs-season-3-bestiary/i9cCVnI5OlMe8d2u.htm)|Displaced Robot (1-2)|
|[iev4EOtchd0hp9sh.htm](pfs-season-3-bestiary/iev4EOtchd0hp9sh.htm)|Reckless Akitonian Scientist|
|[IHuTNi8FQt54OpPj.htm](pfs-season-3-bestiary/IHuTNi8FQt54OpPj.htm)|Rampaging Roast (Medium Rare)|
|[iibGqYRM92mGVV8h.htm](pfs-season-3-bestiary/iibGqYRM92mGVV8h.htm)|Cog the Torchbearer|
|[iL7qWBOdmhRwjcVG.htm](pfs-season-3-bestiary/iL7qWBOdmhRwjcVG.htm)|Panicked Bat Swarm|
|[ips7faBeWNAqpByM.htm](pfs-season-3-bestiary/ips7faBeWNAqpByM.htm)|Iron Ring Deckhand|
|[ISBNLUY6Ilu0Jl28.htm](pfs-season-3-bestiary/ISBNLUY6Ilu0Jl28.htm)|Eloko (PFS 3-03)|
|[j78lsOcAHxTNQQRE.htm](pfs-season-3-bestiary/j78lsOcAHxTNQQRE.htm)|Tombstone Door|
|[jaMihaRbFNvKHVUc.htm](pfs-season-3-bestiary/jaMihaRbFNvKHVUc.htm)|Grimstalker (PFS 3-13)|
|[jItk6GNZ7lmT5zVY.htm](pfs-season-3-bestiary/jItk6GNZ7lmT5zVY.htm)|Hellbound Jailer (3-4)|
|[jmPtnZwZq7YugkFe.htm](pfs-season-3-bestiary/jmPtnZwZq7YugkFe.htm)|Precarious Sliding Floor|
|[JmQiEhCnr9WcG6Dl.htm](pfs-season-3-bestiary/JmQiEhCnr9WcG6Dl.htm)|Temporal Echoes (7-8)|
|[jMxFlkAbepA47sHb.htm](pfs-season-3-bestiary/jMxFlkAbepA47sHb.htm)|Scalding Mephit|
|[JteHqFsY5wFDnrcF.htm](pfs-season-3-bestiary/JteHqFsY5wFDnrcF.htm)|Volatile Tidal Controls|
|[JXWHVfaSm3eHIb5v.htm](pfs-season-3-bestiary/JXWHVfaSm3eHIb5v.htm)|Onyx Alliance Commander|
|[JZtLQye4QlfFjrYT.htm](pfs-season-3-bestiary/JZtLQye4QlfFjrYT.htm)|Wasp Swarm (Heretical)|
|[kBYuJs4je7XBPisk.htm](pfs-season-3-bestiary/kBYuJs4je7XBPisk.htm)|Foot Pincer|
|[KSKDACF4qh3HJTDo.htm](pfs-season-3-bestiary/KSKDACF4qh3HJTDo.htm)|Vaggas the Fanatic|
|[kTsUEbM3JkhT5VZM.htm](pfs-season-3-bestiary/kTsUEbM3JkhT5VZM.htm)|Sandswept Statue|
|[l1EWODWiDlDLQx0L.htm](pfs-season-3-bestiary/l1EWODWiDlDLQx0L.htm)|Kobold Scout (PFS 3-99)|
|[L1gzOT7o2xxJoqUD.htm](pfs-season-3-bestiary/L1gzOT7o2xxJoqUD.htm)|Timewarped Amalgam|
|[L2BPCPQ4fRMviF2d.htm](pfs-season-3-bestiary/L2BPCPQ4fRMviF2d.htm)|Shadow Double Runes (3-4)|
|[LG2pZMbLXZKtcWZh.htm](pfs-season-3-bestiary/LG2pZMbLXZKtcWZh.htm)|Arisept (9-10)|
|[LHEI3E4THLsz0rr0.htm](pfs-season-3-bestiary/LHEI3E4THLsz0rr0.htm)|Enduring Zombie Hulk|
|[LiKplPXYeqLVJ462.htm](pfs-season-3-bestiary/LiKplPXYeqLVJ462.htm)|Insolent's Retort|
|[Lmo4SH5wpywZL1YS.htm](pfs-season-3-bestiary/Lmo4SH5wpywZL1YS.htm)|Displaced Robot (5-6)|
|[LqEHXPTgyVUltvS2.htm](pfs-season-3-bestiary/LqEHXPTgyVUltvS2.htm)|Skulk (PFS 3-13)|
|[m1bVKRXsSXnBNka5.htm](pfs-season-3-bestiary/m1bVKRXsSXnBNka5.htm)|Elite Hieracosphinx (PFS 3-14)|
|[MK2YstG6JpFwI9sB.htm](pfs-season-3-bestiary/MK2YstG6JpFwI9sB.htm)|Duergar Crusher|
|[mQmcKIgTUn3xMClx.htm](pfs-season-3-bestiary/mQmcKIgTUn3xMClx.htm)|Revinus's Guards (1-2)|
|[MQMfOX5h2OgJrhm6.htm](pfs-season-3-bestiary/MQMfOX5h2OgJrhm6.htm)|Uncle Jeb (3-4)|
|[mSkooYG0IgmBAktL.htm](pfs-season-3-bestiary/mSkooYG0IgmBAktL.htm)|Flensing Trap (5-6)|
|[mYpAGYbt6cGiTU8p.htm](pfs-season-3-bestiary/mYpAGYbt6cGiTU8p.htm)|Junior (1-2)|
|[mZ2bmxg4JcVh1p2k.htm](pfs-season-3-bestiary/mZ2bmxg4JcVh1p2k.htm)|Rampaging Roast (Well Done)|
|[n0Gq9VPAOReN4IJB.htm](pfs-season-3-bestiary/n0Gq9VPAOReN4IJB.htm)|Overloading Arch (1-2)|
|[nABHOLMNGoOFzsfE.htm](pfs-season-3-bestiary/nABHOLMNGoOFzsfE.htm)|Junk Launcher|
|[njJCv6ItwIzwO3Xj.htm](pfs-season-3-bestiary/njJCv6ItwIzwO3Xj.htm)|Ninth Army Fence|
|[nPuiF2GEyJ2WMG4v.htm](pfs-season-3-bestiary/nPuiF2GEyJ2WMG4v.htm)|Grimple (PFS 3-18)|
|[nS4auasfEXEU0rtx.htm](pfs-season-3-bestiary/nS4auasfEXEU0rtx.htm)|Onyx Alliance Scout|
|[oJiVHS6pZlJumvj5.htm](pfs-season-3-bestiary/oJiVHS6pZlJumvj5.htm)|Shadow Spears (7-8)|
|[Ol8JpWfZKdJ28K5L.htm](pfs-season-3-bestiary/Ol8JpWfZKdJ28K5L.htm)|Sorrowful Tombstone Door|
|[olP1l0gi9qYFZtt2.htm](pfs-season-3-bestiary/olP1l0gi9qYFZtt2.htm)|Lightweight Animated Armor|
|[OOHNMq2uOfnp8QCg.htm](pfs-season-3-bestiary/OOHNMq2uOfnp8QCg.htm)|Fasiel Ibn Sazadin (1-2)|
|[oOl4UOYKpGeS3Ep7.htm](pfs-season-3-bestiary/oOl4UOYKpGeS3Ep7.htm)|Onyx Alliance Enforcer|
|[oqxqAesRlacxYKiY.htm](pfs-season-3-bestiary/oqxqAesRlacxYKiY.htm)|Temporal Echoes (3-4)|
|[oTftuPSL5iB6fhWU.htm](pfs-season-3-bestiary/oTftuPSL5iB6fhWU.htm)|Poisoned Dart Trap (1-2)|
|[ovNZDN5d2yYY8tqP.htm](pfs-season-3-bestiary/ovNZDN5d2yYY8tqP.htm)|Falling Tree|
|[OVsp8tvdybZbbszm.htm](pfs-season-3-bestiary/OVsp8tvdybZbbszm.htm)|Onyx Alliance Officer|
|[p0RvcmcEe8zfoMkT.htm](pfs-season-3-bestiary/p0RvcmcEe8zfoMkT.htm)|Hellbound Rogue|
|[P0Y86CC9ULe0D4x7.htm](pfs-season-3-bestiary/P0Y86CC9ULe0D4x7.htm)|Debased Relic|
|[P51YAmzcq1jONhJR.htm](pfs-season-3-bestiary/P51YAmzcq1jONhJR.htm)|Wailing Ghost|
|[pBRkhSlkyNznhzP9.htm](pfs-season-3-bestiary/pBRkhSlkyNznhzP9.htm)|Poisoned Dart Trap (3-4)|
|[PcKhzFKtzNvtBfdI.htm](pfs-season-3-bestiary/PcKhzFKtzNvtBfdI.htm)|Manipulator's Metronome (3-4)|
|[pcSXAn8KbvPDWCWU.htm](pfs-season-3-bestiary/pcSXAn8KbvPDWCWU.htm)|Fast-Spinning Juice Fountain|
|[phgXSggQiGRNn4Yr.htm](pfs-season-3-bestiary/phgXSggQiGRNn4Yr.htm)|Ninth Army Bodyguard|
|[PhnmHglnmoiIb3Al.htm](pfs-season-3-bestiary/PhnmHglnmoiIb3Al.htm)|Flaming Animated Armor (Leadroar) (1-2)|
|[plYMMhlfO6OVrtEo.htm](pfs-season-3-bestiary/plYMMhlfO6OVrtEo.htm)|Eloise's Ghost (1-2)|
|[pnZ9aFB0mdYdU7Zj.htm](pfs-season-3-bestiary/pnZ9aFB0mdYdU7Zj.htm)|Shadow Double Runes (1-2)|
|[POFH4BcrEF9wxwp2.htm](pfs-season-3-bestiary/POFH4BcrEF9wxwp2.htm)|Young Arabuk|
|[PqawYWEK0iqvcBZj.htm](pfs-season-3-bestiary/PqawYWEK0iqvcBZj.htm)|Marcien Blakros (7-8) (Shadow Double)|
|[psBBFRgVi0NwKSWX.htm](pfs-season-3-bestiary/psBBFRgVi0NwKSWX.htm)|Muesello's Greater Summoning Rune|
|[psFZ9UdpJR4rRpEy.htm](pfs-season-3-bestiary/psFZ9UdpJR4rRpEy.htm)|Kobold Warrior (PFS 3-99)|
|[PXXLxoPdGwtiVvtS.htm](pfs-season-3-bestiary/PXXLxoPdGwtiVvtS.htm)|Eternal Hall (5-6)|
|[pYR3Ovkd9eKQQ5qE.htm](pfs-season-3-bestiary/pYR3Ovkd9eKQQ5qE.htm)|Eltha Embercall (11-12, 28+ CP)|
|[Q2a9EA1zQB8acuiF.htm](pfs-season-3-bestiary/Q2a9EA1zQB8acuiF.htm)|Shobhad Hunter (5-6)|
|[q6J7DcU3am7wKIRD.htm](pfs-season-3-bestiary/q6J7DcU3am7wKIRD.htm)|Aspis Warden|
|[qd6yO0nBdmoz8Fl2.htm](pfs-season-3-bestiary/qd6yO0nBdmoz8Fl2.htm)|Cole Farsen, Advisor|
|[qDpKFbhWgSWEEA00.htm](pfs-season-3-bestiary/qDpKFbhWgSWEEA00.htm)|Marcien Blakros (7-8)|
|[qFzhrDwmnBhk4FyL.htm](pfs-season-3-bestiary/qFzhrDwmnBhk4FyL.htm)|Skeleton Warrior|
|[QgyodFPH6wehKmmM.htm](pfs-season-3-bestiary/QgyodFPH6wehKmmM.htm)|Carved Mourner|
|[QHDVWljImF5sRhX2.htm](pfs-season-3-bestiary/QHDVWljImF5sRhX2.htm)|Drakauthix (PFS 3-15)|
|[qnu2pwoPe5Hh73vB.htm](pfs-season-3-bestiary/qnu2pwoPe5Hh73vB.htm)|Uncle Jeb (1-2)|
|[qPUP8Qtp7HXmXiBi.htm](pfs-season-3-bestiary/qPUP8Qtp7HXmXiBi.htm)|Displaced Robot (3-4)|
|[qRX3Gax6xC3yIqyt.htm](pfs-season-3-bestiary/qRX3Gax6xC3yIqyt.htm)|Ninth Army War Mage|
|[qulQzbgDOLz969yg.htm](pfs-season-3-bestiary/qulQzbgDOLz969yg.htm)|Void Boils|
|[QvrnfL8u3azh9lBA.htm](pfs-season-3-bestiary/QvrnfL8u3azh9lBA.htm)|Fetchling Sneak|
|[R7d4HsytkpganuFa.htm](pfs-season-3-bestiary/R7d4HsytkpganuFa.htm)|Rock Launcher|
|[RCCGR5wlfGNPkkBy.htm](pfs-season-3-bestiary/RCCGR5wlfGNPkkBy.htm)|Enraged Shrouded Bloodseeker|
|[rhohGomgIOjLv2by.htm](pfs-season-3-bestiary/rhohGomgIOjLv2by.htm)|Firespark Trap (1-2)|
|[riCZJcenSSIQckJi.htm](pfs-season-3-bestiary/riCZJcenSSIQckJi.htm)|Faulty Tidal Controls|
|[RLBOKSaMrOCA4fdZ.htm](pfs-season-3-bestiary/RLBOKSaMrOCA4fdZ.htm)|Ninth Army Ruffian|
|[RmgJ2iZ20AKxZZjQ.htm](pfs-season-3-bestiary/RmgJ2iZ20AKxZZjQ.htm)|Animated Knickknack (3-4)|
|[RTHM0lrmzvUskBMy.htm](pfs-season-3-bestiary/RTHM0lrmzvUskBMy.htm)|Arabuk|
|[rwRPi6GwXhF34W55.htm](pfs-season-3-bestiary/rwRPi6GwXhF34W55.htm)|Branwaen|
|[RxB7Hj5E3kGcE9Fi.htm](pfs-season-3-bestiary/RxB7Hj5E3kGcE9Fi.htm)|Ruthless Fetchling Sneak|
|[rXNQ9zxsf9rN7zUQ.htm](pfs-season-3-bestiary/rXNQ9zxsf9rN7zUQ.htm)|Enhanced Brown Mold|
|[S6xhwxwQDP3tEXiS.htm](pfs-season-3-bestiary/S6xhwxwQDP3tEXiS.htm)|Flensing Trap (7-8)|
|[S9v8OCktGEL0k6sC.htm](pfs-season-3-bestiary/S9v8OCktGEL0k6sC.htm)|Cinder Rat (Ashgrip)|
|[SBMZYJ1wYptIuWRA.htm](pfs-season-3-bestiary/SBMZYJ1wYptIuWRA.htm)|Unstable Column (3-4)|
|[sHl25TefznyV8xC8.htm](pfs-season-3-bestiary/sHl25TefznyV8xC8.htm)|Unstable Skeleton Guard|
|[SJoWqX2P5LhJsbYA.htm](pfs-season-3-bestiary/SJoWqX2P5LhJsbYA.htm)|Thundering Wihsaak|
|[SkV5zYJgu7hdpZE0.htm](pfs-season-3-bestiary/SkV5zYJgu7hdpZE0.htm)|Sparkles (11-12)|
|[SPt3t3ROjGqkwpCU.htm](pfs-season-3-bestiary/SPt3t3ROjGqkwpCU.htm)|Insolent's Castigation|
|[SyFPYtqvE9jAZ6LR.htm](pfs-season-3-bestiary/SyFPYtqvE9jAZ6LR.htm)|Aspis Defender|
|[SzcyJHMmYgLwtXlQ.htm](pfs-season-3-bestiary/SzcyJHMmYgLwtXlQ.htm)|Blessed Crossbow Trap|
|[T8rc4C98gf87869G.htm](pfs-season-3-bestiary/T8rc4C98gf87869G.htm)|Strong Fire Sentry|
|[TIR0DiANNlcQGwMS.htm](pfs-season-3-bestiary/TIR0DiANNlcQGwMS.htm)|Overloading Arch (3-4)|
|[TmMK56lcxYBbhXqt.htm](pfs-season-3-bestiary/TmMK56lcxYBbhXqt.htm)|Pernicious Powder Keg|
|[TrZ0ixbWfscr0k93.htm](pfs-season-3-bestiary/TrZ0ixbWfscr0k93.htm)|Raging Debris Storm|
|[Tt9DtLxj1N2TBT9w.htm](pfs-season-3-bestiary/Tt9DtLxj1N2TBT9w.htm)|Kobold Dragon Mage (PFS 3-99)|
|[tU0JjT2Hxbi8bPJW.htm](pfs-season-3-bestiary/tU0JjT2Hxbi8bPJW.htm)|Eternal Hall (7-8)|
|[tVsrWl9kfVIGG5OW.htm](pfs-season-3-bestiary/tVsrWl9kfVIGG5OW.htm)|Thick Pelagastr Tail|
|[U41mRr07sJywSFYZ.htm](pfs-season-3-bestiary/U41mRr07sJywSFYZ.htm)|Werebat (PFS 3-13)|
|[UBV2LL1j8vP9cW43.htm](pfs-season-3-bestiary/UBV2LL1j8vP9cW43.htm)|Cooking Disaster|
|[uD1GCmruAUJ3zfdj.htm](pfs-season-3-bestiary/uD1GCmruAUJ3zfdj.htm)|Natron Eruption|
|[uip49rfJG1KuUlyn.htm](pfs-season-3-bestiary/uip49rfJG1KuUlyn.htm)|Rockslide Trap|
|[uJe5OEPDuvIG6eg6.htm](pfs-season-3-bestiary/uJe5OEPDuvIG6eg6.htm)|Ghast (Arctic)|
|[umiKZDfhNO3mXvEb.htm](pfs-season-3-bestiary/umiKZDfhNO3mXvEb.htm)|Alchemy-Gorged Giant Leech|
|[uOgiYc5VGhSxkLFa.htm](pfs-season-3-bestiary/uOgiYc5VGhSxkLFa.htm)|Pontrius Tilasti (1-2)|
|[Upa2rtDx5daLyx2z.htm](pfs-season-3-bestiary/Upa2rtDx5daLyx2z.htm)|Eloise's Last Gasp (3-4)|
|[uzFHHePtuxBv6K9u.htm](pfs-season-3-bestiary/uzFHHePtuxBv6K9u.htm)|Revinus (1-2)|
|[vkrKdprKwRpuXHS3.htm](pfs-season-3-bestiary/vkrKdprKwRpuXHS3.htm)|Caustic Mud Pot|
|[VMeVz1AsEkq6QWDE.htm](pfs-season-3-bestiary/VMeVz1AsEkq6QWDE.htm)|Muesello's Elite Summoning Rune|
|[vQjnfmvUzZwaUoBd.htm](pfs-season-3-bestiary/vQjnfmvUzZwaUoBd.htm)|Pelagastr Tail|
|[vuxdXTrDMv5YQ1Fx.htm](pfs-season-3-bestiary/vuxdXTrDMv5YQ1Fx.htm)|Displaced Robot (7-8)|
|[vwV33OVSDH6fsdPI.htm](pfs-season-3-bestiary/vwV33OVSDH6fsdPI.htm)|Mummy Guardian (PFS 3-09)|
|[vwWjjQwPR9XItpjY.htm](pfs-season-3-bestiary/vwWjjQwPR9XItpjY.htm)|Giant Rat (PFS 3-98)|
|[w295JTcaHaTQuplt.htm](pfs-season-3-bestiary/w295JTcaHaTQuplt.htm)|Malfunctioning Tidal Controls|
|[WaTrewLfMf8WH0O9.htm](pfs-season-3-bestiary/WaTrewLfMf8WH0O9.htm)|Ninth Army Mage|
|[WB4DMTHCIEBOiitp.htm](pfs-season-3-bestiary/WB4DMTHCIEBOiitp.htm)|Shadow Guardian|
|[WGi99nXZYSv7U92M.htm](pfs-season-3-bestiary/WGi99nXZYSv7U92M.htm)|Tadrun (1-2)|
|[wLM38nmdIITxkr4k.htm](pfs-season-3-bestiary/wLM38nmdIITxkr4k.htm)|Elite Green Hag (PFS 3-99)|
|[WpdOwEpoWwxrBUpF.htm](pfs-season-3-bestiary/WpdOwEpoWwxrBUpF.htm)|Peryton (Rifle Mutation)|
|[wqJDAf89wSAdG0qy.htm](pfs-season-3-bestiary/wqJDAf89wSAdG0qy.htm)|Peryton (Arctic)|
|[WQUsCm35NNZUlJYy.htm](pfs-season-3-bestiary/WQUsCm35NNZUlJYy.htm)|Faulty Rockslide Trap|
|[WRVcJ6z2q2oDxTO7.htm](pfs-season-3-bestiary/WRVcJ6z2q2oDxTO7.htm)|Crushing Spirits (7-8)|
|[wun5VzDIMSZCVLuI.htm](pfs-season-3-bestiary/wun5VzDIMSZCVLuI.htm)|Waking Nightmare|
|[WxN19w3aO5MpVY6Z.htm](pfs-season-3-bestiary/WxN19w3aO5MpVY6Z.htm)|Cole Farsen, Gang Leader|
|[x6uXLDuFimMuvz3w.htm](pfs-season-3-bestiary/x6uXLDuFimMuvz3w.htm)|Half-Formed Abrikandilu|
|[XhxucXUyWWLyty9H.htm](pfs-season-3-bestiary/XhxucXUyWWLyty9H.htm)|Overloading Arch (7-8)|
|[xiBVzPUwh1QI2lIs.htm](pfs-season-3-bestiary/xiBVzPUwh1QI2lIs.htm)|Eltha Embercall (9-10)|
|[xkXMKw8GhdI5FNar.htm](pfs-season-3-bestiary/xkXMKw8GhdI5FNar.htm)|Monastic Bodyguard (3-4)|
|[Xpf9HQDxoafOwfoB.htm](pfs-season-3-bestiary/Xpf9HQDxoafOwfoB.htm)|Thomil Bolyrius (3-4)|
|[xShj7KlFATzm397y.htm](pfs-season-3-bestiary/xShj7KlFATzm397y.htm)|Wasp Swarm (Arctic)|
|[xVmDwoBxjKyCihYz.htm](pfs-season-3-bestiary/xVmDwoBxjKyCihYz.htm)|Marcien Blakros (9-10) (Shadow Double)|
|[XxbdbTlnY0P3RU3h.htm](pfs-season-3-bestiary/XxbdbTlnY0P3RU3h.htm)|Aspis Usurper|
|[XYBA4EoyegNciY2P.htm](pfs-season-3-bestiary/XYBA4EoyegNciY2P.htm)|Elite Hell Hound (PFS 3-07)|
|[xYByANF8t6zhxt4p.htm](pfs-season-3-bestiary/xYByANF8t6zhxt4p.htm)|Veteran Onyx Alliance Scout|
|[XyrToL9Wyl4MoAf1.htm](pfs-season-3-bestiary/XyrToL9Wyl4MoAf1.htm)|Caliclotherax (9-10)|
|[Y5ZcwtbKxA1BImQq.htm](pfs-season-3-bestiary/Y5ZcwtbKxA1BImQq.htm)|Umbral Cu Sith|
|[yb3tVvtIkblQoEoH.htm](pfs-season-3-bestiary/yb3tVvtIkblQoEoH.htm)|Unstable Column (5-6)|
|[yfh8wibKauDIh6Dj.htm](pfs-season-3-bestiary/yfh8wibKauDIh6Dj.htm)|Sulfuric Slime|
|[yhQALO3emgx8Z5N9.htm](pfs-season-3-bestiary/yhQALO3emgx8Z5N9.htm)|Waking Terror|
|[YMT0Hl0LO9QKrb3X.htm](pfs-season-3-bestiary/YMT0Hl0LO9QKrb3X.htm)|Aspis Agent|
|[YsF8TteK6XsThooV.htm](pfs-season-3-bestiary/YsF8TteK6XsThooV.htm)|Shanrigol Mound|
|[yw5hz4q8vn7S7aUF.htm](pfs-season-3-bestiary/yw5hz4q8vn7S7aUF.htm)|Umbral Drake|
|[YwkPGpBPttW2G7Xn.htm](pfs-season-3-bestiary/YwkPGpBPttW2G7Xn.htm)|Bloody Skeleton Guard|
|[Z5zTWbeZ0N1XedgS.htm](pfs-season-3-bestiary/Z5zTWbeZ0N1XedgS.htm)|Strong Electric Latch Rune|
|[zC29Os27IYf65qNp.htm](pfs-season-3-bestiary/zC29Os27IYf65qNp.htm)|Shadow Spears (9-10)|
|[zDNR0eYpfEvGLOGo.htm](pfs-season-3-bestiary/zDNR0eYpfEvGLOGo.htm)|Debris Storm|
|[zhtkM2GPvDNw3bY1.htm](pfs-season-3-bestiary/zhtkM2GPvDNw3bY1.htm)|Eloise's Ghost (3-4)|
|[zIMgoLwuJVMYxqeF.htm](pfs-season-3-bestiary/zIMgoLwuJVMYxqeF.htm)|Languid Isqulug|
|[zMVQXCDhP7yeuQg3.htm](pfs-season-3-bestiary/zMVQXCDhP7yeuQg3.htm)|Aspis Bulwark|
|[ZSCBm3JFU6tFtqvL.htm](pfs-season-3-bestiary/ZSCBm3JFU6tFtqvL.htm)|Eloise's Last Gasp (1-2)|
|[zUbfyAyNN4aYLQKA.htm](pfs-season-3-bestiary/zUbfyAyNN4aYLQKA.htm)|Khisisi (5-6)|
|[zzbBBSmjvCzJ6lyt.htm](pfs-season-3-bestiary/zzbBBSmjvCzJ6lyt.htm)|Monastic Bodyguard (5-6)|
|[ZzH4EqJtWIhaNWSv.htm](pfs-season-3-bestiary/ZzH4EqJtWIhaNWSv.htm)|Animated Blade (PFS 3-99)|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[2wV0Yv8uQpmRs2E5.htm](pfs-season-3-bestiary/2wV0Yv8uQpmRs2E5.htm)|Smoldering Nightmare|Destrier incandescent|libre|
|[6aGKyGMSHD4fqReA.htm](pfs-season-3-bestiary/6aGKyGMSHD4fqReA.htm)|Weretiger (PFS 3-13)|Tigre-garou|libre|
|[eiWWhTeR8QTLKptH.htm](pfs-season-3-bestiary/eiWWhTeR8QTLKptH.htm)|Lesser Animate Dream|Rêve animé inférieur|libre|
|[FmZ8bz1ItIzxuu1D.htm](pfs-season-3-bestiary/FmZ8bz1ItIzxuu1D.htm)|Iron Ring Bosun|Bosco du cercle de fer|libre|
|[gR4cLkgcor3Lp35F.htm](pfs-season-3-bestiary/gR4cLkgcor3Lp35F.htm)|Bugbear Tormentor (PFS 3-13)|Gobelours tourmenteur (PFS 3-13)|libre|
|[h65eZJq6Iie69zLD.htm](pfs-season-3-bestiary/h65eZJq6Iie69zLD.htm)|Bloody Barber Ambusher|Piègeur des Barbiers sanglants|libre|
|[IP4wercLl66JDCaG.htm](pfs-season-3-bestiary/IP4wercLl66JDCaG.htm)|Bloody Barber Leader|Chef des Barbiers sanglants|libre|
|[RZ8sWKkogng0IU8M.htm](pfs-season-3-bestiary/RZ8sWKkogng0IU8M.htm)|Vaggas the Bosun|Vaggas le bosco|libre|
|[sfStaEmGGvXWftsn.htm](pfs-season-3-bestiary/sfStaEmGGvXWftsn.htm)|Horrid Nightmare|Destrier noir horrifiant|libre|
