<h1>Actions</h1>
<p><em>Vous affectez le monde qui vous entoure essentiellement en utilisant des actions, lesquelles produisent des effets. C'est au cours du mode rencontre que les actions sont le plus étroitement mesurées et limitées mais, même lorsqu'il n'est pas important de les suivre de près, elles restent le moyen d'interagir avec le monde du jeu.</em></p>

<p>Vous devrez suivre attentivement vos actions au cours d'une rencontre. Au début de chacun des tours d'une rencontre, vous récupérez 3 actions et 1 réaction à dépenser pendant ce tour. Vous pouvez dépenser vos actions de différentes manières. Il existe quatre types d'actions : les actions simples, les activités, les réactions et les actions gratuites.</p>
<p>Les <strong>actions uniques</strong> peuvent être terminées en un temps très court. Elles sont autonomes et leurs effets sont générés dans le cadre de cette action unique.</p>
<p>Les <strong>activités</strong> prennent habituellement plus de temps et nécessitent d'utiliser plusieurs actions qui doivent être dépensées successivement. Marcher rapidement est une action unique, mais Charge soudaine est une activité au cours de laquelle vous utilisez à la fois l'action Marcher rapidement et l'action Frapper pour générer son effet.</p>
<p>Les <strong>réactions</strong> possèdent des déclencheurs qui doivent être remplies pour que vous puissiez les utiliser. Vous pouvez utiliser une réaction à chaque fois que son déclencheur est rempli, que ce soit ou non pendant votre tour. En dehors des rencontres, votre utilisation des réactions est plus souple et laissée à l'appréciation du MJ. Les réactions sont habituellement déclenchées par les autres créatures ou par des évènements qui ne sont pas sous votre contrôle.</p>
<p>Les <strong>actions gratuites</strong> ne vous coûtent aucune des actions de votre tour, ni votre réaction. Une action libre sans déclencheur suit les mêmes règles qu'une action simple (à l'exception du coût de l'action). Elle doit être utilisée durant votre tour et ne peut pas être utilisée au cours d'une autre action. Une action gratuite possédant un déclencheur suit les mêmes règles qu'une réaction (sauf le coût de la réaction). Elle peut être utilisée à chaque fois que son déclencheur est rempli.</p>

<section>
<h2>ICÔNES D'ACTIONS CLÉS</h2>
<p>Ces icônes apparaissent dans les blocs de statistiques comme des raccourcis pour désigner chaque type d'action. En tant que joueur, vous verrez habituellement l'icône dans l'en-tête d'une action (tels qu'une action basique, une action de compétence, des dons ou des sorts). Au sein d'un bloc de statistiques d'une créature ou d'un don qui vous accorde une nouvelle action en plus d'autres avantages, l'icône apparaîtra dans le texte.</p>
<p><span class="action-glyph">1</span> <strong>Action unique</strong></p>
<p><span class="action-glyph">2</span> <strong>Activité de deux actions</strong></p>
<p><span class="action-glyph">3</span> <strong>Activité de trois actions</strong></p>
<p><span class="action-glyph">R</span> <strong>Réaction</strong></p>
<p><span class="action-glyph">F</span> <strong>Action gratuite</strong></p>
</section>

<h2>Activités</h2>
<p>Une activité implique typiquement d'utiliser plusieurs actions pour créer un effet supérieur à celui que vous pouvez produire avec une unique action ou de combiner plusieurs actions uniques pour produire un effet qui soit différent de la simple somme de ces deux actions. Dans certains cas, notamment lors de l'incantation d'un sort, une activité peut consister en une action unique, une seule réaction ou même une seule action gratuite.</p>
<p>Une activité peut vous amener à utiliser des actions spécifiques en son sein. Il n'est pas nécessaire d'effectuer des actions supplémentaires pour les réaliser : elles sont déjà prises en compte dans les actions requises par l'activité.</p>
<p>Vous devez dépenser toutes les actions d'une activité en une seule fois pour bénéficier de ses effets. Lors d'une rencontre, cela signifie que vous devez la terminer pendant votre tour. Si une activité est interrompue ou perturbée lors d'une rencontre, vous perdez toutes les actions que vous y avez consacrées.</p>

<h3>Exploration et Activités d'intermède</h3>
<p>En dehors des rencontres, les activités peuvent vous prendre plusieurs minutes, des heures, voire même des jours. Ces activités possèdent habituellement les traits exploration ou intermède pour indiquer qu'elles doivent être utilisées durant ces modes de jeu. Vous pouvez souvent faire d'autres choses de manière ponctuelle tandis que vous menez ces activités, pourvu qu'il ne s'agisse pas elles-mêmes d'activités significatives. Par exemple, si vous Réparez un objet, vous pourriez vous dégourdir les jambes ou avoir une brève discussion, mais vous ne pourriez pas Déchiffrer un texte en même temps.</p>
<p>Si une activité en dehors d'une rencontre est interrompue ou perturbée comme indiqué dans Actions interrompues, vous perdez habituellement le temps que vous y avez investi, mais pas de temps supplémentaire.</p>

<h2>Actions avec déclencheurs</h2>
<p>Vous pouvez utiliser des actions gratuites qui possèdent des déclencheurs et des réactions seulement en réponse à certains évènements. Chacune de ces réactions et actions gratuites indique un Déclencheur qui doit se produire pour que vous puissiez les accomplir. Lorsque le déclencheur est rempli - et seulement lorsque c'est le cas - vous pouvez utiliser la réaction ou l'action gratuite, bien que vous ne soyez pas obligé d'utiliser l'action si vous ne le souhaitez pas.</p>
<p>Il n'y a que quelques réactions basiques et quelques actions gratuites que tous les personnages peuvent utiliser. Vous avez plus de chances d'obtenir des actions avec des déclencheurs par votre classe, vos dons ou par vos objets magiques.</p>

<h3>Limitations des déclencheurs</h3>
<p>Les déclencheurs listés dans les blocs de statistiques des réactions et de certaines actions gratuites limitent le moment où vous pouvez utiliser ces actions. Vous ne pouvez utiliser qu'une seule action en réponse à un déclencheur donné.</p>
<p>Par exemple, si vous avez une réaction et une action gratuite qui possèdent toutes deux le déclencheur “votre tour commence”, vous pourriez utiliser l'une ou l'autre au début de votre tour, mais pas les deux. Si deux déclencheurs sont semblables, mais pas identiques, le MJ détermine si vous pouvez utiliser une action en réponse à chacun ou si les déclencheurs sont effectivement la même chose. Habituellement, cette décision sera basée sur ce qui se produit au cours de la narration.</p>
<p>Cette limitation d'une action par déclencheur vaut pour une créature. Plus d'une créature peuvent utiliser une réaction ou une action gratuite en réponse à un déclencheur donné. Si plusieurs actions devraient se produire en même temps et si l'ordre dans lequel elles se produisent n'est pas clair, le MJ détermine l'ordre en fonction de la narration.</p>

<h2>Autres actions</h2>
<p>Parfois, vous devez tenter quelque chose qui n'est pas déjà couvert par des actions définies dans le jeu. Dans ce cas, les règles vous indiquent le nombre d'actions que vous devez dépenser, ainsi que les caractéristiques éventuelles de votre action. Par exemple, un sort qui vous permet de changer de cibles pourrait dire que vous pouvez le faire “en dépensant une action unique, qui possède le trait concentration”. Les maîtres du jeu peuvent aussi utiliser cette approche lorsqu'un personnage essaie de faire quelque chose qui n'est pas couvert par les règles.</p>

<h2>Obtenir et perdre des actions</h2>
<p>Les effets peuvent changer le nombre d'actions que vous pouvez utiliser lors de votre tour ou vous empêcher d'utiliser des actions. L'état Ralenti, par exemple, vous fait perdre des actions, alors que l'état Accéléré vous en fait gagner. Ces états sont détaillés plus loin. Lorsque vous perdez un nombre d'actions — soit à cause d'un état, soit d'une autre manière, vous choisissez laquelle vous perdez s'il y a des différences entre elles. Par exemple, le sort <em>Rapidité</em> vous rend Accéléré, mais limite la manière dont vous pouvez utiliser votre action supplémentaire. Si vous avez perdu une action alors que le sort <em>Rapidité</em> est actif, vous pourriez préférer supprimer l'action découlant de <em>Rapidité</em> car elle est plus limitée que vos actions normales.</p>
<p>Certains effets sont même plus restrictifs. Certaines capacités, au lieu ou en plus de changer le nombre d'actions que vous pouvez utiliser, indiquent spécifiquement que vous ne pouvez pas utiliser de réactions. La forme la plus restrictive de réduction des actions, c'est lorsqu'un effet stipule que vous ne pouvez pas agir : cela signifie que vous ne pouvez pas utiliser la moindre action, pas même parler. Lorsque vous ne pouvez pas agir, vous récupérez vos actions à moins qu'un autre effet (comme l'état Étourdi) ne vous en empêche.</p>

<h2>Interrompre des actions</h2>
<p>Des capacités et des états variés, comme Frappe réactive, peuvent interrompre une action. Lorsqu'une action est interrompue, vous utilisez toujours les actions ou réactions que vous avez engagées et vous dépensez toujours les coûts, mais les effets de l'action ne se produisent pas. Dans le cas d'une activité, vous perdez habituellement la totalité des actions dépensées pour l'activité jusqu'à la fin de ce tour. Par exemple, si vous avez commencé à lancer un sort nécessitant 3 actions et que la première action est interrompue, vous perdez la totalité des 3 actions que vous avez consacré à cette activité.</p>
<p>Le MJ décide quels effets une interruption provoque au-delà de simplement annuler les effets qui se seraient produits par l'action interrompue. Par exemple, un Bond interrompu  à mi-distance ne va pas vous ramener en arrière, à l'endroit d'où vous avez sauté et la remise d'un objet interrompue peut entraîner la chute de l'objet au sol au lieu de le laisser dans la main de la créature qui essayait de le donner.</p>

<section>
<h2>RÈGLES D'ACTION APPROFONDIES</h2>
<p>Ces règles clarifient certaines spécificités de l'utilisation des actions.</p>
<h3>Actions simultanées</h3>
<p>Vous ne pouvez utiliser qu'une seule action, activité ou action gratuite qui n'a pas de Déclencheur à la fois. Vous devez en terminer une avant d'en commencer une autre. Par exemple, l'activité Charge soudaine stipule que vous devez Marcher rapidement deux fois puis Frapper, vous ne pouvez donc pas utiliser une action Interagir pour ouvrir une porte au milieu du déplacement et vous ne pouvez pas non plus effectuer une partie du déplacement, faire votre attaque, puis terminer le déplacement.</p>
<p>Les actions gratuites possédant des déclencheurs et des réactions fonctionnent différemment. Vous pouvez les utiliser à chaque fois que le déclencheur se produit, même si celui-ci intervient au milieu d'une autre action.</p>
<h3>Actions subordonnées</h3>
<p>Une action pourrait vous permettre d'utiliser une action plus simple - généralement l'une des actions basiques - dans des circonstances ou avec des effets différents. Cette action subordonnée possède toujours ses traits et ses effets normaux, mais elle est modifiée de toutes les façons indiquées dans l'action plus importante. Par exemple, une activité qui vous dit de Marcher rapidement à la moitié de votre Vitesse modifie la distance normale que vous pouvez parcourir en Marchant rapidement. Marcher rapidement possédera toujours le trait déplacement, déclenchera toujours les réactions basées sur le déplacement, etc. L'action subordonnée ne bénéficie d'aucun des traits de l'action plus importante, sauf si cela est spécifié. L'action qui vous permet d'utiliser une action subordonnée ne vous oblige pas à dépenser plus d'actions ou de réactions pour le faire. Ce coût est déjà pris en compte.</p>
<p>L'utilisation d'une activité n'est pas la même chose que l'utilisation d'une de ses actions subordonnées. Par exemple, l'état Accéléré que vous obtenez par le sort <em>Rapidité</em> vous permet de dépenser une action supplémentaire à chaque tour pour Marcher rapidement ou Frapper, mais vous ne pouvez pas utiliser cette action supplémentaire pour une activité qui inclut de devoir Marcher rapidement ou de Frapper. Autre exemple : si vous utilisez une action qui spécifie "Si la prochaine action que vous utilisez est une Frappe", une activité qui inclut une Frappe ne sera pas prise en compte car la prochaine chose que vous ferez consistera à commencer une activité et non d'utiliser l'action basique consistant à Frapper.</p>
</section>

<h2>Actions basiques</h2>
<p>Les <strong>actions basiques</strong> représentent des tâches courantes comme se déplacer, attaquer et aider les autres. En tant que telles, toutes les créatures peuvent utiliser les actions basiques, sauf dans certaines circonstances extrêmes et nombre de ces actions sont utilisées très fréquemment. Vous utiliserez notamment beaucoup Interagir, Faire un pas, Marcher rapidement et Frapper. De nombreux dons et autres actions vous demandent d'utiliser l'une de ces actions basiques ou les modifieront pour produire des effets différents. Par exemple, une action plus complexe peut vous permettre de Marcher rapidement deux fois et d'ajouter un grand nombre d'activités y compris de Frapper. Une action ou une activité pourrait également modifier une action basique, par exemple en vous permettant de Marcher rapidement seulement à la moitié de votre vitesse.</p>
<p>Les actions qui sont utilisées moins fréquemment mais qui sont toujours disponibles pour la plupart des créatures sont présentées dans les Actions basiques spécialisées. Celles-ci ont généralement des conditions que tous les personnages ne sont pas susceptibles de remplir, comme de manier un bouclier, d'avoir une vitesse de creusement ou de chuter en étant dans les airs.</p>
<p>En plus des actions de ces deux parties, il existe des actions d'incantation et des actions pour utiliser des objets magiques que vous trouverez ailleurs.</p>
<p><strong>Retarder et Préparer</strong> Si vous souhaitez modifier le moment où vous effectuez des actions, deux actions basiques vous permettent de le faire. Retarder décale votre tour entier à un autre moment dans l'ordre d'initiative au cours du round et Préparer vous permet de vous préparer à entreprendre une action spécifique lorsqu'un déclencheur que vous définissez est rempli.</p>

<h3>Actions basiques</h3>

<h2>AIDER <span class="action-glyph">R</span></h2>
<hr />
<p><strong>Déclencheur</strong> Un allié est sur le point d'utiliser une action qui lui demande de faire un test de compétence ou un jet d'attaque.</p>
<p><strong>Conditions</strong> L'allié consent à accepter votre aide et vous vous êtes apprêté à l'aider (voir ci-dessous).</p>
<hr />
<p>Vous tentez d'apporter votre aide à un allié pour accomplir une tâche. Pour utiliser cette réaction, vous devez préalablement vous être apprêté à l'aider, généralement en utilisant une action lors de votre tour. Vous devez expliquer au MJ de quelle manière vous comptez apporter votre aide et il détermine alors si vous pouvez Aider votre allié.</p>
<p>Quand vous utilisez la réaction Aider, faites un test de compétence ou un jet d'attaque dont le type est décidé par le MJ. Le DD est généralement de 15, mais le MJ pourrait l'ajuster si la tâche est particulièrement facile ou difficile. Le MJ peut ajouter les traits adaptés à votre action préparatoire ou à votre réaction Aider ou même vous autoriser à Aider sur des tests autres que des tests de compétence ou des jets d'attaque.</p>
<hr />
<p><strong>Succès critique</strong> Vous permettez à votre allié un bonus de circonstances de +2 au test déclencheur. Si vous êtes maître dans le test que vous faites, ce bonus est de +3 et, si vous êtes légendaire, il est de +4.</p>
<p><strong>Succès</strong> Vous accordez à votre allié un bonus de circonstances de +1 au test déclencheur.</p>
<p><strong>Échec critique</strong> Votre allié subit une pénalité de circonstances de -1 au test déclencheur.</p>

<section>
<h2>DÉTAILS POUR AIDER</h2>
<p>Les clarifications suivantes pourraient être pertinentes lorsque vous Aidez un allié.</p>
<p><strong>Tâches longues</strong> Pour une tâche qui dure plus longtemps qu'un round, vous avez besoin de dépenser plus d'une action pour vous apprêter à aider, à la discrétion du MJ.</p>
<p><strong>Proximité</strong> Vous n'avez pas nécessairement besoin d'être proche de votre allié pour l'aider, bien que vous deviez vous trouver à un endroit raisonnable pour l'aider à la fois lorsque vous fixez tous les deux le plan et lorsque vous dépensez la réaction.</p>
<p><strong>Répétition</strong> Aider plusieurs fois la même créature peut avoir des effets dégressifs. En particulier, si vous essayez d'Aider à plusieurs reprises des attaques ou des tests de compétence contre une créature, le MJ augmentera généralement le DD à chaque fois que votre ennemi deviendra plus averti. Ce n'est pas le cas s'il n'y a aucune raison pour que la tâche ait moins de chances de réussir si elle est répétée, comme Aider quelqu'un qui escalade un mur ou qui crochète une serrure.</p>
</section>

<h2>BONDIR <span class="action-glyph">1</span></h2>
<p><em>DÉPLACEMENT</em></p>
<hr />
<p>Vous faites un petit saut horizontal ou vertical. Sauter sur une plus grande distance nécessite d'utiliser un test d'Athlétisme pour Sauter en hauteur ou Sauter en longueur.</p>
<ul>
<li><strong>Horizontal</strong> Vous pouvez Bondir de 3 m au maximum à l'horizontale si votre Vitesse est d'au moins 4,5 m ou de 4,5 m au maximum à l'horizontale si votre Vitesse est d'au moins 9 m. Vous atterrissez dans l'espace où se termine votre Bond (ce qui signifie que vous pouvez généralement sauter par-dessus un fossé de 1,50 m ou un fossé de 3 m si votre Vitesse est supérieure ou égale à 9 m). Vous ne pouvez pas faire de bond horizontal si votre vitesse est inférieure à 4,5 m.</li>
<li><strong>Vertical</strong> Vous pouvez sauter de 90 cm en hauteur sur 1,50 m en longueur pour atteindre une surface surélevée.</li>
</ul>

<h2>CHERCHER <span class="action-glyph">1</span></h2>
<p><em>CONCENTRATION SECRET</em></p>
<hr />
<p>Vous scrutez une zone à la recherche de signes de créatures ou d'objets, incluant possiblement des portes secrètes ou des dangers. Choisissez une zone à scruter. Le MJ détermine la zone que vous pouvez scruter avec une action Rechercher. Toujours environ 9 mètres dans n'importe quelle dimension. Le MJ pourrait vous imposer une pénalité si vous Cherchez au loin ou ajuster le nombre d'actions qu'il vous faut ou si la zone est particulièrement encombrée.</p>
<p>Le MJ fait un unique test de Perception secret à votre place et compare le résultat aux DD de Discrétion de toutes les créatures Non détectées ou Cachées dans la zone ou au DD nécessaire pour détecter chaque objet qui se trouve dans la zone (qui est alors déterminé par le MJ ou par quiconque a Dissimulé cet objet). Une créature que vous détectez pourrait rester Cachée plutôt que dans l'état Observé si vous utilisez un sens imprécis ou si un effet (comme Invisibilité) empêche ce sujet d'être Observé.</p>
<hr />
<p><strong>Succès critique</strong> Toute créature Non détectée ou Cachée contre laquelle vous avez obtenu un succès critique devient Observée. Vous apprenez l'emplacement des objets dans la zone contre lesquels vous avez obtenu un succès critique.</p>
<p><strong>Succès</strong> Toute créature Non détectée contre laquelle vous avez obtenu un succès devient Cachée au lieu de Non détectée et toute créature Cachée devient Observée. Vous apprenez l'emplacement ou obtenez un indice vous aidant à trouver celui-ci, selon ce que détermine le MJ.</p>

<h2>DEVINER LES INTENTIONS <span class="action-glyph">1</span></h2>
<p><em>CONCENTRATION SECRET</em></p>
<hr />
<p>Vous tentez de déterminer si le comportement d'une créature est anormal. Choisissez une créature et tentez de repérer un langage corporel étrange, des signes de nervosité ou d'autres indicateurs qui peuvent laisser penser qu'elle tente de tromper quelqu'un. Le MJ fait un unique test de Perception secret à votre place et compare le résultat au DD de Duperie de la créature, au DD d'un sort qui affecte l'état mental de la créature ou à un autre DD qu'il juge approprié à la situation. Vous ne pouvez généralement pas refaire de tentative de Deviner les intentions de la même créature si la situation n'évolue pas significativement.</p>
<hr />
<p><strong>Succès critique</strong> Vous devinez les véritables intentions de la créature et avez une bonne idée de toute magie mentale qui l'affecte.</p>
<p><strong>Succès</strong> Vous êtes capable de dire si la créature se comporte normalement ou non, mais vous ne connaissez pas ses véritables intentions ni le type de magie qui pourrait l'affecter.</p>
<p><strong>Échec</strong> Vous détectez seulement ce qu'une créature trompeuse cherche à vous faire croire. Si la créature ne cherche pas à vous tromper, vous êtes convaincu qu'elle se comporte normalement.</p>
<p><strong>Échec critique</strong> Votre impression concernant les intentions de la créature est erronée.</p>

<h2>FAIRE UN PAS <span class="action-glyph">1</span></h2>
<p><em>DÉPLACEMENT</em></p>
<hr />
<p><strong>Conditions</strong> Votre Vitesse est d'au moins 3 mètres.</p>
<hr />
<p>Vous vous déplacez prudemment de 1,5 mètre. Contrairement à la plupart des autres types de déplacements, Faire un pas ne déclenche pas de réactions comme Frappe réactive qui peuvent être déclenchées par des actions de mouvement ou lorsque vous pénétrez ou que vous quittez une case.</p>
<p>Vous ne pouvez pas Faire un pas sur un terrain difficile et vous ne pouvez pas Faire un pas en utilisant une autre Vitesse que votre Vitesse au sol.</p>

<h2>FRAPPER <span class="action-glyph">1</span></h2>
<p><em>ATTAQUE</em></p>
<hr />
<p>Vous attaquez avec une arme que vous tenez en main ou une attaque à mains nues, en ciblant une créature qui se trouve dans votre allonge (pour une attaque au corps-à-corps) ou à portée (pour une attaque à distance). Lancez un jet d'attaque en utilisant le modificateur d'attaque pour l'arme ou l'attaque à mains nues que vous utilisez, puis comparez le résultat à la CA de la créature cible afin de déterminer l'effet.</p>
<hr />
<p><strong>Coup critique</strong> Vous lancez les jets de dégâts en fonction de l'arme ou de l'attaque à mains et infligez le double des dégâts.</p>
<p><strong>Succès</strong> Vous lancez les jets de dégâts en fonction de l'arme ou de l'attaque à mains et infligez les dégâts.</p>

<section>
<h2>STATISTIQUES DES FRAPPES</h2>
<p>Voir Jets d'attaque et Dégâts pour obtenir des détails sur le calcul de vos jets d'attaque et de dégâts. Le jet de dégât d'une Frappe utilise le dé de dégât de l'arme ou de l'attaque à mains nues, plus tous les modificateurs, les bonus et les pénalités que vous possédez aux dégâts. Si vous utilisez un type d'attaque autre qu'une Frappe, tel qu'une attaque de sort ou une action pour Saisir, vous calculez les dégâts différemment (voire pas du tout).</p>
</section>

<h2>INTERAGIR <span class="action-glyph">1</span></h2>
<p><em>MANIPULATION</em></p>
<hr />
<p>Vous utilisez votre main ou vos mains pour manipuler un objet ou un élément de votre environnement. Vous pouvez vous saisir d'un objet non porté ou rangé, dégainer une arme, échanger un objet tenu avec un autre, ouvrir une porte ou terminer un effet comparable. En de rares occasions, Vous pourriez avoir à tenter un test de compétence pour déterminer si votre action Interagir est un succès.</p>

<p>MARCHER RAPIDEMENT <span class="action-glyph">1</span></p>
<p><em>DÉPLACEMENT</em></p>
<hr >
<p>Vous vous déplacez d'une distance maximale égale à votre Vitesse.</p>

<section>
<h2>PARLER</h2>
<hr />
<p>Tant que vous pouvez agir, vous pouvez aussi parler. Vous n'avez pas besoin de dépenser d'action pour parler, mais comme un round représente 6 secondes de temps, vous ne pouvez généralement prononcer qu'une seule phrase par round. Les utilisations spéciales de la parole, comme tenter un test de compétence de Duperie pour mentir, nécessitent de dépenser des actions et suivent leurs propres conditions. Toutes les paroles ont le trait audible. Si vous communiquez autrement que par la parole, d'autres règles peuvent s'appliquer. Par exemple, le langage des signes est visuel et non audible.</p>
</section>

<h2>PRÉPARER <span class="action-glyph">2</span></h2>
<p><em>CONCENTRATION</em></p>
<hr />
<p>Vous vous préparez à utiliser une action qui se déroulera hors de votre tour. Choisissez une action unique ou une action gratuite que vous pouvez utiliser et précisez un déclencheur. Votre tour prend alors fin. Si le déclencheur que vous avez indiqué se déroule avant le début de votre prochain tour, vous pouvez utiliser l'action choisie par une réaction (dans la mesure où vous répondez à toutes les conditions pour l'utiliser). Vous ne pouvez pas Préparer une action gratuite qui possède déjà un déclencheur.</p>
<p>Si vous êtes soumis à une pénalité d'attaques multiples et que votre action préparée est une action d'attaque, votre attaque préparée est soumise à la pénalité d'attaques multiples que vous aviez quand vous l'avez Préparée. Il s'agit d'une des rares occasions où la pénalité d'attaques multiples est appliquée hors de votre tour.</p>

<h2>RAMPER <span class="action-glyph">1</span></h2>
<p><em>DÉPLACEMENT</em></p>
<hr />
<p><strong>Conditions</strong> Vous êtes À terre et votre Vitesse est d'au moins 3 mètres.</p>
<hr />
<p>Vous vous déplacez de 1,5 mètre en rampant, en continuant à rester À terre.</p>

<h2>RELÂCHER <span class="action-glyph">F</span></h2>
<p><em>MANIPULATION</em></p>
<hr />
<p>Vous Relâchez quelque chose que vous tenez dans une main ou dans vos mains. Cela peut signifier laisser tomber un objet, retirer une main de votre arme tout en continuant à la tenir avec l'autre, relâcher une corde à laquelle est suspendu un chandelier ou accomplir une action comparable. À la différence des autres actions de manipulation, Relâcher ne déclenche pas de réactions qui peuvent être déclenchées par les actions possédant le trait manipulation (comme Frappe réactive).</p>
<p>Si vous voulez vous tenir prêt à Relâcher quelque chose hors de votre tour, utilisez l'activité Préparer.</p>

<h2>RETARDER <span class="action-glyph">F</span></h2>
<hr />
<p><strong>Déclencheur</strong> Votre tour commence.</p>
<hr />
<p>Vous attendez le bon moment pour agir. Le reste de votre tour ne s'est pas encore déroulé. À la place, vous vous retirez de l'ordre d'initiative. Vous pouvez reprendre une place dans l'ordre d'initiative par une action gratuite déclenchée par la fin du tour d'une autre créature. Cette nouvelle place devient votre place permanente dans l'ordre d'initiative. Vous ne pouvez pas utiliser de réaction avant de reprendre une place dans l'ordre d'initiative. Si vous Retardez un round entier sans reprendre une place dans l'ordre d'initiative, les actions du tour Retardé sont perdues, votre place dans l'ordre d'initiative ne change pas et votre prochain tour se déroule donc à votre place originale dans cet ordre.</p>
<p>Quand vous Retardez, tous les dégâts persistants et les autres effets de vide qui se produisent normalement au début de votre tour sont appliqués immédiatement quand vous utilisez l'action Retarder. Tous les effets bénéfiques qui devraient normalement prendre fin à n'importe quel moment de votre tour se terminent également. Le MJ détermine également quels autres effets prennent fin quand vous Retardez votre tour. En bref, vous ne pouvez pas Retarder votre tour pour échapper à des effets de vide qui vont se dérouler pendant votre tour ou étendre la durée d'effets qui doivent prendre fin pendant votre tour.</p>

<h2>S'ÉCHAPPER <span class="action-glyph">1</span></h2>
<p><em>ATTAQUE</em></p>
<hr />
<p>Vous tentez de vous tirer d'une situation dans laquelle vous êtes Empoigné/agrippé, Immobilisé ou Entravé. Choisissez une créature, un objet, un effet de sort, un danger ou un autre obstacle qui vous impose l'un de ces états. Vous faites un test contre le DD de cet effet en utilisant votre modificateur d'attaque à mains nues. Il s'agit généralement du DD d'Athlétisme de la créature qui vous empoigne, du DD de Vol de la créature qui vous a attaché, du DD des sorts qui vous affectent ou du DD pour S'échapper indiqué dans le profil d'un objet, d'un danger ou d'un autre obstacle. Au lieu d'utiliser votre modificateur d'attaque, vous pouvez faire un test d'Acrobaties ou d'Athlétisme (mais cette action possède toujours le trait attaque).</p>
<hr />
<p><strong>Succès critique</strong> Vous vous libérez et perdez l'état Empoigné/agrippé, Immobilisé ou Entravé que la cible désignée vous imposait. Vous pouvez alors Marcher rapidement sur une distance de 1,5 m.</p>
<p><strong>Succès</strong> Vous vous libérez et perdez l'état Empoigné/Agrippé, Immobilisé ou Entravé que la cible désignée vous imposait.</p>
<p><strong>Échec critique</strong> Vous ne réussissez pas à vous libérer et vous ne pouvez pas tenter de Vous échapper avant votre prochain tour.</p>

<h2>SE JETER À TERRE <span class="action-glyph">1</span></h2>
<p><em>DÉPLACEMENT</em></p>
<hr />
<p>Vous vous jetez À terre.</p>

<h2>SE RELEVER <span class="action-glyph">1</span></h2>
<p><em>DÉPLACEMENT</em></p>
<hr />
<p>Vous étiez À terre et vous Vous relevez.</p>

<h2>MISE À L'ABRI <span class="action-glyph">1</span></h2>
<hr />
<p><strong>Conditions</strong> Vous bénéficiez d'un abri, êtes à proximité d'un élément de votre environnement derrière lequel vous abriter ou êtes À terre.</p>
<hr />
<p>Vous vous plaquez contre un mur ou vous jetez derrière un obstacle pour mieux vous mettre à l'abri. Si vous bénéficiez déjà d'un abri standard, vous obtenez à la place un abri important qui vous confère un bonus de +4 à la CA, à vos jets de réflexes contre les zones d'effet et à vos tests de Discrétion pour Vous cacher, Être furtif ou éviter de vous faire détecter de manière générale. Vous bénéficiez sinon des avantages liés à un abri standard (un bonus de circonstances de +2). Ces avantages perdurent jusqu'à ce que vous quittiez votre emplacement actuel, utilisiez une action d'attaque, vous trouviez dans l'état Inconscient ou mettiez fin à cet effet par une action gratuite.</p>

<h3>Actions basiques spécialisées</h3>
<p>Ces actions sont utiles dans des circonstances spécifiques. Les actions Arrêter une chute, Creuser et Voler nécessitent d'avoir un type de mouvement spécial. Les vitesses d'escalade et de nage utilisent les actions correspondantes de la compétence Athlétisme.</p>

<h2>ARRÊTER UNE CHUTE <span class="action-glyph">R</span></h2>
<hr />
<p><strong>Déclencheur</strong> Vous chutez.</p>
<p><strong>Conditions</strong> Vous possédez une Vitesse de vol.</p>
<hr />
<p>Vous faites un test d'Acrobaties ou de Réflexes pour ralentir votre chute. Le DD est généralement de 15, mais il peut être plus élevé en présence de turbulences ou d'autres circonstances.</p>
<hr />
<p><strong>Succès</strong> Vous ne subissez pas de dégâts de la chute.</p>

<h2>CREUSER <span class="action-glyph">1</span></h2>
<p><em>DÉPLACEMENT</em></p>
<hr />
<p><strong>Conditions</strong> Vous avez une Vitesse de creusement.</p>
<hr />
<p>Vous creusez un chemin à travers de la terre, du sable ou un autre matériau meuble à un rythme égal à votre Vitesse de creusement. Vous ne pouvez pas creuser à travers la roche ou d'autres substances plus denses que la terre, à moins que vous ne possédiez une capacité qui vous permet de le faire.</p>

<h2>DÉTOURNER LE REGARD <span class="action-glyph">1</span></h2>
<hr />
<p>Vous détournez le regard d'un danger, comme le regard d'une méduse. Vous obtenez un bonus de circonstances de +2 à vos jets de sauvegarde contre les capacités visuelles qui nécessitent que vous regardiez une créature ou un objet, comme le regard pétrifiant d'une méduse. Votre regard reste détourné jusqu'au début de votre prochain tour.</p>

<h2>LEVER UN BOUCLIER <span class="action-glyph">1</span></h2>
<hr />
<p><strong>Conditions</strong> Vous maniez un bouclier.</p>
<hr />
<p>Vous positionnez votre bouclier pour vous protéger. Quand vous avez Levé un bouclier, vous ajoutez son bonus de circonstances indiqué à votre CA. Votre bouclier reste levé jusqu'au début de votre prochain tour.</p>

<h2>MAINTENIR <span class="action-glyph">1</span></h2>
<p><em>CONCENTRATION</em></p>
<p>Choisissez un de vos effets qui a une durée <em>maintenue...</em> ou qui indique un avantage spécial lorsque vous le Maintenez. La plupart de ces effets proviennent des sorts ou de l'activation d'objets magiques. Si l'effet possède une durée maintenue, sa durée se prolonge jusqu'à la fin de votre prochain tour. (Maintenir plusieurs fois au cours du même round n'a pas pour effet d'étendre les effets lors des tours subséquents). Si une capacité peut être maintenue mais n'indique pas combien de temps, elle peut être maintenue pendant 10 minutes.</p>
<p>Un effet pourrait indiquer un avantage supplémentaire qui se produit si vous le Maintenez et cela peut même arriver avec des effets qui n'ont pas une durée maintenue. Si l'effet dispose à la fois d'un effet spécial et d'une durée maintenue, votre action Maintenir étend la durée et vous permet de bénéficier de l'avantage spécifique.</p>
<p>Si votre action Maintenir est interrompue, la capacité se termine.</p>

<h2>RÉVOQUER <span class="action-glyph">1</span></h2>
<p><em>CONCENTRATION</em></p>
<hr />
<p>Vous mettez un terme à un effet qui stipule que vous pouvez le Révoquer. Révoquer met fin à l'entier effet à moins qu'il ne soit indiqué l'inverse.</p>

<h2>SE METTRE EN SELLE <span class="action-glyph">1</span></h2>
<p><em>DÉPLACEMENT</em></p>
<hr />
<p><strong>Conditions</strong> Vous vous trouvez sur une case adjacente à une créature qui fait au moins une catégorie de taille de plus que la vôtre et qui consent à être votre monture.</p>
<hr />
<p>Vous enfourchez la créature et la chevauchez. Si vous êtes déjà en selle, vous pouvez à la place utiliser cette action pour descendre de selle et mettre pied à terre dans un espace adjacent à votre monture.</p>

<h2>SE RACCROCHER IN EXTREMIS <span class="action-glyph">R</span></h2>
<p><em>MANIPULATION</em></p>
<hr />
<p><strong>Déclencheur</strong> Vous chutez ou passez à proximité d'un rebord ou d'une prise.</p>
<p><strong>Conditions</strong> Vos mains ne sont ni attachées dans votre dos, ni Entravées.</p>
<hr />
<p>Quand vous chutez ou passez à proximité d'un rebord ou d'une autre prise, vous pouvez tenter de vous y raccrocher afin d'interrompre votre chute. Vous devez pour cela réussir soit un jet d'Acrobaties, soit un jet de Réflexes, à votre choix, correspondant généralement au DD pour l'Escalader. Si vous réussissez à vous raccrocher au rebord ou à la prise, vous pouvez alors l'Escalader en utilisant la compétence Athlétisme.</p>
<hr />
<p><strong>Succès critique</strong> Vous attrapez le rebord ou la prise, sans nécessairement avoir besoin d'avoir une main libre, généralement en utilisant un objet tenu adapté pour vous rattraper (en plantant la lame d'une hache de bataille sur un rebord, par exemple). Vous subissez quand même les dégâts correspondant à la hauteur de laquelle vous avez chuté, mais traitez cette chute comme si elle était plus courte de 9 m.</p>
<p><strong>Succès</strong> Si vous avez au moins une main libre, vous attrapez le rebord ou la prise et interrompez votre chute. Vous subissez quand même les dégâts correspondant à la hauteur de laquelle vous avez chuté, mais vous traitez cette chute comme si elle était plus courte de 6 m. Si vous n'avez aucune main libre, vous continuez à chuter comme si vous aviez échoué à votre test.</p>
<p><strong>Échec critique</strong> Vous continuez à tomber et, si vous aviez déjà chuté d'une hauteur supérieure ou égale à 6 m quand vous avez utilisé cette réaction, vous subissez 10 dégâts contondants lors de l'impact par tranche de 6 m de la chute.</p>

<h2>SIGNALER <span class="action-glyph">1</span></h2>
<p><em>AUDIBLE MANIPULATION VISUEL</em></p>
<hr />
<p><strong>Conditions</strong> Une créature est non détectée par un ou plusieurs de vos alliés, mais pas par vous.</p>
<hr />
<p>Vous désignez une créature que vous pouvez voir à un ou plusieurs de vos alliés, en la montrant du doigt et en indiquant verbalement à quelle distance elle se trouve. La créature se trouve alors Cachée par rapport à vos alliés plutôt que Non détectée. Cela ne fonctionne que si vos alliés peuvent vous voir et qu'ils se trouvent à un endroit d'où ils pourraient potentiellement détecter la cible. Si vos alliés ne peuvent pas vous entendre ou vous comprendre, ils doivent alors réussir un test de Perception contre le DD de Discrétion de la créature. Sur un échec, ils vous comprennent mal et croient que la cible se trouve à un autre endroit que celui que vous désignez.</p>

<h2>VOLER <span class="action-glyph">1</span></h2>
<p><em>DÉPLACEMENT</em></p>
<hr />
<p><strong>Conditions</strong> Vous avez une Vitesse de vol.</p>
<hr />
<p>Vous vous déplacez dans les airs d'une distance maximale égale à votre Vitesse de vol. Vous appliquez les règles du déplacement sur un terrain difficile pour vous déplacer vers le haut (verticalement ou en diagonale). Vous pouvez vous diriger verticalement vers le bas de 3 m en dépensant 1,50 m de mouvement. Si vous Volez jusqu'au sol, vous ne subissez pas de dégâts de chute. Vous pouvez utiliser une action pour Voler sur une distance de 0 m afin de faire du vol stationnaire. Si vous êtes dans les airs à la fin de votre tour et que vous n'avez pas utilisé d'action pour Voler lors de ce tour, vous chutez.</p>