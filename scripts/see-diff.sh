#!/bin/bash

# Ne fonctionne que si la version anglaise est sur une seule ligne.
# dans le cas contraire:
#
# FILENAME=xxx
# COMMIT=`git blame $FILENAME | grep 'État: changé' | cut -d' ' -f1`
# PARENT=`git rev-parse ${COMMIT}\^`
# git diff `git blame $FILENAME | grep 'État: changé' | cut -d' ' -f1`.. $FILENAME

[ -z $1 ] && echo "give a file name" && exit 0
[ -f $1 ] || (echo "File not found" && exit 0)

FILENAME=$1
COMMIT=`git blame $FILENAME | grep 'État: changé' | cut -d' ' -f1`

echo "COMMIT: $COMMIT"

PARENT=`git rev-parse ${COMMIT}\^`

echo "PARENT: $PARENT"
echo

git show $PARENT:$FILENAME | sed -n "/^-* Desc\(ription\)\? (en) -*$/{n;p;}" | sed -e 's/\(<\/\?\(h[1-8]\( class=\"[a-z]\*\"\)\?|tbody\|tr\|td\|p\|div\|br\|ul\|li\)>\)/\n\1\n/g' > avant
cat $FILENAME | sed -n "/^-* Desc\(ription\)\? (en) -*$/{n;p;}" | sed -e 's/\(<\/\?\(h[1-8]\( class=\"[a-z]\*\"\)\?|tbody\|tr\|td\|p\|div\|br\|ul\|li\)>\)/\n\1\n/g' > apres

diff avant apres
rm -f avant apres

exit 0
