#!/bin/bash

set -e

echo "Generating Babele file"
./update-babele.py

echo "Mise à jour des fichiers status et dictionnaires"
./update-status.py

MAJ=`grep "version" ../module.json | awk -F'.' '{print $1}' | awk -F'"' '{print $4}'`
MIN=`grep "version" ../module.json | awk -F'.' '{print $2}'`
PATCH=`grep "version" ../module.json | awk -F'.' '{print $3}' | awk -F'"' '{print $1}'`
if [ $CI_RELEASE_TYPE = "major" ]
then
  MAJ="$((MAJ+1))"
  MIN="0"
  PATCH="0"
elif [ $CI_RELEASE_TYPE = "minor" ]
then
  MIN="$((MIN+1))"
  PATCH="0"
else
  PATCH="$((PATCH+1))"
fi
VERSION="$MAJ.$MIN.$PATCH"

echo "Increment module version to $VERSION"
cat ../module.template.json | sed "s/VERSION/$VERSION/g" > ../module.json

if [ $CI_DEPLOY_MODULE = "true" ]
then
    echo "Commit and push module update"
    git add ../archive ../lang ../data ../babele* ../module.json
    git commit -m $VERSION
    git tag $VERSION
    git push --tags https://root:$ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:$CI_COMMIT_REF_NAME
else
    echo "Module deployment disabled, skipping git commands, outputting diff"
    git diff --stat
fi

echo "Done"
