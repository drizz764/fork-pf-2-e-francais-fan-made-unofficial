#!/bin/bash

set -e

echo "Downloading latest foundry packs and system keys"
mkdir .ci/ || true
wget --no-cache -q https://github.com/foundryvtt/pf2e/releases/latest/download/json-assets.zip -O .ci/pf2e.zip
unzip -o -d .ci/ .ci/pf2e.zip # ZIP du système
mv .ci/packs ../packs
cp .ci/lang/en.json ../lang/en.json
cp .ci/lang/re-en.json ../lang/re/en.json
cp .ci/lang/action-en.json ../lang/action/en.json
cp .ci/lang/kingmaker-en.json ../lang/kingmaker/en.json
rm -rf .ci/

if [ $CI_UPDATE_LANG = "true" ]
then
    echo "Commit et push du fichier lang"
    git add ../lang

    if [ $CI_UPDATE_COMPENDIUM != "true" ] && [ $CI_DEPLOY_MODULE != "true" ]
    then
        git diff-index --quiet --cached HEAD || git commit -m "Mise à jour du en.json" -- ../lang
        git push -o ci.skip https://root:$ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:master
    fi
fi

echo "Downloading animal companions packs"
mkdir .ci/ || true
wget --no-cache -q https://github.com/TikaelSol/PF2e-Animal-Companions/archive/refs/heads/main.zip -O .ci/animal.zip
unzip -o -d .ci/ .ci/animal.zip
ls .ci/PF2e-Animal-Companions-main/
mv .ci/PF2e-Animal-Companions-main/packs ../packs-animal
rm -rf .ci/

echo "Done"