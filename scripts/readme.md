# Information concernant l'automatisation

## Contenu du répertoir

Ce répertoire contient
- Des scrips .sh
- Des configs de pipe Gitlab pour pouvoir faire tourner des automatismes en auto.
## Approches pour la génération des fichiers à traduire

- Les fichiers lang (fichiers de clés systèmes) sont mis sur le côté et sont synchros sur une plateforme de traduction, nommée Crowdin, qui permet de traduire ces fichiers facilement.
- la traduction des compendiums via Babele pour 3, il y a plusieurs scripts
    
    A. libdata.py : fonctions générales, définitions
    B. prepare.py : ouvre les fichiers VO, ouvre les fichiers de traduction VF, compare les deux, écrit de nouveaux fichiers de traduction VF, archive ce qui est obsolète
    C. update-babele.py : transforme les fichiers de traduction en fichier Babele
    D. update-status.py : crée des fichiers de statuts et des dictionnaires pour la gestion générale 

Enfin, il y a ce qui est nécessaire à faire tourner un module Foundry :

- Le module.json (avec une gestion automatique des versions via le pipeline gitlab et le script de build-ci-module.sh)
- Le script pf2fr.js qui fournit des convertisseurs Babele avancés et le nouveau mapping automatique pour PF2E animations